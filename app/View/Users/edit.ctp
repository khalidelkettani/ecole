<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'users','action' => '/dashboard')); ?>"><?php echo __('Accueil');?></a></li>
             <?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
            <!--<li><span><?php echo __('Recrutement');?></span></li>-->
            <li><span><?php echo __('élève');?></span></li>
            <!--<li class="active"><span><?php echo __('Ajouter un utilisateur');?></span></li>-->
        </ol>                                   
        <h1><?php echo __('Ajouter un utilisateur');?><small></small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="main-box">
            <header class="main-box-header clearfix">
                <h2>Ajout</h2>
            </header>                       
            <div class="main-box-body clearfix">
                 <?php echo $this->Form->create('User',array('type'=>'file','role'=>'form','data-parsley-validate'=>'data-parsley-validate')); ?>
                 <!--  <?php echo $this->Form->input('id',array('type'=>'hidden')); ?> -->
                <div class="col-lg-6">
                    <div class="form-group">
                        <label><?php echo __('Nom de l\'utilisateur'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                                              <?php echo $this->Form->input('username',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
                    </div>
                     <div class="form-group">
                        <label><?php echo __('Email de l\'utilisateur'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                        <?php echo $this->Form->input('email',array('class'=>'form-control','div'=>false,'placeholder'=>__('Email de de l\'utilisateur'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
                    </div>
                    <div class="form-group">
                        <label><?php echo __('Mot de passe de l\'utilisateur'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                        <?php echo $this->Form->input('password',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prenom de de l\'élève'),'label'=>false,'data-parsley-required'=>true, 'data-parsley-error-message' =>__('Ce champ est requis')));?>
                    </div>
                    <div class="form-group">
                            <label><?php echo __('Rôle de l\'utilisateur'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                            <?php echo $this->Form->select('role',array(array('admin' => 'Admin', 'author' => 'Author')));?>
                    </div>
                    
                    <div class="form-group">
                        <label><?php echo __('Photo'); ?></label>
                        <?php echo $this->Form->input('avatar_logo',array('label'=> false, 'type' =>'file','data-parsley-required'=>false, 'placeholder'=>__('Photo'),'required'=> false, 'data-parsley-error-message' =>__('Ce champ est requis') ));?>
                    </div> 

                                                
                </div>
                                
                    
                </div>
                <h1></h1>
                <div class="col-lg-12" style="text-align:right">
                    <a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'index')); ?>"><?php echo __('ANNULER'); ?></a>
                    <button type="submit" data-loading-text="Loading..." class="btn btn-primary btn-lg" id="btn-loading-demo"><?php echo __('ENREGISTRER'); ?>
                    </button>

                </div>
                <?php echo  $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(function($) {
        
        $('#datepickerDate').datepicker({
          format: 'yyyy-mm-dd'
        });
        
    });
</script>
<script>
    $(function($) {
        $('#sel2').select2();
        $('#sel3').select2();
        $('#sel4').select2();

        
    });
</script>



<script type="text/javascript">
    $(document).ready(function(){
          
            $('body').on('keyup', '.search', function(){
            var q=$(this).val();
            var url = '<?php echo Router::url(array('controller'=>'users','action'=>'search')) ?>'+'/'+q;
                $(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
                $.get(url, function(data) {
                    $(".xtable").html(data);
                });
            });

        //edit function
            $(".modalEd").on('click', function(){
                 var url=$(this).attr('href'); 
                $.get(url, function(data) 
                {
                  $("#modalEdit").html(data);
                  $("#modalEdit").modal();
                });
            return false;
            });

    });

    $(function($) {
        $('#sel2').select2();
    });

    </script>
