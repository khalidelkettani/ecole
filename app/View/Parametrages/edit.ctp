<div class="row">
				<div class="col-lg-12">
					<header id="email-header" class="clearfix">
						<ol class="breadcrumb">
							<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/dashboard')); ?>"><?php echo __('Accueil');?></a></li>
							<li><span><?php echo __('ADMIN');?></span></li>
							<li><span><?php echo __('Paramétrages');?></span></li>
							
						</ol>

					</header>
				</div>
			</div>
			<div class="row">
				<?php echo $this->Form->create('Classs',array('url'=> array(
                                                 'controller' => 'parametrages','action' => 'edit'
                                                  ))); ?>
						<div class="col-lg-6">
							<h3><span><?php echo __('Géneral'); ?></span></h3>
							
								<div class="form-group">
									<label><?php echo __('Année scollaire par defaut'); ?> <span class="f_req"></span></label>
										<?php  echo $this->Form->input( __('Année scollaire'), array(
			                      	'class' => 'form-control  ch_filtrer filtrer', 'id'=>'annee','required' => false,
										  'options' => array($annees),
										  //'empty' => $options,
										  'value'=> $options
										  
										));?>
								</div>
						</div>
			</div>					
			<div class="form-group">
				<label><?php echo __('Mois de l\'année scollaire'); ?> </label>
				
				<div class="col-lg-12">
					<div class="col-lg-3">
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-1"  />
							<!--checked="checked"-->
							<label><?php echo __('Janvier'); ?> </label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-2" />
							<label for="checkbox-2">
								<label><?php echo __('Février'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-3">
								<label><?php echo __('Mars'); ?> </label>
							</label>
						</div>
				    </div>	
				    <div class="col-lg-3">
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-2" />
							<label for="checkbox-4">
								<label><?php echo __('Avril'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-5">
								<label><?php echo __('Mai'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-2" />
							<label for="checkbox-6">
								<label><?php echo __('Juin'); ?> </label>
							</label>
						</div>
				    </div>	
				    <div class="col-lg-3">
						
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-7">
								<label><?php echo __('Août'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-8">
								<label><?php echo __('Septembre'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-9">
								<label><?php echo __('Octobre'); ?> </label>
							</label>
						</div>
				    </div>	
				    <div class="col-lg-3">
						
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-10">
								<label><?php echo __('Novembre'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-3" />
							<label for="checkbox-11">
								<label><?php echo __('Décembre'); ?> </label>
							</label>
						</div>
						
				    </div>	
				</div>		
			</div>
			<div class="form-group">
				<label><?php echo __('Jours de la semaine scollaire'); ?> </label>
				
				<div class="col-lg-12">
					<div class="col-lg-3">
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-j1" checked="checked" />
							<label><?php echo __('Lundi'); ?> </label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-j2" />
							<label for="checkbox-j2">
								<label><?php echo __('Mardi'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-j3" />
							<label for="checkbox-j3">
								<label><?php echo __('Mercredi'); ?> </label>
							</label>
						</div>
						</div>
						<div class="col-lg-3">
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-j4" />
							<label for="checkbox-j4">
								<label><?php echo __('Jeudi'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-j5" />
							<label for="checkbox-j5">
								<label><?php echo __('Vendredi'); ?> </label>
							</label>
						</div>
						<div class="checkbox-nice">
							<input type="checkbox" id="checkbox-j6" />
							<label for="checkbox-j6">
								<label><?php echo __('Samedi'); ?> </label>
							</label>
						</div>
						</div>
				        <div class="col-lg-3">
								<div class="checkbox-nice">
									<input type="checkbox" id="checkbox-j7" />
									<label for="checkbox-j7">
										<label><?php echo __('Dimanche'); ?> </label>
									</label>
								</div>
						</div>

				    </div>
				    </div>
				    <div class="form-group">
				<label><?php echo __('Horaire de la semaine scollaire'); ?> </label>
				
				<div class="col-lg-12">
					<div class="col-lg-3">
						<div class="form-group" style="height:600px" >
													<label><?php echo __('Jours'); ?></label>
													<select multiple class="form-control">
														<option>Lundi</option>
														<option>Mardi</option>
														<option>Mercredi</option>
														<option>Jeudi</option>
														<option>Vendredi</option>
														<option>Samedi</option>
													</select>
												</div>
						</div>
						<div class="col-lg-3">							
						<div class="form-group">
								<label><?php echo __('Horaire '); ?></label>
								<div class="form-group">
													<select multiple class="form-control">
														<option>1</option>
														<option>2</option>
														<option>3</option>
														<option>4</option>
														<option>5</option>
													</select>
												</div>
						</div>
						</div>
						</div>
						</div>
						</div>
						
						

				
				    <div class="modal-footer">
					 <a class="btn btn-primary btn-lg" href="<?php echo $this->Html->url(array('controller' => 'parametrages','action' => 'parametrage')); ?>"><?php echo __('Enregistrer'); ?></a> 
					
				    </div>
				<?php echo $this->Form->end(); ?>