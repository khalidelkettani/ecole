<?php echo $this->Form->create('Horaire'); ?>
	<?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
<div class="modal-dialog">	
			<div class="modal-content" >
				<div class="modal-header">
					<button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="col-lg-12" style="text-align:right">
						<!-- <button onClick="hideEDiteur()"class="btn btn-primary btn-lg" data-toggle="modal"  data-target="#">
  								<?php echo __('Nouveau Horaire');?>
						</button> -->
						<p><button class="btn btn-default" id="btnaffAction"  onClick="hideEDiteur()"><?php echo __('Ajouter'); ?></button></p>

					</div>
					<h4 class="modal-title"><?php echo __('Liste des horaires'); ?></h4>
				</div>
				
				<div class="modal-body">
				<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Du');?></th>
							<th><?php echo __('Au');?></th>
							<th><?php echo __('Période');?></th>
							<th>Actions</th>
						</tr>
					</thead>
					<?php 
					foreach ($horaires as $horaire): 
							$id=$horaire['Horaire']['id'];?>
					<tbody>

						<tr>
							<td>
								<!-- <input type="text" value="<?php echo h($horaire['Horaire']['du']); ?>"> -->
								<?php echo h($horaire['Horaire']['du']); ?>&nbsp;
							</td>
							<td>
								<!--<input type="text" value="<?php echo h($horaire['Horaire']['au']); ?>">-->
									<?php echo h($horaire['Horaire']['au']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($horaire['Horaire']['periode']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'villes','action'=>'edit', $horaire['Horaire']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>
	<button style="width: 25%;" type="text"  id="delete" name="<?php echo h($horaire['Horaire']['id']); ?>">
									<!--<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i id="delete" name='.$id.'  class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span> ', array(), array('escape' => false,'class' => 'table-link danger')); ?>-->
									
	
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>

				<?php echo $this->element('paginate'); ?>
				
			</div>
			<div class="row" id="editheure" >
				<div class="col-lg-12">
					<div class="form_style">
							

				<div class="form-group" id="frmAdd">
								
					<div class="col-lg-12">
						<div class="col-lg-6">
						<label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('du',array('class'=>'form-control','id'=>'txtdu','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="col-lg-6">
						<div class="main-box">
                          <label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('au',array('class'=>'form-control','id'=>'txtau','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						</div>
						<div class="col-lg-6">
						<div class="main-box">
                          <label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('periode',array('class'=>'form-control','value'=>'matinee','id'=>'txtp','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						</div>
						
					</div>	
					<div class="col-lg-3">
							<p><button class="btn btn-default" id="btnAddAction" name="submit" onClick="callCrudAction('add','')"><?php echo __('Ajouter'); ?></button></p>
					</div>
					</div>
						<!-- 	<img src=<img src=<?php echo $this->webroot.'img/ajax_loader.gif' ?> id="loaderIcon" style="display:none" /> -->
						</div>
						
					</div>	
				</div>			
			</div>
			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?>&times;</button>
					<!-- <button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button> -->
			</div>
			</div>
 
		</div>
</div>
<?php echo $this->Form->end(); ?>
<script  type="text/javascript">
		alert("index h");
		document.getElementById('frmAdd').style.display = "none";
		/*$("#bts").on('click', function(){
			hideThis('editheure');
		   	alert("addhh");
			//	id=null;
				break;
		});*/ 
   		
		$("#delete").on('click', function(){
							alert("dd");
							var name=$("#delete").attr('name');
							var obj = document.getElementById("delete");
							//alert(name);
							
						     // $('#myModal').modal('show');
						      //$idp=q; 	 
						 alert($("#delete").val());
						 if(window.confirm("Etes-vous sûr que vous voulez supprimer ")){
			                //if confirmed, submit the form
			               callCrudAction("delete",name);
			               var url = '<?php echo Router::url(array('controller'=>'anneesscollaires','action'=>'crud_action')) ?>/0/0/0/0/0';
			                alert("-x");
			                /*$.get(url, function(data) {
								$(".xtable").html(data);
								//alert("Data: " + data);
								alert("x");
							});*/
			                

				      	return false;
			     		
			            }else{
			                //cancel logic here
			            }
		});
    
function hideEDiteur(){
	alert("hideEDiteur");
	document.getElementById('frmAdd').style.display = "block";
	break;
}  
function hideThis(_div){
		  // 	alert("hide");
		    var obj = document.getElementById(_div);
		    if(obj.style.display == "block")
		        obj.style.display = "none";
		    else
		        obj.style.display = "block";
}
   

   

function callCrudAction(action,id) {
	//$("#loaderIcon").show();
alert("add action");
	var queryString;
	var du;
	var au;
	var periode;
if(action=="add"){
	du= $("#txtdu").val();
	au= $("#txtau").val();
	periode= $("#txtp").val();
    id=$("#delete").attr('name');
}
if(action=="delete"){
   	alert("test");
   	du= "0";
	au= "0";
	periode="0";
}
	alert("ajax"+id+action+du);
        $.ajax({
            dataType: "html",
            type: "POST",
            //evalScripts: true,
            url: '<?php echo Router::url(array('controller'=>'anneesscollaires','action'=>'crud_action'));?>'+'/'+du+'/'+au+'/'+periode+'/'+action+'/'+id,
            data: ({du:du,au:au,periode:periode}),
            success: function (data, textStatus){
            	document.getElementById('editheure').style.display = "none";
               $("#frmAdd").html(data);
				return false;
            }
            

        });
	//$('#myModal').modal('show');
}
   
	</script>