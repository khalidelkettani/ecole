<div class="classses view">
<h2><?php echo __('Classs'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($classs['Classs']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($classs['Classs']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($classs['User']['id'], array('controller' => 'users', 'action' => 'view', $classs['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($classs['Classs']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($classs['Classs']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Classs'), array('action' => 'edit', $classs['Classs']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Classs'), array('action' => 'delete', $classs['Classs']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $classs['Classs']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Classses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Classs'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
