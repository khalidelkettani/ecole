<?php echo $this->Flash->render('auth'); ?>

<?php echo $this->Form->create('User'); ?>

		<div class="row">
			<div class="col-xs-12">

				
					<!-- <header id="login-header">
									<div id="login-logo">
										<img src="img/logo.png" alt=""/>
										<div class="top_b"><?php echo __('Bienvenue Sur '); ?></div>
									</div>
							     	</header> -->
					<div id="login-box-holder">
                        <div id="login-box-inner">
                        	<?php echo $this->Form->create('User',array('url'=> array('controller' => 'users','action' => 'login','type' =>'post','role' => 'form') )); ?>
												<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
												<input class="form-control" name="data[User][username]"  type="text" placeholder="<?php echo __('Le username'); ?>" required="true"> 
											</div>
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-key"></i></span>
												<input type="password" name="data[User][password]" class="form-control" placeholder="<?php echo __('Mot de passe'); ?>" required="true">
											</div>
											<div id="remember-me-wrapper">
												<div class="row">
													<div class="col-xs-6">
														<div class="checkbox-nice">
															<!-- <input type="checkbox" id="remember-me" checked="checked" />
															<label for="remember-me">
																Remember me
															</label> -->
														</div>
													</div>
													<a href="<?php echo $this->webroot;?>reset_password" id="login-forget-link" class="col-xs-6">
														<?php echo __('Mot de passe oublié?'); ?>
													</a>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-12">
													<button type="submit" class="btn btn-success col-xs-12"><?php echo __('SE CONNECTER'); ?></button>
												</div>
											</div>
									<?php echo $this->Form->end(); ?>
									</div>
									<!-- <div id="login-box-footer">
										<div class="row">
											<div class="col-xs-12">
												<?php echo __('Vous n\'avez pas de compte?'); ?>
												<a href="<?php echo $this->webroot;?>register">
													<?php echo __('S\'inscrire'); ?>
												</a>
											</div>
										</div>
									</div> -->


				</div>
			
		</div>
	</div>


