<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');


class AppController extends Controller {



    public function beforeFilter() {
        $this->Auth->allow('index', 'view');
    }

    // app/Controller/AppController.php



/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
public $components = array(
    'Flash',
    'Auth' => array(
        /*'loginRedirect' => array('controller' => 'Home', 'action' => 'index'),
                'logoutRedirect' => array('controller' => 'Home', 'action' => 'index'),*/
        'loginRedirect' => array('controller' => 'homes', 'action' => 'index'),
        'logoutRedirect' => array(
            'controller' => 'users',
            'action' => 'login'
            
        ),
        'authenticate' => array(
            'Form' => array(
                'passwordHasher' => 'Blowfish',
                'fields' => array(
                // 'username' => 'email', //Default is 'username' in the userModel
                  'password' => 'password'  //Default is 'password' in the userModel
                ),
                'userModel'=> 'User'
            )
        ),
        'authorize' => array('Controller') // Added this line
    )
);

/*public $components = array('Session',
        'Auth' => array('authenticate' => array('Form' => array( 'userModel' => 'Administrator',
                                                         'fields' => array(
                                                                              'username' => 'email',
                                                                              'password' => 'password'
                                                                              )
                                                            )
                                            ),
                'authorize' => array('Controller'),
                'loginAction' => array('controller' => 'administrators', 'action' => 'login'),
                'loginRedirect' => array('controller' => 'Home', 'action' => 'index'),
                'logoutRedirect' => array('controller' => 'Home', 'action' => 'index'),
          ),             
    );*/



public function isAuthorized($user) {
    // Admin peut accéder à toute action
    if (isset($user['role']) && $user['role'] === 'admin') {
        return true;
    }

    // Refus par défaut
    return false;
}


}
