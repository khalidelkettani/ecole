<?php
App::uses('AppModel', 'Model');
class Matiersprof extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
public function isOwnedBy($post, $user) {
    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
}


public $belongsTo = array(
		'Matier' => array(
			'className' => 'Matier',
			'foreignKey' => 'matires_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Prof' => array(
			'className' => 'Prof',
			'foreignKey' => 'profs_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
