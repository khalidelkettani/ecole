-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 21 Janvier 2017 à 15:27
-- Version du serveur :  10.1.8-MariaDB
-- Version de PHP :  5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ecoletest`
--

-- --------------------------------------------------------

--
-- Structure de la table `absences`
--

CREATE TABLE `absences` (
  `date` date NOT NULL,
  `id_eleve` varchar(20) NOT NULL,
  `matin` int(4) NOT NULL,
  `apres_midi` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `absences_justificatifs`
--

CREATE TABLE `absences_justificatifs` (
  `id` varchar(20) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `heure_debut` varchar(1) NOT NULL,
  `heure_fin` varchar(1) NOT NULL,
  `id_eleve` varchar(20) NOT NULL,
  `type` varchar(2) NOT NULL,
  `motif` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `accueil_news`
--

CREATE TABLE `accueil_news` (
  `id` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `cible` varchar(1) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `accueil_perso`
--

CREATE TABLE `accueil_perso` (
  `id` varchar(10) NOT NULL,
  `id_util` varchar(20) NOT NULL,
  `subpanel` varchar(50) NOT NULL,
  `titre` varchar(50) NOT NULL,
  `ordre` int(5) NOT NULL,
  `colonne` int(3) NOT NULL,
  `parametre` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `accueil_perso`
--

INSERT INTO `accueil_perso` (`id`, `id_util`, `subpanel`, `titre`, `ordre`, `colonne`, `parametre`) VALUES
('NI6DXBSQLU', '1', 'news', 'Actualités', 1, 1, 'T|5');

-- --------------------------------------------------------

--
-- Structure de la table `anneesscollaires`
--

CREATE TABLE `anneesscollaires` (
  `id` int(11) NOT NULL,
  `libelle` varchar(200) CHARACTER SET armscii8 NOT NULL,
  `cerated` datetime NOT NULL,
  `modifed` datetime NOT NULL,
  `datedebut` date NOT NULL,
  `datefin` date NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `default_year` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `anneesscollaires`
--

INSERT INTO `anneesscollaires` (`id`, `libelle`, `cerated`, `modifed`, `datedebut`, `datefin`, `user_id`, `default_year`) VALUES
(20, 'annee 2016', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2015-09-02', '2016-06-30', '3', 0),
(21, 'Ann?e 2015', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2014-09-01', '2015-06-30', '3', 0),
(23, '2013', '2016-11-04 00:00:00', '2016-11-03 00:00:00', '2016-11-01', '2016-11-01', '3', 0),
(151, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(152, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(153, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(154, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(155, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(156, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(157, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(158, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 0),
(159, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `bibliotheque`
--

CREATE TABLE `bibliotheque` (
  `id` varchar(20) NOT NULL,
  `id_cat` varchar(10) NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `reference` varchar(20) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `resume` text NOT NULL,
  `couverture` text NOT NULL,
  `editeur` varchar(100) NOT NULL,
  `collection` varchar(100) NOT NULL,
  `auteur` varchar(255) NOT NULL,
  `etat` varchar(2) NOT NULL,
  `isbn` varchar(17) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bibliotheque_emprunt`
--

CREATE TABLE `bibliotheque_emprunt` (
  `id` varchar(20) NOT NULL,
  `id_util` varchar(20) NOT NULL,
  `type_util` varchar(1) NOT NULL,
  `id_livre` varchar(20) NOT NULL,
  `date_emprunt` date NOT NULL,
  `date_retour` date NOT NULL,
  `reservation` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cahierjournal`
--

CREATE TABLE `cahierjournal` (
  `id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `id_classe` varchar(5) NOT NULL,
  `id_matiere` varchar(10) NOT NULL,
  `id_niveau` varchar(10) NOT NULL,
  `contenu` text NOT NULL,
  `parent` text NOT NULL,
  `objectifs` text NOT NULL,
  `materiel` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `classses`
--

CREATE TABLE `classses` (
  `id` int(11) NOT NULL,
  `libelle` varchar(250) CHARACTER SET armscii8 NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `capacite` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `level_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `classses`
--

INSERT INTO `classses` (`id`, `libelle`, `user_id`, `capacite`, `created`, `modified`, `level_id`) VALUES
(11, 'QSqsQS', '3', 4, '2016-11-14 19:50:52', '2016-11-14 19:50:52', 1);

-- --------------------------------------------------------

--
-- Structure de la table `competences`
--

CREATE TABLE `competences` (
  `id` varchar(20) NOT NULL,
  `id_cat` varchar(10) NOT NULL,
  `code` varchar(10) NOT NULL,
  `intitule` text NOT NULL,
  `ordre` int(10) NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `id_niveau` varchar(10) NOT NULL,
  `supprime` date NOT NULL,
  `cree` date NOT NULL,
  `statistiques` varchar(1) NOT NULL DEFAULT 'A'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `competences_categories`
--

CREATE TABLE `competences_categories` (
  `id` varchar(10) NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `id_niveau` varchar(10) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `id_parent` varchar(10) NOT NULL,
  `ordre` int(10) NOT NULL,
  `supprime` date NOT NULL,
  `cree` date NOT NULL DEFAULT '2012-01-22'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `config`
--

CREATE TABLE `config` (
  `parametre` varchar(50) NOT NULL,
  `valeur` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `config`
--

INSERT INTO `config` (`parametre`, `valeur`) VALUES
('version_de_l_application', '2.2'),
('message_connexion', ''),
('langue_choix', '1'),
('theme_choix', '1'),
('theme_defaut', 'base'),
('langue_defaut', 'fr-FR'),
('sauve_bdd', '2016-08-13'),
('cle_stat', 'ec89602bedcaa9ad3dc96225220a7d7c');

-- --------------------------------------------------------

--
-- Structure de la table `contacts_eleves`
--

CREATE TABLE `contacts_eleves` (
  `id` varchar(64) NOT NULL,
  `id_eleve` varchar(64) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `lien` varchar(255) NOT NULL,
  `adresse` text NOT NULL,
  `tel` varchar(50) NOT NULL,
  `tel2` varchar(50) NOT NULL,
  `portable` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `controles`
--

CREATE TABLE `controles` (
  `id` varchar(20) NOT NULL,
  `id_classe` varchar(5) NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `id_niveau` varchar(10) NOT NULL,
  `id_matiere` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `trimestre` varchar(2) NOT NULL,
  `descriptif` varchar(255) NOT NULL,
  `coefficient` int(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `controles_competences`
--

CREATE TABLE `controles_competences` (
  `id` varchar(20) NOT NULL,
  `id_controle` varchar(20) NOT NULL,
  `id_competence` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `controles_resultats`
--

CREATE TABLE `controles_resultats` (
  `id_eleve` varchar(20) NOT NULL,
  `id_controle` varchar(20) NOT NULL,
  `id_competence` varchar(20) NOT NULL,
  `resultat` varchar(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cooperative`
--

CREATE TABLE `cooperative` (
  `id` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `mode` varchar(1) NOT NULL,
  `ligne_comptable` varchar(5) NOT NULL,
  `piece` varchar(255) NOT NULL,
  `id_classe` varchar(5) NOT NULL,
  `montant` double(10,2) NOT NULL,
  `pointe` int(2) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `releve` varchar(255) NOT NULL,
  `banque` varchar(4) NOT NULL,
  `tiers` varchar(20) NOT NULL,
  `reference_bancaire` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cooperative2016`
--

CREATE TABLE `cooperative2016` (
  `id` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `mode` varchar(1) NOT NULL,
  `ligne_comptable` varchar(5) NOT NULL,
  `piece` varchar(255) NOT NULL,
  `id_classe` varchar(5) NOT NULL,
  `montant` double(10,2) NOT NULL,
  `pointe` int(2) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `releve` varchar(255) NOT NULL,
  `banque` varchar(4) NOT NULL,
  `tiers` varchar(10) NOT NULL,
  `reference_bancaire` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cooperative_bilan`
--

CREATE TABLE `cooperative_bilan` (
  `annee` year(4) NOT NULL,
  `clos` int(2) NOT NULL,
  `nb_200` int(4) NOT NULL,
  `nb_100` int(4) NOT NULL,
  `nb_50` int(4) NOT NULL,
  `nb_20` int(4) NOT NULL,
  `nb_10` int(4) NOT NULL,
  `nb_5` int(4) NOT NULL,
  `nb_2` int(4) NOT NULL,
  `nb_1` int(4) NOT NULL,
  `nb_05` int(4) NOT NULL,
  `nb_02` int(4) NOT NULL,
  `nb_01` int(4) NOT NULL,
  `nb_005` int(4) NOT NULL,
  `nb_002` int(4) NOT NULL,
  `nb_001` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cooperative_tiers`
--

CREATE TABLE `cooperative_tiers` (
  `id` varchar(7) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `cooperative_tiers`
--

INSERT INTO `cooperative_tiers` (`id`, `nom`) VALUES
('411-001', 'Département'),
('411-002', 'Etat'),
('411-003', 'Mairie'),
('411-004', 'Parents'),
('411-005', 'Région'),
('401-001', 'OCCE');

-- --------------------------------------------------------

--
-- Structure de la table `courriers`
--

CREATE TABLE `courriers` (
  `id` int(4) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `texte` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `courriers`
--

INSERT INTO `courriers` (`id`, `titre`, `texte`) VALUES
(1, 'Certificat de scolarit&eacute;', '<p>{LOGO_COMPLET}</p><p>&nbsp;</p><p>&nbsp;</p><p style="text-align: center;"><strong><span style="font-size: x-large;">CERTIFICAT DE SCOLARITE</span></strong></p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p>Je, soussign&eacute; {NOM_DIRECTEUR}, certifie que l''enfant :</p><p style="padding-left: 30px;"><strong>{NOM_ELEVE} {PRENOM_ELEVE}</strong></p><p style="padding-left: 30px;">n&eacute;(e) le <strong>{DATE_NAISSANCE_ELEVE}</strong> &agrave; <strong>{LIEU_NAISSANCE_ELEVE}</strong></p><p style="padding-left: 30px;">r&eacute;sidant <strong>{ADRESSE_ELEVE}</strong></p><p>&nbsp;</p><p>est inscrit(e) sur les registres de l''&eacute;cole pour l''ann&eacute;e scolaire {ANNEE_SCOLAIRE}.</p><p>&nbsp;</p><p>&nbsp;</p><p>Le {DATE_DU_JOUR}</p><p>Le directeur,</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>{NOM_DIRECTEUR}</strong></p>'),
(2, 'Certificat de radiation', '<p>{LOGO_COMPLET}</p><p>&nbsp;</p><p>&nbsp;</p><p style="text-align: center;"><strong><span style="font-size: x-large;">CERTIFICAT DE RADIATION</span></strong></p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p>Je, soussign&eacute; {NOM_DIRECTEUR}, certifie que l''enfant :</p><p style="padding-left: 30px;"><strong>{NOM_ELEVE} {PRENOM_ELEVE}</strong></p><p style="padding-left: 30px;">n&eacute;(e) le <strong>{DATE_NAISSANCE_ELEVE}</strong> &agrave; <strong>{LIEU_NAISSANCE_ELEVE}</strong></p><p style="padding-left: 30px;">r&eacute;sidant <strong>{ADRESSE_ELEVE}</strong></p><p>&nbsp;</p><p>est radi&eacute;(e) du registre des &eacute;l&egrave;ves inscrits&nbsp;de l&rsquo;&eacute;cole &agrave; compter du&nbsp;<strong>{DATE_SORTIE_ELEVE}</strong>.</p><p>.</p><p>&nbsp;</p><p>&nbsp;</p><p>Le {DATE_DU_JOUR}</p><p>Le directeur,</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>{NOM_DIRECTEUR}</strong></p>'),
(3, 'Certificat d''inscription', '<p>{LOGO_COMPLET}</p><p>&nbsp;</p><p>&nbsp;</p><p style="text-align: center;"><strong><span style="font-size: x-large;">CERTIFICAT DE SCOLARITE</span></strong></p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p>Je, soussign&eacute; {NOM_DIRECTEUR}, certifie que l''enfant :</p><p style="padding-left: 30px;"><strong>{NOM_ELEVE} {PRENOM_ELEVE}</strong></p><p style="padding-left: 30px;">n&eacute;(e) le <strong>{DATE_NAISSANCE_ELEVE}</strong> &agrave; <strong>{LIEU_NAISSANCE_ELEVE}</strong></p><p style="padding-left: 30px;">r&eacute;sidant <strong>{ADRESSE_ELEVE}</strong></p><p>&nbsp;</p><p>est inscrit(e) sur les registres de l''&eacute;cole pour l''ann&eacute;e scolaire {ANNEE_SCOLAIRE}.</p><p>&nbsp;</p><p>&nbsp;</p><p>Le {DATE_DU_JOUR}</p><p>Le directeur,</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>{NOM_DIRECTEUR}</strong></p>'),
(4, 'Certificat de fr&eacute;quentation', '<p>{LOGO_COMPLET}</p><p>&nbsp;</p><p>&nbsp;</p><p style="text-align: center;"><strong><span style="font-size: x-large;">CERTIFICAT DE FREQUENTATION</span></strong></p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p style="text-align: center;">&nbsp;</p><p>Je, soussign&eacute; {NOM_DIRECTEUR}, certifie que l''enfant :</p><p style="padding-left: 30px;"><strong>{NOM_ELEVE} {PRENOM_ELEVE}</strong></p><p style="padding-left: 30px;">n&eacute;(e) le <strong>{DATE_NAISSANCE_ELEVE}</strong> &agrave; <strong>{LIEU_NAISSANCE_ELEVE}</strong></p><p style="padding-left: 30px;">r&eacute;sidant <strong>{ADRESSE_ELEVE}</strong></p><p>&nbsp;</p><ul><li>est inscrit(e) sur les registres de l''&eacute;cole pour l''ann&eacute;e scolaire {ANNEE_SCOLAIRE}&nbsp;</li><li>fr&eacute;quente r&eacute;guli&egrave;rement la classe de <strong>{CLASSE_ELEVE}</strong>&nbsp;en <strong>{NIVEAU_ELEVE}</strong>.</li></ul><p>&nbsp;</p><p>&nbsp;</p><p>Le {DATE_DU_JOUR}</p><p>Le directeur,</p><p>&nbsp;</p><p>&nbsp;</p><p><strong>{NOM_DIRECTEUR}</strong></p>');

-- --------------------------------------------------------

--
-- Structure de la table `dates_speciales`
--

CREATE TABLE `dates_speciales` (
  `date` date NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `dates_speciales`
--

INSERT INTO `dates_speciales` (`date`, `type`) VALUES
('2011-01-01', 'ABCDEFGHIJKLM'),
('2011-05-01', 'ABCDEFGHIJKLM'),
('2011-05-08', 'ABCDEFGHIJKLM'),
('2011-07-14', 'ABCDEFGHIJKLM'),
('2011-08-15', 'ABCDEFGHIJKLM'),
('2011-11-01', 'ABCDEFGHIJKLM'),
('2011-11-11', 'ABCDEFGHIJKLM'),
('2011-12-25', 'ABCDEFGHIJKLM'),
('2011-04-25', 'ABCDEFGHIJKLM'),
('2011-06-02', 'ABCDEFGHIJKLM'),
('2011-06-13', 'ABCDEFGHIJKLM'),
('2011-05-27', 'E'),
('2011-06-10', 'F'),
('2011-04-27', 'H'),
('2011-03-05', 'J'),
('2011-04-28', 'M'),
('2011-06-29', 'M'),
('2011-07-29', 'M'),
('2012-01-01', 'ABCDEFGHIJKLM'),
('2012-05-01', 'ABCDEFGHIJKLM'),
('2012-05-08', 'ABCDEFGHIJKLM'),
('2012-07-14', 'ABCDEFGHIJKLM'),
('2012-08-15', 'ABCDEFGHIJKLM'),
('2012-11-01', 'ABCDEFGHIJKLM'),
('2012-11-11', 'ABCDEFGHIJKLM'),
('2012-12-25', 'ABCDEFGHIJKLM'),
('2012-04-09', 'ABCDEFGHIJKLM'),
('2012-05-17', 'ABCDEFGHIJKLM'),
('2012-05-28', 'ABCDEFGHIJKLM'),
('2012-03-15', 'E'),
('2012-05-27', 'E'),
('2012-06-10', 'F'),
('2012-04-27', 'H'),
('2012-03-05', 'J'),
('2012-05-18', 'L'),
('2012-04-28', 'M'),
('2012-06-29', 'M'),
('2012-07-29', 'M'),
('2013-01-01', 'ABCDEFGHIJKLM'),
('2013-05-01', 'ABCDEFGHIJKLM'),
('2013-05-08', 'ABCDEFGHIJKLM'),
('2013-07-14', 'ABCDEFGHIJKLM'),
('2013-08-15', 'ABCDEFGHIJKLM'),
('2013-11-01', 'ABCDEFGHIJKLM'),
('2013-11-11', 'ABCDEFGHIJKLM'),
('2013-12-25', 'ABCDEFGHIJKLM'),
('2013-04-01', 'ABCDEFGHIJKLM'),
('2013-05-20', 'ABCDEFGHIJKLM'),
('2013-05-09', 'ABCDEFGHIJKLM'),
('2013-03-07', 'E'),
('2013-05-27', 'E'),
('2013-06-10', 'F'),
('2013-04-27', 'H'),
('2013-03-05', 'J'),
('2013-03-29', 'J'),
('2013-05-10', 'L'),
('2013-04-28', 'M'),
('2013-06-29', 'M'),
('2013-07-29', 'M'),
('2014-01-01', 'ABCDEFGHIJKLM'),
('2014-05-01', 'ABCDEFGHIJKLM'),
('2014-05-08', 'ABCDEFGHIJKLM'),
('2014-07-14', 'ABCDEFGHIJKLM'),
('2014-08-15', 'ABCDEFGHIJKLM'),
('2014-11-01', 'ABCDEFGHIJKLM'),
('2014-11-11', 'ABCDEFGHIJKLM'),
('2014-12-25', 'ABCDEFGHIJKLM'),
('2014-04-21', 'ABCDEFGHIJKLM'),
('2014-05-29', 'ABCDEFGHIJKLM'),
('2014-06-09', 'ABCDEFGHIJKLM'),
('2014-05-27', 'E'),
('2014-06-10', 'F'),
('2014-04-27', 'H'),
('2014-03-05', 'J'),
('2014-04-18', 'J'),
('2014-04-28', 'M'),
('2014-06-29', 'M'),
('2014-07-29', 'M');

-- --------------------------------------------------------

--
-- Structure de la table `devoirs`
--

CREATE TABLE `devoirs` (
  `id` varchar(20) NOT NULL,
  `id_prof` varchar(5) NOT NULL,
  `id_classe` varchar(5) NOT NULL,
  `id_niveau` varchar(10) NOT NULL,
  `id_matiere` varchar(10) NOT NULL,
  `date_donnee` date NOT NULL,
  `date_faire` date NOT NULL,
  `contenu` text NOT NULL,
  `id_seance` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `elections_electeurs`
--

CREATE TABLE `elections_electeurs` (
  `id` varchar(50) NOT NULL,
  `id_parent` varchar(21) NOT NULL,
  `annee` year(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `elections_elus`
--

CREATE TABLE `elections_elus` (
  `id` varchar(10) NOT NULL,
  `id_parent` varchar(21) NOT NULL,
  `annee` year(4) NOT NULL,
  `type` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `eleves`
--

CREATE TABLE `eleves` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `date_naissance` datetime DEFAULT NULL,
  `lieu_naissance` int(11) NOT NULL,
  `pays_id` int(11) DEFAULT NULL,
  `adresse` text,
  `nom_pere` varchar(255) DEFAULT NULL,
  `prenom_pere` varchar(255) DEFAULT NULL,
  `civilite_pere` varchar(1) DEFAULT NULL,
  `adresse_pere` text,
  `tel_pere` varchar(50) DEFAULT NULL,
  `tel2_pere` varchar(50) DEFAULT NULL,
  `portable_pere` varchar(50) DEFAULT NULL,
  `email_pere` varchar(255) DEFAULT NULL,
  `nom_mere` varchar(255) DEFAULT NULL,
  `prenom_mere` varchar(255) DEFAULT NULL,
  `adresse_mere` text,
  `tel_mere` varchar(50) DEFAULT NULL,
  `tel2_mere` varchar(50) DEFAULT NULL,
  `portable_mere` varchar(50) DEFAULT NULL,
  `email_mere` varchar(255) DEFAULT NULL,
  `date_entree` date DEFAULT NULL,
  `date_sortie` date DEFAULT NULL,
  `recevoir_email_pere` tinyint(2) DEFAULT NULL,
  `recevoir_sms_pere` tinyint(4) DEFAULT NULL,
  `contacter_urgent_pere` tinyint(4) DEFAULT NULL,
  `recevoir_email_mere` tinyint(2) DEFAULT NULL,
  `recevoir_sms_mere` tinyint(4) DEFAULT NULL,
  `contacter_urgent_mere` tinyint(4) DEFAULT NULL,
  `contacter_urgent_autre` tinyint(4) DEFAULT NULL,
  `nom_autre` varchar(100) DEFAULT NULL,
  `prenom_autre` varchar(100) CHARACTER SET armscii8 DEFAULT NULL,
  `tel_autre` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `gsm_autre` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `description` text CHARACTER SET armscii8,
  `hopital` varchar(100) CHARACTER SET armscii8 DEFAULT NULL,
  `adresse_hopital` text CHARACTER SET armscii8,
  `tel_hopital` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `email_hopital` int(11) DEFAULT NULL,
  `tel2_hopital` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `nationalite_id` int(11) NOT NULL,
  `logo` text,
  `validated` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `eleves`
--

INSERT INTO `eleves` (`id`, `nom`, `prenom`, `sexe`, `date_naissance`, `lieu_naissance`, `pays_id`, `adresse`, `nom_pere`, `prenom_pere`, `civilite_pere`, `adresse_pere`, `tel_pere`, `tel2_pere`, `portable_pere`, `email_pere`, `nom_mere`, `prenom_mere`, `adresse_mere`, `tel_mere`, `tel2_mere`, `portable_mere`, `email_mere`, `date_entree`, `date_sortie`, `recevoir_email_pere`, `recevoir_sms_pere`, `contacter_urgent_pere`, `recevoir_email_mere`, `recevoir_sms_mere`, `contacter_urgent_mere`, `contacter_urgent_autre`, `nom_autre`, `prenom_autre`, `tel_autre`, `gsm_autre`, `email`, `description`, `hopital`, `adresse_hopital`, `tel_hopital`, `email_hopital`, `tel2_hopital`, `user_id`, `nationalite_id`, `logo`, `validated`) VALUES
(142, 'ddfhfdhgf', 'qsdqsd', 'Masculin', '2016-12-29 00:00:00', 129, NULL, 'sdqd', 'SDFSDF', 'SQDFd', NULL, 'sdsdd', 'QSDQdSD', 'sdfdsf', 'sdfsdfsd', NULL, 'sdfdsfsdsd', 'sdsd', 'sddsd', 'sdsd', 'sdsd', 'sdfdsfsdds', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', 'fgfdgdfg@ggggg.vom', '', '', '', '', NULL, '', 13, 10, 'png', 0),
(143, 'khalid', 'MOHAMED', 'Masculin', '2016-12-29 00:00:00', 129, NULL, 'sdfdsf', 'dfcqcxc', 'ahmed', NULL, 'dwwdcwx', 'zerzerzer', 'SDSD', 'zerzer', NULL, 'SAdi', 'khadija', 'wxcxwc', 'sdfsdf', 'sdfsdfsdf', 'sdfsdf', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '', '', '', '', 'hgfhgh@dop', '', '', '', '', NULL, '', 3, 10, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `eleves_classes`
--

CREATE TABLE `eleves_classes` (
  `id` varchar(20) NOT NULL,
  `id_eleve` varchar(20) NOT NULL,
  `id_classe` varchar(5) NOT NULL,
  `id_niveau` varchar(10) NOT NULL,
  `redoublement` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `email`
--

CREATE TABLE `email` (
  `id` varchar(50) NOT NULL,
  `id_expediteur` varchar(20) NOT NULL,
  `type_expediteur` varchar(1) NOT NULL,
  `id_destinataire` text NOT NULL,
  `etat` text NOT NULL,
  `id_dossier_exp` text NOT NULL,
  `id_dossier_dest` text NOT NULL,
  `titre` varchar(255) NOT NULL,
  `messagerie` text NOT NULL,
  `date` datetime NOT NULL,
  `pj` text NOT NULL,
  `pj_nom` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

CREATE TABLE `etablissement` (
  `id` int(11) NOT NULL,
  `libelle` varchar(200) CHARACTER SET armscii8 NOT NULL,
  `telephone` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `telephone2` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `portable` varchar(100) CHARACTER SET armscii8 DEFAULT NULL,
  `adresse` text CHARACTER SET armscii8,
  `pays_id` int(11) NOT NULL,
  `ville_id` int(11) NOT NULL,
  `email` varchar(100) CHARACTER SET armscii8 DEFAULT NULL,
  `lieninternet` varchar(50) CHARACTER SET armscii8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `etablissement`
--

INSERT INTO `etablissement` (`id`, `libelle`, `telephone`, `telephone2`, `portable`, `adresse`, `pays_id`, `ville_id`, `email`, `lieninternet`) VALUES
(110, 'ALAMANA', 'IUYUIY', '9879889', '97897', NULL, 1, 2, 'RE@JJ.BN', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `etablissement2016`
--

CREATE TABLE `etablissement2016` (
  `parametre` varchar(50) NOT NULL,
  `valeur` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `etablissement2016`
--

INSERT INTO `etablissement2016` (`parametre`, `valeur`) VALUES
('tel', ''),
('tel2', ''),
('email', ''),
('fax', ''),
('site_web', ''),
('matin1', '1'),
('am1', '1'),
('matin2', '1'),
('am2', '1'),
('matin3', '0'),
('am3', '0'),
('matin4', '1'),
('am4', '1'),
('matin5', '1'),
('am5', '1'),
('matin6', '0'),
('am6', '0'),
('jour_matin_debut', '08:30'),
('jour_matin_fin', '11:30'),
('jour_am_debut', '13:30'),
('jour_am_fin', '16:30'),
('circonscription', ''),
('rne', ''),
('recre_duree', '15'),
('onglet_P', 'accueil,eleves,absences,cahier,livrets,bibliotheque,email,cooperative,ged,calendrier,compte,configuration,aproposde'),
('onglet_D', 'accueil,personnels,classes,eleves,absences,cahier,livrets,bibliotheque,email,cooperative,ged,calendrier,configuration,aproposde'),
('onglet_E', 'accueil,cahier,devoirs,absences,livrets,email,ged,calendrier,compte,aproposde'),
('decoupage_livret', 'T'),
('calcul_moyenne', 'M'),
('biblio_ecole', '0'),
('biblio_duree_emprunt', '15'),
('biblio_nombre_livres', '3'),
('biblio_classe', '0'),
('biblio_duree_emprunt_classe', '15'),
('biblio_nombre_livres_classe', '3'),
('gestclasse_url', ''),
('signature_messagerie', ''),
('email_defaut', ''),
('cooperative_presente', '0'),
('cooperative_repartition', 'C'),
('cooperative_mandataires', '1'),
('signature_registre', '0'),
('signature_livrets', '0'),
('registre_livrets', '0'),
('appreciation_livrets', '0'),
('decalage_horaire', '0:00'),
('taille_max_fichier', '500K'),
('registre_decloisonnement', '0'),
('registre_livrets', '0'),
('espace_travail', '0'),
('extensions_elfinder', '7z|avi|bmp|bz|bz2|csv|doc,dot|docx|dotx|gif|jpg,jpeg,jpe|kar,mid|odp|ods|odt|pdf|png|ppt,pot,pps|pptx|potx|pub|rar|rtf|tar|tiff,tif|xls,xlt|xlsx|xltx|zip'),
('nom', 'Nom de votre école'),
('adresse', 'Son adresse'),
('couleur0', '#FFFFFF'),
('couleur1', '#FFFFFF'),
('couleur2', '#FFFFFF'),
('couleur3', ''),
('couleur4', ''),
('couleur5', ''),
('couleur6', ''),
('couleur7', ''),
('couleur8', ''),
('couleur9', ''),
('couleur_fond0', '#FF0000'),
('couleur_fond1', '#FF8000'),
('couleur_fond2', '#00FF00'),
('couleur_fond3', ''),
('couleur_fond4', ''),
('couleur_fond5', ''),
('couleur_fond6', ''),
('couleur_fond7', ''),
('couleur_fond8', ''),
('couleur_fond9', ''),
('c1', 'En cours'),
('i1', '35'),
('s1', '70'),
('c2', 'Acquis'),
('i2', '70'),
('s2', '100'),
('c3', ''),
('i3', ''),
('s3', ''),
('c4', ''),
('i4', ''),
('s4', ''),
('c5', ''),
('i5', ''),
('s5', ''),
('c6', ''),
('i6', ''),
('s6', ''),
('c7', ''),
('i7', ''),
('s7', ''),
('c8', ''),
('i8', ''),
('s8', ''),
('c9', ''),
('i9', ''),
('s9', ''),
('c0', 'Non acquis'),
('i0', '0'),
('s0', '35'),
('nom', 'Arc cile'),
('adresse', '55 avenu alal al fasi'),
('zone', 'P'),
('etendue_annee_scolaire', '1'),
('debut_annee_scolaire', '2016-01-01'),
('fin_annee_scolaire', '2016-12-31'),
('debut_P1', '2016-01-01'),
('debut_P2', '2016-03-15'),
('debut_P3', '2016-06-01'),
('debut_P4', '2016-07-15'),
('debut_P5', '2016-10-01'),
('fin_P1', '2016-03-14'),
('fin_P2', '2016-05-31'),
('fin_P3', '2016-07-14'),
('fin_P4', '2016-09-30'),
('fin_P5', '2016-12-31'),
('debut_T1', '2016-01-01'),
('debut_T2', '2016-05-01'),
('debut_T3', '2016-09-01'),
('fin_T1', '2016-04-30'),
('fin_T2', '2016-08-31'),
('fin_T3', '2016-12-31');

-- --------------------------------------------------------

--
-- Structure de la table `filaires`
--

CREATE TABLE `filaires` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) CHARACTER SET armscii8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `level_id` int(11) NOT NULL,
  `user_id` varchar(36) CHARACTER SET armscii8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `filaires`
--

INSERT INTO `filaires` (`id`, `libelle`, `created`, `modified`, `level_id`, `user_id`) VALUES
(10, 'ARABE', '2016-10-19 10:45:34', '2016-10-19 10:45:34', 0, '13'),
(11, 'MATH', '2016-10-19 10:45:42', '2016-10-19 10:45:42', 0, '13'),
(12, 'FRANCAIS', '2016-10-19 10:45:57', '2016-10-19 10:46:02', 0, '13'),
(13, 'ARABE', '2016-10-20 17:03:34', '2016-10-20 17:03:34', 0, '13'),
(14, 'Premi?re ann?e lit?raire', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, ''),
(15, 'premi?re litt?raire ', '2016-10-13 00:00:00', '2016-10-05 00:00:00', 0, '13'),
(16, 'premi?re ann?e de litt?rature ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '13'),
(17, 'premi?reann?e ', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '13'),
(19, 'dd', '2016-11-08 20:15:25', '2016-11-08 20:36:17', 1, '3'),
(20, 'Entreprise', '2016-11-08 20:35:39', '2016-11-08 20:35:39', 1, '3');

-- --------------------------------------------------------

--
-- Structure de la table `groupes`
--

CREATE TABLE `groupes` (
  `id` int(11) NOT NULL,
  `libelle` varchar(100) CHARACTER SET armscii8 NOT NULL,
  `filaire_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `groupes`
--

INSERT INTO `groupes` (`id`, `libelle`, `filaire_id`, `created`, `modified`, `user_id`) VALUES
(7, 'informatique', 12, '2016-10-21 18:31:58', '2016-10-21 18:31:58', '13'),
(11, 'sdfqsdf', 11, '2016-10-21 19:03:00', '2016-10-21 19:03:00', '13'),
(12, 'zerzer', 10, '2016-10-21 19:08:50', '2016-10-21 19:08:50', '13'),
(13, 'dsfqsd', 11, '2016-10-21 19:09:24', '2016-10-21 19:09:24', '13'),
(14, 'b', 18, '2016-11-05 23:02:07', '2016-11-05 23:02:07', '3');

-- --------------------------------------------------------

--
-- Structure de la table `horaires`
--

CREATE TABLE `horaires` (
  `id` int(10) NOT NULL,
  `du` time DEFAULT NULL,
  `au` time DEFAULT NULL,
  `periode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `horaires`
--

INSERT INTO `horaires` (`id`, `du`, `au`, `periode`) VALUES
(3, '08:00:00', '09:00:00', 'matinee'),
(4, '09:00:00', '10:00:00', 'matinee'),
(5, '10:00:00', '11:00:00', 'matinee');

-- --------------------------------------------------------

--
-- Structure de la table `inscriptions`
--

CREATE TABLE `inscriptions` (
  `id` int(11) NOT NULL,
  `eleves_id` int(11) NOT NULL,
  `classes_id` int(11) NOT NULL,
  `remarques` text,
  `code` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `annesscollaires_id` int(11) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `inscriptions`
--

INSERT INTO `inscriptions` (`id`, `eleves_id`, `classes_id`, `remarques`, `code`, `date`, `annesscollaires_id`, `user_id`, `created`, `modified`) VALUES
(1, 142, 7, NULL, '00001', '0000-00-00', 1, '3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 142, 8, NULL, '0000001', '2016-11-09', 20, '13', '2016-11-09 00:00:00', '2016-11-09 00:00:00'),
(4, 142, 0, NULL, '', '2018-03-23', 0, '3', '2016-11-24 12:51:17', '2016-11-24 12:51:17'),
(6, 142, 7, NULL, '', '2016-11-24', 20, '3', '2016-11-24 14:00:32', '2016-11-24 14:00:32'),
(7, 142, 7, NULL, '', '2016-11-24', 20, '3', '2016-11-24 14:18:17', '2016-11-24 14:18:17'),
(8, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 11:18:34', '2016-11-25 11:18:34'),
(9, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 11:18:53', '2016-11-25 11:18:53'),
(10, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 11:33:51', '2016-11-25 11:33:51'),
(11, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 11:40:37', '2016-11-25 11:40:37'),
(12, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 14:34:04', '2016-11-25 14:34:04'),
(13, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 14:36:04', '2016-11-25 14:36:04'),
(14, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 14:39:51', '2016-11-25 14:39:51'),
(15, 142, 7, NULL, '', '2016-11-25', 20, '3', '2016-11-25 14:41:02', '2016-11-25 14:41:02'),
(32, 142, 0, NULL, '', '2016-11-19', 0, '', '2016-11-29 10:59:28', '2016-11-29 10:59:28'),
(33, 142, 0, NULL, '', '2016-11-04', 0, '', '2016-11-29 11:00:32', '2016-11-29 11:00:32'),
(34, 142, 0, NULL, '', '2016-11-25', 0, '', '2016-11-29 11:02:26', '2016-11-29 11:02:26'),
(35, 142, 0, NULL, '', '2016-11-03', 0, '', '2016-11-29 11:06:42', '2016-11-29 11:06:42'),
(36, 142, 0, NULL, '', '2016-11-25', 0, '', '2016-11-29 11:09:49', '2016-11-29 11:09:49'),
(38, 142, 0, NULL, '', '2016-11-01', 0, '', '2016-11-29 11:39:19', '2016-11-29 11:39:19'),
(39, 142, 0, NULL, '', '2016-11-01', 0, '', '2016-11-29 11:42:15', '2016-11-29 11:42:15'),
(40, 142, 0, NULL, '', '2016-11-19', 0, '', '2016-11-29 11:43:16', '2016-11-29 11:43:16'),
(41, 142, 0, NULL, '', '2016-11-08', 0, '', '2016-11-29 11:43:31', '2016-11-29 11:43:31'),
(42, 142, 0, NULL, '', '2016-11-19', 0, '', '2016-11-29 11:43:41', '2016-11-29 11:43:41'),
(43, 142, 0, NULL, '', '2016-11-09', 0, '', '2016-11-29 11:47:23', '2016-11-29 11:47:23'),
(44, 142, 0, NULL, '', '2016-11-07', 0, '', '2016-11-29 11:53:09', '2016-11-29 11:53:09'),
(45, 142, 0, NULL, '', '2016-11-02', 0, '', '2016-11-29 11:54:10', '2016-11-29 11:54:10'),
(46, 142, 0, NULL, '', '2016-11-02', 0, '', '2016-11-29 12:12:05', '2016-11-29 12:12:05'),
(47, 142, 0, NULL, '', '2016-11-09', 0, '', '2016-11-29 12:45:45', '2016-11-29 12:45:45'),
(48, 142, 0, NULL, '', '2016-11-22', 0, '', '2016-11-29 12:50:31', '2016-11-29 12:50:31'),
(49, 142, 0, NULL, '', '2016-11-02', 0, '', '2016-11-29 13:56:11', '2016-11-29 13:56:11'),
(59, 142, 11, 'jhghj', '', '2016-11-30', 20, '3', '2016-11-30 13:13:37', '2016-12-06 20:43:06'),
(62, 142, 11, '', '', '2016-12-02', 20, '3', '2016-12-02 12:19:15', '2016-12-02 12:19:15');

-- --------------------------------------------------------

--
-- Structure de la table `jours`
--

CREATE TABLE `jours` (
  `id` int(11) NOT NULL,
  `jours` varchar(20) DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `jours`
--

INSERT INTO `jours` (`id`, `jours`, `checked`) VALUES
(1, 'lundi', 1),
(2, 'mardi', 1),
(3, 'mercredi', 1),
(4, 'jeudi', 1),
(5, 'vendredi', 1),
(6, 'samedi', 1),
(7, 'dimanche', 1);

-- --------------------------------------------------------

--
-- Structure de la table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `levels`
--

INSERT INTO `levels` (`id`, `libelle`, `user_id`) VALUES
(1, 'Maternelle', 3),
(2, 'Primaire', 3);

-- --------------------------------------------------------

--
-- Structure de la table `listes`
--

CREATE TABLE `listes` (
  `id` varchar(10) NOT NULL,
  `nom_liste` varchar(255) NOT NULL,
  `intitule` varchar(255) NOT NULL,
  `ordre` int(10) NOT NULL,
  `id_prof` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `listes`
--

INSERT INTO `listes` (`id`, `nom_liste`, `intitule`, `ordre`, `id_prof`) VALUES
('6', 'niveaux', 'CE1', 6, ''),
('7', 'niveaux', 'CE2', 7, ''),
('8', 'niveaux', 'CM1', 8, ''),
('9', 'niveaux', 'CM2', 9, ''),
('5', 'niveaux', 'CP', 5, ''),
('4', 'niveaux', 'GS', 4, ''),
('3', 'niveaux', 'MS', 3, ''),
('2', 'niveaux', 'PS', 2, ''),
('1', 'niveaux', 'TPS', 1, ''),
('26', 'categ_biblio_ecole', 'CD, DVD', 7, ''),
('25', 'categ_biblio_ecole', 'Livres de jeux', 6, ''),
('24', 'categ_biblio_ecole', 'Bandes dessinées', 5, ''),
('23', 'categ_biblio_ecole', 'Journaux, Magazines', 4, ''),
('22', 'categ_biblio_ecole', 'Albums', 3, ''),
('21', 'categ_biblio_ecole', 'Documentaires', 2, ''),
('20', 'categ_biblio_ecole', 'Romans Jeunesse', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `livrets_appreciation`
--

CREATE TABLE `livrets_appreciation` (
  `id` varchar(20) NOT NULL,
  `id_eleve` varchar(20) NOT NULL,
  `id_util` varchar(5) NOT NULL,
  `type_util` varchar(1) NOT NULL,
  `trimestre` varchar(2) NOT NULL,
  `annee` year(4) NOT NULL,
  `appreciation` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `matiers`
--

CREATE TABLE `matiers` (
  `id` int(11) NOT NULL,
  `libelle` varchar(100) CHARACTER SET armscii8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `matiers`
--

INSERT INTO `matiers` (`id`, `libelle`, `created`, `modified`, `user_id`) VALUES
(1, 'Mathg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '13'),
(2, 'MATH', '2016-10-12 00:00:00', '2016-10-15 00:00:00', '13');

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE `modules` (
  `id` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL,
  `cle` varchar(20) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `auteur` text,
  `supprimable` int(2) NOT NULL DEFAULT '1',
  `info` text NOT NULL,
  `fichier` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `modules`
--

INSERT INTO `modules` (`id`, `type`, `cle`, `nom`, `auteur`, `supprimable`, `info`, `fichier`) VALUES
('1', 'theme', 'base', 'Base', 'UIDesignTeam', 0, '', ''),
('2', 'subpanel', 'calculette', 'Calculette', 'DOX Conception', 0, '', ''),
('3', 'subpanel', 'blocnotes', 'Bloc-note', 'DOX Conception', 0, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `mois`
--

CREATE TABLE `mois` (
  `id` int(11) NOT NULL,
  `mois` varchar(20) DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `mois`
--

INSERT INTO `mois` (`id`, `mois`, `checked`) VALUES
(22, 'janvier', 1),
(26, 'Février', 1),
(27, 'Mars', 1),
(28, 'Avril', 1),
(29, 'Mai', 1),
(30, 'Juin', 1),
(31, 'Juillet', 1),
(32, 'Août', 1),
(33, 'Septembre', 1),
(34, 'Octobre', 1),
(35, 'Novembre', 1),
(36, 'Décembre', 1);

-- --------------------------------------------------------

--
-- Structure de la table `moyenne_classe`
--

CREATE TABLE `moyenne_classe` (
  `id_classe` varchar(5) NOT NULL,
  `id_niveau` varchar(20) NOT NULL,
  `marquage` int(4) NOT NULL,
  `periode1` decimal(5,2) NOT NULL,
  `periode2` decimal(5,2) NOT NULL,
  `periode3` decimal(5,2) NOT NULL,
  `periode4` decimal(5,2) NOT NULL,
  `periode5` decimal(5,2) NOT NULL,
  `statistiques` varchar(1) NOT NULL DEFAULT 'G'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `nationalites`
--

CREATE TABLE `nationalites` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `pay_id` int(11) DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET armscii8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `nationalites`
--

INSERT INTO `nationalites` (`id`, `libelle`, `pay_id`, `user_id`, `created`, `modified`) VALUES
(10, 'Marocaine', NULL, '13', '2016-10-19 13:33:31', '2016-10-19 13:33:31');

-- --------------------------------------------------------

--
-- Structure de la table `niveaus`
--

CREATE TABLE `niveaus` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `personnel_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `niveaus`
--

INSERT INTO `niveaus` (`id`, `libelle`, `personnel_id`) VALUES
(1, 'CE1', 0),
(2, 'ce3', 0);

-- --------------------------------------------------------

--
-- Structure de la table `param_persos`
--

CREATE TABLE `param_persos` (
  `id_prof` varchar(20) NOT NULL,
  `annee` year(4) NOT NULL,
  `id_classe_cours` varchar(5) NOT NULL,
  `theme` varchar(50) NOT NULL,
  `affiche` tinyint(2) NOT NULL,
  `niveau_en_cours` varchar(10) NOT NULL,
  `devoirs` tinyint(2) NOT NULL,
  `personnels` text NOT NULL,
  `classes` text NOT NULL,
  `eleves` text NOT NULL,
  `bibliotheque` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `param_persos`
--

INSERT INTO `param_persos` (`id_prof`, `annee`, `id_classe_cours`, `theme`, `affiche`, `niveau_en_cours`, `devoirs`, `personnels`, `classes`, `eleves`, `bibliotheque`) VALUES
('1', 2016, '', 'base', 0, '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `param_profs_plus`
--

CREATE TABLE `param_profs_plus` (
  `id_prof` varchar(5) NOT NULL,
  `parametre` varchar(50) NOT NULL,
  `valeur` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `param_profs_plus2016`
--

CREATE TABLE `param_profs_plus2016` (
  `id_prof` varchar(64) NOT NULL,
  `parametre` varchar(50) NOT NULL,
  `valeur` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `id` int(11) NOT NULL,
  `libelle` varchar(245) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pays`
--

INSERT INTO `pays` (`id`, `libelle`, `user_id`, `created`, `modified`) VALUES
(30, 'MAROC', 13, '2016-10-19 11:27:03', '2016-10-19 11:27:03'),
(31, 'MAROC', 3, '2016-11-05 09:49:20', '2016-11-08 20:11:21'),
(32, 'dgfdfgdfgdfgdfg', 3, '2016-11-08 20:11:29', '2016-11-08 20:11:29');

-- --------------------------------------------------------

--
-- Structure de la table `profs`
--

CREATE TABLE `profs` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `civilite` varchar(10) NOT NULL,
  `matier_id` int(11) DEFAULT NULL,
  `adresse` text,
  `tel` varchar(50) NOT NULL,
  `tel2` varchar(50) NOT NULL,
  `portable` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_naissance` date NOT NULL,
  `type` varchar(2) NOT NULL,
  `date_entree` date NOT NULL,
  `date_sortie` date NOT NULL,
  `infos_compl` text NOT NULL,
  `recevoir_email` int(2) NOT NULL,
  `user_id` varchar(36) CHARACTER SET armscii8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `profs`
--

INSERT INTO `profs` (`id`, `nom`, `prenom`, `civilite`, `matier_id`, `adresse`, `tel`, `tel2`, `portable`, `email`, `date_naissance`, `type`, `date_entree`, `date_sortie`, `infos_compl`, `recevoir_email`, `user_id`) VALUES
(4, 'sdfsdf', 'sdfsdf', 'Mr', 1, '', '', '', 'dfgd', 'SD@SD.com', '0000-00-00', '2', '2036-01-01', '0000-00-00', '', 0, '13'),
(5, 'AZIZE', 'ZEZE', 'Mr', 2, 'QSDQSD', '', '', 'ZEZE', 'SD@SD.com', '0000-00-00', '2', '2036-01-01', '0000-00-00', '', 0, '13'),
(6, 'sqdqsdf', 'qsdfsqdf', 'Mlle', 1, 'wdcwdfdf', '', '', 'dsfdsf', 'SD@SD.com', '0000-00-00', '3', '2016-10-28', '0000-00-00', '', 0, '13');

-- --------------------------------------------------------

--
-- Structure de la table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `libelle` varchar(245) NOT NULL,
  `pay_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modifed` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `regions`
--

INSERT INTO `regions` (`id`, `libelle`, `pay_id`, `user_id`, `created`, `modifed`) VALUES
(34, 'RABAT NORD', 29, 13, '2016-10-18 12:03:36', '0000-00-00 00:00:00'),
(35, 'RABAT NORD', 31, 3, '2016-11-05 09:49:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `reunions`
--

CREATE TABLE `reunions` (
  `id` varchar(15) NOT NULL,
  `date` date NOT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  `resume` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `id_util` text NOT NULL,
  `type` varchar(1) NOT NULL,
  `id_saisie` varchar(21) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `salles`
--

CREATE TABLE `salles` (
  `id` int(11) NOT NULL,
  `libelle` varchar(100) CHARACTER SET armscii8 NOT NULL,
  `capacite` int(11) NOT NULL,
  `equipements` text CHARACTER SET armscii8,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` varchar(36) CHARACTER SET armscii8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `salles`
--

INSERT INTO `salles` (`id`, `libelle`, `capacite`, `equipements`, `created`, `modified`, `user_id`) VALUES
(20, 'TP', 22, 'SDFSFF\r\nSDFSDFDS\r\nFSDFSDFSDF\r\nSDFDSFD\r\nF', '2016-10-19 11:12:08', '2016-10-19 11:12:08', '13');

-- --------------------------------------------------------

--
-- Structure de la table `sauve_vue`
--

CREATE TABLE `sauve_vue` (
  `id` varchar(20) NOT NULL,
  `id_util` varchar(5) NOT NULL,
  `module` varchar(20) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `recherche` text NOT NULL,
  `colonne` text NOT NULL,
  `autre_param` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `signatures`
--

CREATE TABLE `signatures` (
  `id` varchar(20) NOT NULL,
  `id_util` varchar(20) NOT NULL,
  `type_util` varchar(1) NOT NULL,
  `date` date NOT NULL,
  `parametre` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `statuts`
--

CREATE TABLE `statuts` (
  `id` int(11) NOT NULL,
  `libelle` varchar(100) CHARACTER SET armscii8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `taches`
--

CREATE TABLE `taches` (
  `id` varchar(50) NOT NULL,
  `id_util` varchar(20) NOT NULL,
  `type_util` varchar(1) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `tache` text NOT NULL,
  `priorite` varchar(1) NOT NULL,
  `etat` varchar(1) NOT NULL,
  `date_echeance` date NOT NULL,
  `heure_echeance` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `typesprofs`
--

CREATE TABLE `typesprofs` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) CHARACTER SET armscii8 DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET armscii8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `typesprofs`
--

INSERT INTO `typesprofs` (`id`, `libelle`, `user_id`, `created`, `modified`) VALUES
(2, 'VACATAIRE', '13', '2016-10-20 18:23:46', '2016-10-20 21:37:52'),
(3, 'PERMANENT', '13', '2016-10-20 21:35:54', '2016-10-20 21:35:54'),
(4, 'PERMANENT', '13', '2016-10-21 14:41:53', '2016-10-21 14:41:53');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(128) CHARACTER SET armscii8 DEFAULT NULL,
  `role` varchar(20) CHARACTER SET armscii8 DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `logo` varchar(225) CHARACTER SET armscii8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `email`, `created`, `modified`, `logo`) VALUES
(3, 'azize', '$2a$10$NoZIDNO2I5R8Sksp7Y.PrOfztrJvb1zz7lWx0.O8xGSRdVeOYgEuK', 'admin', 'youssefmaghfour@gmail.com', '2016-11-01 11:16:23', '2016-11-01 16:31:58', 'png'),
(4, 'khalid', '$2a$10$eliG.70ygck89zvnJoaWk.jlw6hHcjfZTt.ohNz7MSBkbPcfbsBMO', 'admin', '', '2016-11-01 11:42:10', '2016-11-01 11:42:10', NULL),
(5, 'ali', '$2a$10$jdUoHZJCXX6qhaaYaYeWx.N3mFr4TWv07VOm60evjSq5a4jXs2MrW', 'admin', 'aa@aa.com', '2016-11-01 11:56:10', '2016-11-03 09:33:56', 'png'),
(6, 'hassan', '$2a$10$ECP3vG2Pg3YQViWAMPr0NuqUthmyEk4dash1c8UOp7RtT2cMrAwIq', 'admin', 'aa@aa.com', '2016-11-01 11:56:56', '2016-11-01 11:56:56', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `vacances`
--

CREATE TABLE `vacances` (
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `zone` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `vacances`
--

INSERT INTO `vacances` (`date_debut`, `date_fin`, `zone`) VALUES
('2010-12-18', '2011-01-02', 'A'),
('2011-02-26', '2011-03-13', 'A'),
('2011-04-23', '2011-05-08', 'A'),
('2011-07-02', '2011-09-04', 'A'),
('2011-10-23', '2011-11-02', 'A'),
('2011-12-18', '2012-01-02', 'A'),
('2012-02-12', '2012-02-26', 'A'),
('2012-04-08', '2012-04-22', 'A'),
('2012-07-06', '2012-09-03', 'A'),
('2012-10-28', '2012-11-11', 'A'),
('2012-12-23', '2013-01-06', 'A'),
('2013-02-24', '2013-03-10', 'A'),
('2013-04-21', '2013-05-05', 'A'),
('2013-07-05', '2013-09-01', 'A'),
('2010-12-18', '2011-01-02', 'B'),
('2011-02-19', '2011-03-06', 'B'),
('2011-04-16', '2011-05-01', 'B'),
('2011-07-02', '2011-09-04', 'B'),
('2011-10-23', '2011-11-02', 'B'),
('2011-12-18', '2012-01-02', 'B'),
('2012-02-26', '2012-03-11', 'B'),
('2012-04-22', '2012-05-06', 'B'),
('2012-07-06', '2012-09-03', 'B'),
('2012-10-28', '2012-11-11', 'B'),
('2012-12-23', '2013-01-06', 'B'),
('2013-02-17', '2013-03-03', 'B'),
('2013-04-14', '2013-04-28', 'B'),
('2013-07-05', '2013-09-01', 'B'),
('2010-12-18', '2011-01-02', 'C'),
('2011-02-12', '2011-02-27', 'C'),
('2011-04-09', '2011-04-25', 'C'),
('2011-07-02', '2011-09-04', 'C'),
('2011-10-23', '2011-11-02', 'C'),
('2011-12-18', '2012-01-02', 'C'),
('2012-02-19', '2012-03-04', 'C'),
('2012-04-15', '2012-04-29', 'C'),
('2012-07-06', '2012-09-03', 'C'),
('2012-10-28', '2012-11-11', 'C'),
('2012-12-23', '2013-01-06', 'C'),
('2013-03-03', '2013-03-17', 'C'),
('2013-04-28', '2013-05-12', 'C'),
('2013-07-05', '2013-09-01', 'C'),
('2011-07-07', '2011-09-08', 'D'),
('2011-10-23', '2011-11-01', 'D'),
('2011-12-18', '2012-01-02', 'D'),
('2012-02-19', '2012-03-04', 'D'),
('2012-04-22', '2012-05-06', 'D'),
('2012-07-08', '2012-09-09', 'D'),
('2012-10-28', '2012-11-05', 'D'),
('2012-12-23', '2013-01-06', 'D'),
('2013-02-17', '2013-03-03', 'D'),
('2013-04-14', '2013-04-28', 'D'),
('2013-07-06', '2013-09-01', 'D'),
('2011-07-07', '2011-09-04', 'E'),
('2011-10-23', '2011-11-02', 'E'),
('2011-12-18', '2012-01-02', 'E'),
('2012-02-19', '2012-02-26', 'E'),
('2012-04-01', '2012-04-15', 'E'),
('2012-05-17', '2012-05-22', 'E'),
('2012-07-06', '2012-09-03', 'E'),
('2012-10-28', '2012-11-07', 'E'),
('2012-12-23', '2013-01-06', 'E'),
('2013-02-10', '2013-02-17', 'E'),
('2013-03-24', '2013-04-07', 'E'),
('2013-05-29', '2013-05-29', 'E'),
('2013-07-03', '2013-09-01', 'E'),
('2011-07-07', '2011-09-04', 'F'),
('2011-10-23', '2011-11-02', 'F'),
('2011-12-18', '2012-01-02', 'F'),
('2012-02-19', '2012-02-29', 'F'),
('2012-04-06', '2012-04-09', 'F'),
('2012-04-22', '2012-05-08', 'F'),
('2012-07-05', '2012-09-04', 'F'),
('2012-10-28', '2012-11-07', 'F'),
('2012-12-23', '2013-01-06', 'F'),
('2013-02-10', '2013-02-20', 'F'),
('2013-03-29', '2013-04-14', 'F'),
('2013-05-08', '2013-05-12', 'F'),
('2013-07-03', '2013-09-01', 'F'),
('2011-07-07', '2011-09-04', 'G'),
('2011-10-23', '2011-11-02', 'G'),
('2011-12-18', '2012-01-02', 'G'),
('2012-02-16', '2012-02-26', 'G'),
('2012-04-05', '2012-04-18', 'G'),
('2012-05-17', '2012-05-22', 'G'),
('2012-07-06', '2012-09-03', 'G'),
('2012-10-28', '2012-11-07', 'G'),
('2012-12-21', '2013-01-02', 'G'),
('2013-02-10', '2013-02-24', 'G'),
('2013-03-28', '2013-04-10', 'G'),
('2013-05-19', '2013-05-22', 'G'),
('2013-07-05', '2013-09-01', 'G'),
('2011-07-07', '2011-08-23', 'H'),
('2011-10-02', '2011-10-16', 'H'),
('2011-12-14', '2012-01-10', 'H'),
('2012-03-04', '2012-03-18', 'H'),
('2012-04-29', '2012-05-07', 'H'),
('2012-07-06', '2012-08-21', 'H'),
('2012-10-07', '2012-10-21', 'H'),
('2012-12-19', '2013-01-15', 'H'),
('2013-03-03', '2013-03-17', 'H'),
('2013-05-01', '2013-05-12', 'H'),
('2013-07-07', '2013-09-01', 'H'),
('2011-12-18', '2012-02-15', 'I'),
('2012-04-01', '2012-04-08', 'I'),
('2012-05-20', '2012-06-03', 'I'),
('2012-07-14', '2012-07-22', 'I'),
('2012-09-02', '2012-09-16', 'I'),
('2012-10-28', '2012-11-04', 'I'),
('2012-12-16', '2013-02-13', 'I'),
('2013-04-07', '2013-04-21', 'I'),
('2013-06-09', '2013-06-23', 'I'),
('2013-08-11', '2013-08-25', 'I'),
('2013-10-13', '2013-10-27', 'I'),
('2013-12-20', '2014-02-16', 'I'),
('2014-04-06', '2014-04-20', 'I'),
('2014-06-07', '2014-06-22', 'I'),
('2014-08-10', '2014-08-24', 'I'),
('2014-10-12', '2014-10-26', 'I'),
('2014-12-19', '2015-02-16', 'I'),
('2011-07-07', '2011-08-17', 'J'),
('2011-09-25', '2011-09-30', 'J'),
('2011-11-06', '2011-11-13', 'J'),
('2011-12-18', '2012-01-15', 'J'),
('2012-02-19', '2012-02-26', 'J'),
('2012-04-01', '2012-04-15', 'J'),
('2012-06-24', '2012-08-15', 'J'),
('2012-09-23', '2012-09-30', 'J'),
('2012-11-04', '2012-11-11', 'J'),
('2012-12-16', '2013-01-13', 'J'),
('2013-02-17', '2013-02-24', 'J'),
('2013-03-31', '2013-04-14', 'J'),
('2013-06-23', '2013-08-15', 'J'),
('2013-09-22', '2013-09-29', 'J'),
('2013-11-03', '2013-11-11', 'J'),
('2013-12-15', '2014-01-12', 'J'),
('2014-02-16', '2014-02-23', 'J'),
('2014-03-30', '2014-04-13', 'J'),
('2014-06-22', '2014-08-12', 'J'),
('2011-07-07', '2011-08-17', 'K'),
('2011-10-02', '2011-10-16', 'K'),
('2011-12-18', '2012-01-22', 'K'),
('2012-03-04', '2012-03-18', 'K'),
('2012-05-06', '2012-05-20', 'K'),
('2012-07-08', '2012-08-15', 'K'),
('2012-10-07', '2012-10-21', 'K'),
('2012-12-16', '2013-01-20', 'K'),
('2013-03-03', '2013-03-17', 'K'),
('2013-05-05', '2013-05-15', 'K'),
('2013-07-07', '2013-08-18', 'K'),
('2013-10-10', '2013-10-23', 'K'),
('2013-12-19', '2014-01-19', 'K'),
('2014-03-06', '2014-03-19', 'K'),
('2014-05-09', '2014-05-18', 'K'),
('2014-07-06', '2014-08-12', 'K'),
('2011-07-07', '2011-08-23', 'L'),
('2011-10-29', '2011-11-06', 'L'),
('2011-12-17', '2012-01-02', 'L'),
('2012-02-15', '2012-02-26', 'L'),
('2012-04-14', '2012-04-29', 'L'),
('2012-07-04', '2012-09-10', 'L'),
('2012-10-27', '2012-11-04', 'L'),
('2012-12-22', '2013-01-06', 'L'),
('2013-02-16', '2013-02-26', 'L'),
('2013-04-13', '2013-04-28', 'L'),
('2013-07-03', '2013-09-01', 'L'),
('2011-12-18', '2012-02-13', 'M'),
('2012-03-31', '2012-04-08', 'M'),
('2012-05-18', '2012-06-03', 'M'),
('2012-07-14', '2012-07-22', 'M'),
('2012-09-01', '2012-09-16', 'M'),
('2012-10-27', '2012-11-04', 'M'),
('2012-12-15', '2013-02-11', 'M'),
('2013-03-30', '2013-04-07', 'M'),
('2013-05-18', '2013-06-02', 'M'),
('2013-07-13', '2013-07-21', 'M'),
('2013-08-29', '2013-09-15', 'M'),
('2013-12-14', '2014-02-16', 'M');

-- --------------------------------------------------------

--
-- Structure de la table `villes`
--

CREATE TABLE `villes` (
  `id` int(11) NOT NULL,
  `libelle` varchar(245) NOT NULL,
  `region_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `villes`
--

INSERT INTO `villes` (`id`, `libelle`, `region_id`, `created`, `modified`, `user_id`) VALUES
(129, 'RABAT', 34, '2016-10-18 13:02:55', '2016-10-18 13:21:30', '13'),
(130, 'RABAT', 35, '2016-11-05 09:49:50', '2016-11-05 09:49:50', '3');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `absences_justificatifs`
--
ALTER TABLE `absences_justificatifs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `accueil_news`
--
ALTER TABLE `accueil_news`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `anneesscollaires`
--
ALTER TABLE `anneesscollaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bibliotheque`
--
ALTER TABLE `bibliotheque`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `bibliotheque_emprunt`
--
ALTER TABLE `bibliotheque_emprunt`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `cahierjournal`
--
ALTER TABLE `cahierjournal`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `classses`
--
ALTER TABLE `classses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `competences_categories`
--
ALTER TABLE `competences_categories`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `contacts_eleves`
--
ALTER TABLE `contacts_eleves`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `cooperative`
--
ALTER TABLE `cooperative`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `cooperative2016`
--
ALTER TABLE `cooperative2016`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `cooperative_tiers`
--
ALTER TABLE `cooperative_tiers`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `courriers`
--
ALTER TABLE `courriers`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `elections_electeurs`
--
ALTER TABLE `elections_electeurs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `elections_elus`
--
ALTER TABLE `elections_elus`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `eleves`
--
ALTER TABLE `eleves`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `eleves_classes`
--
ALTER TABLE `eleves_classes`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `email`
--
ALTER TABLE `email`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `filaires`
--
ALTER TABLE `filaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groupes`
--
ALTER TABLE `groupes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `horaires`
--
ALTER TABLE `horaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `inscriptions`
--
ALTER TABLE `inscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `jours`
--
ALTER TABLE `jours`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `listes`
--
ALTER TABLE `listes`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `livrets_appreciation`
--
ALTER TABLE `livrets_appreciation`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `matiers`
--
ALTER TABLE `matiers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `modules`
--
ALTER TABLE `modules`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `mois`
--
ALTER TABLE `mois`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `nationalites`
--
ALTER TABLE `nationalites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_id` (`pay_id`);

--
-- Index pour la table `niveaus`
--
ALTER TABLE `niveaus`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `profs`
--
ALTER TABLE `profs`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pay_id` (`pay_id`);

--
-- Index pour la table `reunions`
--
ALTER TABLE `reunions`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `salles`
--
ALTER TABLE `salles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sauve_vue`
--
ALTER TABLE `sauve_vue`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `signatures`
--
ALTER TABLE `signatures`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `statuts`
--
ALTER TABLE `statuts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `taches`
--
ALTER TABLE `taches`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `typesprofs`
--
ALTER TABLE `typesprofs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `villes`
--
ALTER TABLE `villes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_villes_pays2_idx` (`region_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `anneesscollaires`
--
ALTER TABLE `anneesscollaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT pour la table `classses`
--
ALTER TABLE `classses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `courriers`
--
ALTER TABLE `courriers`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `eleves`
--
ALTER TABLE `eleves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT pour la table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT pour la table `filaires`
--
ALTER TABLE `filaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `groupes`
--
ALTER TABLE `groupes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `horaires`
--
ALTER TABLE `horaires`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `inscriptions`
--
ALTER TABLE `inscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT pour la table `jours`
--
ALTER TABLE `jours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `matiers`
--
ALTER TABLE `matiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `mois`
--
ALTER TABLE `mois`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `nationalites`
--
ALTER TABLE `nationalites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `niveaus`
--
ALTER TABLE `niveaus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `profs`
--
ALTER TABLE `profs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT pour la table `salles`
--
ALTER TABLE `salles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `statuts`
--
ALTER TABLE `statuts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `typesprofs`
--
ALTER TABLE `typesprofs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `villes`
--
ALTER TABLE `villes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
