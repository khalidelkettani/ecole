


<?php echo $this->Form->create('Nationalite'); ?>
<?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Modifier une nationalité'); ?></h4>
				</div>
				<div class="modal-body">
						<div class="form-group">
							<label><?php echo __('Nom de la nationalité'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de la nationalité'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Nom de pays'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('pay_id',$pays,array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div>				
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
<?php echo $this->Form->end(); ?>
