<?php
App::uses('LevelsController', 'Controller');

/**
 * LevelsController Test Case
 */
class LevelsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.level'
	);

}
