<?php
App::uses('AppModel', 'Model');
/**
 * Ville Model
 *
 * @property Pay $Pay
 * @property Company $Company
 * @property Filiale $Filiale
 */
class Prof extends AppModel {
	
	public function isOwnedBy($post, $user) {
    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
}

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nom' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	public $belongsTo = array(
		'Matier' => array(
			'className' => 'Matier',
			'foreignKey' => 'matier_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
			),

		'Typesprof' => array(
			'className' => 'Typesprof',
			'foreignKey' => 'type',
			'conditions' => '',
			'fields' => '',
			'order' => ''
			),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);



}
