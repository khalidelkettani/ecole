
<div class="row">
	<div class="col-lg-12">
							
		<div id="email-box" class="clearfix">
			<div class="row">
				<div class="col-lg-12">
					<header id="email-header" class="clearfix">
						<ol class="breadcrumb">
							<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/dashboard')); ?>"><?php echo __('Accueil');?></a></li>
							<li><span><?php echo __('ADMIN');?></span></li>
							<li><span><?php echo __('Paramétrages');?></span></li>
							<li class="active"><span><?php echo __('Géneral');?></span></li>
						</ol>

					</header>
				</div>
			</div>
								
			<div class="row">
				<div class="col-lg-12">
					<div id="email-navigation" class="email-nav-nano hidden-xs hidden-sm">
					<?php echo  $this->element('rhq/nav_pim_user'); ?>
					<div id="email-content" class="email-content-nano clearfix">
						<div class="email-content-nano-content">
							
							<div class="row">
								<div class="col-lg-12">
									<h3><span><?php echo __('Géneral'); ?></span></h3>
									<div class="col-lg-6">	
										<div class="form-group">
											<label><?php echo __('Année scollaire par defaut'); ?> <span class="f_req">*</span></label>
										<!--<?php	echo $this->Form->select('libelle', $annees, array('class' => 'form-control', 'empty' =>__('Sélectionnez une année'), 'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis'))); ?>-->
										 <?php  echo $this->Form->input( __('Année scollaire'), array(
                              	'class' => 'form-control  ch_filtrer filtrer', 'id'=>'annee','required' => false,
      'options' => array($annees),
      //'empty' => $options,
      'value'=> $options
      
  ));?>
										</div>
										<div class="form-group">
											<label><?php echo __('Mois de l\'année scollaire'); ?> <span class="f_req">*</span></label>
											<?php echo $this->Form->input('lastname',array('class'=>'form-control','label'=> false, 'type' =>'text', 'placeholder'=>__('Prénom'), 'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
										</div>
										<div class="form-group">
											<label><?php echo __('Les jours féries'); ?> <span class="f_req">*</span></label>

											<?php $sexes=array('Masculin'=>__('Masculin'),'Feminin'=>__('Feminin'));
											echo $this->Form->select('sexe', $sexes, array('class' => 'form-control', 'empty' =>__('Sélectionnez le sexe'), 'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis'))); ?>
										</div>
										<div class="form-group">
											<label><?php echo __('Les jours de la semaine'); ?> <span class="f_req">*</span></label>

											<?php 
											$sexes=array('M'=>__('Marié'),'C'=>__('Celibataire'),'A'=>__('Autre'));
											echo $this->Form->select('etat_civile', $sexes, array('class' => 'form-control', 'empty' =>__('Sélectionnez l\'état civile'), 'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis'))); ?>
										</div>
										<div class="form-group">
											<label><?php echo __('l\'horaire du travail'); ?> </label>
											<?php echo $this->Form->input('num_permis',array('class'=>'form-control','label'=> false, 'type' =>'text', 'placeholder'=>__('Numéro du permis de conduire'), 'data-parsley-required'=>false,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
										</div>
										
									</div>
									
									
										
									</div>
								</div>
								
								</div>

								<div class="col-lg-11" style="text-align:right">
										<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'index')); ?>"><?php echo __('ANNULER'); ?></a>
									<button type="submit" class="btn btn-primary btn-lg" id="btn-loading-demo"><?php echo __('ENREGISTRER'); ?>
									</button>
								</div>
								<!--<?php echo  $this->Form->end(); ?>-->
								

																						
								
								
							</div>
								</div>
								

								</div>
								</div>
							</div>
						</div>
					</div>
				

<div id="listeDiv"></div>

<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		$('#datepickerDate1').datepicker({
		  format: 'yyyy-mm-dd'
		});
		$('#datepickerDate2').datepicker({
		  format: 'yyyy-mm-dd'
		});

		$('#sel2').select2();
	});
</script>

	<!--<script>
    $(function () {
        $("#annee").bind('input', function () {
            $.ajax({
                url: "/anneesscollaires/edit_annee_defaut",
                data: {
                    prix: $("#annee").val()
                },
                dataType: 'html',
                type: 'post',
                success: function (html) {
                   $("#listeDiv").html(html);
                }
            })
        });
    })
</script>-->
