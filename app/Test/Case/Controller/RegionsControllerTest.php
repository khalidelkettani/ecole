<?php
App::uses('RegionsController', 'Controller');

/**
 * RegionsController Test Case
 */
class RegionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.region',
		'app.pay',
		'app.ville'
	);

}
