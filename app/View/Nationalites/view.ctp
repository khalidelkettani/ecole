<div class="nationalites view">
<h2><?php echo __('Nationalite'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($nationalite['Nationalite']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($nationalite['Nationalite']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($nationalite['User']['id'], array('controller' => 'users', 'action' => 'view', $nationalite['User']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Nationalite'), array('action' => 'edit', $nationalite['Nationalite']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Nationalite'), array('action' => 'delete', $nationalite['Nationalite']['id']), array(), __('Are you sure you want to delete # %s?', $nationalite['Nationalite']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Nationalites'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Nationalite'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
