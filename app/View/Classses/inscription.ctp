<?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>"><?php echo __('Accueil');?></a></li>
			<li><span><?php echo __('Traitements');?></span></li>
			<li><span><?php echo __('Elèves');?></span></li>
			
			<li class="active"><span><?php echo __('Inscriptions');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des classes');?><small></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left"><?php echo __('Statistique des classes');?></h2>
					
						<div class="form-group">
							<div class="col-lg-12">
							<div class="col-lg-6">
								<!--<label><?php echo __('Année scollaire '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                                <?php echo $this->Form->select('libelle',$annees,array( 'empty' =>array('0'=>$options),'class' => 'form-control id filtrer','data-parsley-required'=>true,'data-parsley-group'=>'block1' ,'id'=>'sel2','data-parsley-error-message' => __('Ce champ est requis')));?>-->
                               <div class="form-group">

                              <?php  echo $this->Form->input( __('Année scollaire '), array(
                              	'class' => 'form-control ch_filtrer filtrer', 'required' => false,
      'options' => array($annees),
      //'empty' => $options,
      'value'=> $ida
      
  ));?>

						<!--!<?php echo $this->Form->select('libelle', $annees,array('class' => 'form-control ch_filtrer filtrer', 'required' => false)); ?>-->
					</div>

							</div>
						   
					</div>
					<div class="col-lg-12">
						<div class="filter-block pull-right">
						<!-- <div class="form-group pull-left">
							<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div> -->			
						<!-- <a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouvelle classe');?>
							
						</a> -->
					</div>

					</div>
				
			</header>
										
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
					<thead>
						<tr>
							<th><?php echo __('Classe ');?></th>
							<th><?php echo __('Niveau ');?></th>
							<!-- <th><?php echo __('Filiére ');?></th> -->
							<th><?php echo __('Capacité ');?></th>
							<th><?php echo __('Nombre d\'élève');?></th>
							<th><?php echo __('Taux de remplissage');?></th>					
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($classses as $var): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($var['Classs']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Level']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Classs']['capacite']); ?>&nbsp;
							</td>
							<td>
								<?php $quantity = $this->requestAction(array(
									'controller' => 'classses',
									'action' => 'getRowCout'),
									array('id' => $var['Classs']['id'],'ido'=>$ida));

                                 $Half=$var['Classs']['capacite']/'2';
                                 $pourcentage=$quantity*100/$var['Classs']['capacite'];
$pourcentage=$this->Number->precision($pourcentage, 2);

                                 if ($quantity >  $Half){
                                 	$val='J';
							    } else if($quantity <=  $Half){
							    	$val='V';
							    }else if($quantity =  $Half){
							    	$val='R';
							    }

							    ?>
							  
							    <span <?php if ($val='V'){ 
							    	echo'class="label label-success"';
							   	}else if ($val='J'){
							    	echo'class="label label-warning"';
							    
							    }else{
							    	echo'class="label label-danger"';
							    }
							    
							    ?>> 
                                 <?php echo h($quantity);?>&nbsp;
								</span> 
			

							</td>
							<td>
								<span <?php if ($val='V'){ 
							    	echo'class="label label-success"';
							   	}else if ($val='J'){
							    	echo'class="label label-warning"';
							    
							    }else{
							    	echo'class="label label-danger"';
							    }
							    
							    ?>> 
                                 <?php echo h($pourcentage);?>&nbsp;
								</span> 
							</td>						
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-search-plus fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'inscriptions','action'=>'index', $var['Classs']['id'],$ida,$pourcentage,
								'conditions'=>array('id_Classe'=>$var['Classs']['id'])), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<!--<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Classs']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Classs']['libelle'])); ?>-->
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo $this->element('paginate'); ?>
			</div>
		</div>
	</div>
</div>
</div>
	

<!--  Model form add -->

	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Ajouter une classe'); ?></h4>
				</div>
				<div class="modal-body">

				<!--<?php echo $this->Form->create('Salle',array('role'=>'form','data-parsley-validate'=>'data-parsley-validate','action'=>'add','data-validate'=>'parsley')); ?>-->
				<?php echo $this->Form->create('Classs',array('url'=> array('controller' => 'classses','action' => 'add' ) )); ?>
						<div class="form-group">
							<label><?php echo __('Nom de la classe'); ?> *</label>
							<?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de la classe'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group">
							<label><?php echo __('Capacité de la classe'); ?> *</label>
							<?php echo $this->Form->input('capacite',array('class'=>'form-control','div'=>false,'placeholder'=>__('Capacité de la classe'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
												

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				<?php echo $this->Form->end(); ?>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<!--  Model form edit -->
<div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-
hidden="true">

</div>


	<script type="text/javascript">
	$(document).ready(function(){
		  
			$(".filtrer").change(function(){
			
			var ido =$('.ch_filtrer').val();
			 var params=ido;
			/*var url = '<?php echo Router::url(array('controller'=>'classses','action'=>'filterbyannee')) ?>'+params;*/
			var url = "<?php echo $this->webroot;?>classses/filterbyannee/"+params;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
					$('#loaddata').html('');
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	

	$(function($) {
		$('#sel2').select2();
	});
});
	</script>
	<!--<script src="js/demo-skin-changer.js"></script>  
	
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/jquery.nanoscroller.min.js"></script>
	
	<script src="js/demo.js"></script> 
	
	
	<script src="js/wizard.js"></script>
	<script src="js/jquery.maskedinput.min.js"></script>
	
	
	<script src="js/scripts.js"></script>
	<script src="js/pace.min.js"></script>
	*/-->
	
	<!-- this page specific inline scripts -->
	<script>
	$(function () {
		$('#myWizard').wizard();
		
		//masked inputs
		$("#maskedDate").mask("99/99/9999");
		$("#maskedPhone").mask("(999) 999-9999");
		$("#maskedPhoneExt").mask("(999) 999-9999? x99999");
	});
	</script>
	