<?php
App::uses('AppController', 'Controller');
/**
 * Levels Controller
 */
class LevelsController extends AppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	//public $scaffold;
public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des régions'));
		//$this->Paginator->settings = $this->paginate;
/**
 * index method
 *
 * pays variable
 *Pay table 
 */
		$this->Level->recursive = 0;
		$data = $this->Paginator->paginate('Level');
    	$this->set('levels', $data);
    	//$this->set('pays', arry(contain' => array('Pays')));
    
	}

public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Level->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}
