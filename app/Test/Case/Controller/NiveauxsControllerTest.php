<?php
App::uses('NiveauxsController', 'Controller');

/**
 * NiveauxsController Test Case
 */
class NiveauxsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.niveaux',
		'app.class',
		'app.classes_niveaux'
	);

}
