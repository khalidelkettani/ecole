
<table class="table">

					<thead>
						<tr>
							<th><?php echo __('Classes ');?></th>
							<th><?php echo __('Niveau ');?></th>
							<!-- <th><?php echo __('Filiére ');?></th> -->
							<th><?php echo __('Capacité ');?></th>
							<th><?php echo __('Nombre d\'élève');?></th>
							<th><?php echo __('Taux de remplissage');?></th>					
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($classses as $var): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($var['Classs']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Level']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Classs']['capacite']); ?>&nbsp;
							</td>
							<td>
								<?php $quantity = $this->requestAction(array(
									'controller' => 'classses',
									'action' => 'getRowCout'),
									array('id' => $var['Classs']['id'],'ido' => $ido));

                                 $Half=$var['Classs']['capacite']/'2';
                                 $pourcentage=$quantity*100/$var['Classs']['capacite'];
                                 $pourcentage=$this->Number->precision($pourcentage, 2 );

                                 if ($quantity >  $Half){
                                 	$val='J';
							    } else if($quantity <=  $Half){
							    	$val='V';
							    }else if($quantity =  $Half){
							    	$val='R';
							    }

							    ?>
							  
							    <span <?php if ($val='V'){ 
							    	echo'class="label label-success"';
							   	}else if ($val='J'){
							    	echo'class="label label-warning"';
							    
							    }else{
							    	echo'class="label label-danger"';
							    }
							    
							    ?>> 
                                 <?php echo h($quantity);?>&nbsp;
								</span> 
			

							</td>
							<td>
								<span <?php if ($val='V'){ 
							    	echo'class="label label-success"';
							   	}else if ($val='J'){
							    	echo'class="label label-warning"';
							    
							    }else{
							    	echo'class="label label-danger"';
							    }
							    
							    ?>> 
                                 <?php echo h($pourcentage);?>&nbsp;
								</span> 
							</td>						
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-search-plus fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'inscriptions','action'=>'index', $var['Classs']['id'],$ido,$pourcentage), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Classs']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Classs']['libelle'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo  $this->element('spaginate'); ?>
		