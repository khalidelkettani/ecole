<ul class="pagination pull-right">
	<p>
<?php
echo $this->Paginator->counter(array(
  'format' => __('Page {:page} / {:pages}, montrant {:current} / {:count} au total')
));
?>
</p>
<?php
  echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>', array('tag' => 'li', 'escape' => false), '<a href="#"><i class="fa fa-chevron-left"></i></a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
  echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a'));
  echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>', array('tag' => 'li', 'escape' => false), '<a href="#"><i class="fa fa-chevron-right"></i></a>', array('class' => 'prev disabled', 'tag' => 'li', 'escape' => false));
?>
</ul>
