
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/dashboard')); ?>"><?php echo __('Accueils');?></a></li>
            <li><span><?php echo __('Admin');?></span></li>
            <li class="active"><span><?php echo __('Liste des utilisateurs');?></span></li>
        </ol>
                                    
        <h1><?php echo __('Liste des utilisateurs');?><small></small></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left"><?php echo __('Utilisateurs');?></h2>
                    <div class="filter-block pull-right">
                        <div class="form-group pull-left">
                            <input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
                            <i class="fa fa-search search-icon"></i>
                        </div>          
                        <a href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'add')); ?>" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 
                            <?php echo __('Nouveau utilisateur');?>
                        </a>
                    </div>
            </header>
                                        
        <div class="main-box-body clearfix">
            <div class="table-responsive xtable">
                <table class="table">
                    <thead>
                        <tr>
                            <th><?php echo __('Photo');?></th>
                            <th><?php echo __('Username');?></th>
                            <th><?php echo __('Mot de passe');?></th>
                            <th><?php echo __('Email');?></th>
                            <th><?php echo __('Role');?></th>
                           
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <?php foreach ($utilisateurs as $var): ?>
                    <tbody>
                        <tr>
                            <td>
                                
                            <?php if($var['Elef']['logo'] == null): ?>
                                    <img src="https://secure.gravatar.com/avatar/<?php echo md5($var['Elef']['logo']) ?>?s=10&d=mm" alt="" title=""/>
                                <?php else: ?>
                                    <img src="<?php echo $this->webroot.'app/webroot/img/users/'.$var['User']['id']. '.' .$var['User']['logo']; ?>" alt=""/>
                                <?php endif; ?>
                            </td>
                            <td>
                                
                                <?php echo h($var['User']['password']); ?>&nbsp;
                            </td>
                            <td>
                                
                                <?php echo h($var['User']['username']); ?>&nbsp;
                            </td>
                            <td>
                                <?php echo h($var['User']['email']); ?>&nbsp;
                            </td>
                            <td>
                                <?php echo h($var['User']['role']); ?>&nbsp;
                            </td>
                                                       
                            <td style="width: 15%;">
                                <?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'users','action'=>'edit', $var['User']['id']), array('escape' => false, 'class' => 'table-link', 'target' => '_self'));?>

                                <?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['User']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['User']['usern'])); ?>

                                
                            </td>
                        </tr>
                    </tbody>
                    <?php endforeach; ?>
                </table>
                <?php echo $this->element('paginate'); ?>
            </div>
        </div>
    </div>
</div>
</div>
    

<!--  Model form add éléve-->
<!--<?php echo $this->Form->create('U',array('role'=>'form','data-parsley-validate'=>'data-parsley-validate','action'=>'add','data-validate'=>'parsley')); ?>-->

    <!--  Model form edit ville-->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    
</div>
<script>
    $(function($) {
        
        $('#datepickerDate').datepicker({
          format: 'yyyy-mm-dd'
        });
        
    });
</script>
    <script type="text/javascript">
    $(document).ready(function(){
          
            $('body').on('keyup', '.search', function(){
            var q=$(this).val();
            var url = '<?php echo Router::url(array('controller'=>'users','action'=>'search')) ?>'+'/'+q;
                $(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
                $.get(url, function(data) {
                    $(".xtable").html(data);
                });
            });

        //edit function
            $(".modalEd").on('click', function(){
                 var url=$(this).attr('href'); 
                $.get(url, function(data) 
                {
                  $("#modalEdit").html(data);
                  $("#modalEdit").modal();
                });
            return false;
            });

    });

    $(function($) {
        $('#sel2').select2();
    });

    </script>