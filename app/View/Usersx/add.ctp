

<!-- <div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('role', array(
            'options' => array('admin' => 'Admin', 'author' => 'Author')
        ));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div> --> 
<div id="login-box">
		<header id="login-header">
			<div id="login-logo">
				<img src="img/logo.png" alt=""/>
				<!--<div class="top_b"><?php echo __('Bienvenue Sur '); ?></div> -->
			</div>
     	</header>
		<div id="login-box-holder">
    <div id="login-box-inner">
        <?php echo $this->Form->create('User',array('type'=>'post', 'action'=>'add', 'role' => 'form', 'data-parsley-validate'=>'data-parsley-validate', 'enctype' => 'multipart/form-data')); ?>

        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input class="form-control" name="data[User][username]" type="text" placeholder="<?php echo __('Username'); ?>" required="true" data-parsley-error-message= "<?php echo __('Le nom est requis'); ?>"> 
        </div>
       
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input class="form-control" name="data[User][email]" type="email" placeholder="<?php echo __('Adresse email'); ?>" required="true" data-parsley-error-message= "<?php echo __('L\'adresse email est requise'); ?>"> 
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" name="data[User][password]" class="form-control" placeholder="<?php echo __('Mot de passe'); ?>" required="true" data-parsley-error-message= "<?php echo __('Le mot de passe est requis'); ?>">
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
            <input type="password" name="data[User][temppassword]" class="form-control" placeholder="<?php echo __('Confirmation mot de passe'); ?>" required="true" data-parsley-error-message= "<?php echo __('La confirmation est requise'); ?>">
        </div>
        <!-- input('role', array(
            'options' => array('admin' => 'Admin', 'author' => 'Author') type="role"-->
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
            <input   name="data[User][role]" class="form-control" placeholder="<?php echo __('Le role'); ?>" required="true" data-parsley-error-message= "<?php echo __('Le role est requise'); ?>">
        </div>
        <div id="remember-me-wrapper">
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkbox-nice">
                        <input type="checkbox" id="terms-cond" checked="checked" />
                        <label for="terms-cond">
                            <?php echo __('J\'accepte les termes et conditions'); ?>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-success col-xs-12"><?php echo __('S\'INSCRIRE'); ?></button>
            </div>
        </div>
    <?php echo $this->Form->end(); ?>
</div>
</div>
</div>