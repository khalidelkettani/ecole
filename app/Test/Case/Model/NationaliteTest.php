<?php
App::uses('Nationalite', 'Model');

/**
 * Nationalite Test Case
 */
class NationaliteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.nationalite',
		'app.pay',
		'app.region',
		'app.ville'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Nationalite = ClassRegistry::init('Nationalite');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Nationalite);

		parent::tearDown();
	}

}
