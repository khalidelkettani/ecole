<?php
App::uses('PaysController', 'Controller');

/**
 * PaysController Test Case
 */
class PaysControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.pay',
		'app.ville',
		'app.filiale'
	);

}
