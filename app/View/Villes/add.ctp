


                    <div class="row-fluid">
                        <div class="span3">
							&nbsp;
                        </div>
                        <div class="span6">
							<h2 class="heading"><?php echo __('Ajouter une Ville'); ?></h2>
							<?php echo $this->Form->create('Competencesmatier',array('class'=>'form_validation_reg')); ?>
								<div class="formSep">
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Nom de la Ville'); ?> <span class="f_req">*</span></label>
											<?php echo $this->Form->input('libelle',array('class'=>'span12','label'=> false, 'type' =>'text', 'placeholder'=>__('Entrer le nom de la Ville')));?>
										</div>
										
									</div>
								</div>
															
								
								<div class="form-actions">
									<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER'); ?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'competencesmatiers','action' => 'index')); ?>"><?php echo __('ANNULER'); ?></a>
								</div>
							<?php echo  $this->Form->end(); ?>
                        </div>
                         <div class="span3">
							&nbsp;
                        </div>

                    </div>
