<?php
App::uses('AppController', 'Controller');

/**
 * Classses Controller
 */
class ParametragesController extends AppController {

//var $uses=array('Inscription');
/**
 * Scaffold
 *
 * @var mixed
 */
	//public $scaffold;
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');
	/**
 * index method
 *
 * @return void
 */
	public function index() {
	
	$this->layout = __('layout_admin_cube');

		$this->loadModel('Anneesscollaire');
		$annees=$this->Anneesscollaire->find('list',array(
    		                           'fields'=>array('id','libelle')));
		$options = $this->Anneesscollaire->find('all',array(
						'conditions' => array('Anneesscollaire.default_year'=>'true'),
                        'fields'=>array('id','libelle')
                        )
     );
		
		
		//$this->set('title_for_layout', __('Liste des classes'));
		//$this->Classs->recursive = 0;
		//$data = $this->Paginator->paginate('');  //array('Classs.user_id' => $this->Auth->user('id'))
    	//$this->set('classses', $data);
    	//$levels=$this->Classs->Level->find('list',array('fields'=>array('id','libelle')));*/
       $this->set(compact('annees','options'));
	}
	
	public function edit() {
	
	$this->layout = __('layout_admin_cube');

		$this->loadModel('Anneesscollaire');
		
    
		if ($this->request->is(array('post', 'put'))) {
			debug("fff");
			die();
			if ($this->Anneesscollaire->l ($this->request->data)) {
				$this->Session->setFlash(__('L\'année a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('L\'année n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		//$this->set('title_for_layout', __('Liste des classes'));
		//$this->Classs->recursive = 0;
		//$data = $this->Paginator->paginate('');  //array('Classs.user_id' => $this->Auth->user('id'))
    	//$this->set('classses', $data);
    	//$levels=$this->Classs->Level->find('list',array('fields'=>array('id','libelle')));*/
       $this->set(compact('annees','options'));
	}
	

}