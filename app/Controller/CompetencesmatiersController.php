<?php
App::uses('AppController', 'Controller');
/**
 * Villes Controller
 *
 * @property Ville $Ville
 * @property PaginatorComponent $Paginator
 */
class CompetencesmatiersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des competences'));
	    $this->loadModel('Matier');
		$this->Competencesmatier->recursive = 0;
        
		$data = $this->Paginator->paginate('Competencesmatier');
    	$this->set('competencesmatiers',$data); //  $this->Paginator->paginate('Competencesmatier',array('Competencesmatier.matiers_id'=>$id))); 
    	$matiers = $this->Matier->find('all');
		$this->set(compact('matiers'));
		//debug($matiers);


	}
	public function affichedetails($q = null) {
		//$id = $this->params['q'];
		$this->layout = null;
		$this->Competencesmatier->recursive = 0;
        
		$data = $this->Paginator->paginate('Competencesmatier');
		$this->set('competencesmatiers',  $this->Paginator->paginate('Competencesmatier',array('Competencesmatier.matiers_id'=>$q))); 
        //$this->set('competencesmatiers', $data);
        /*$comps = $this->Competencesmatier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Competencesmatier.matiers_id' => $q)));
		//debug($comps );*/
		$this->set(compact('q'));
	}
	public function search($q = null) {
		$this->layout=null;
		$this->Competencesmatier->recursive = 0;
		$this->set('competencesmatiers', $this->Paginator->paginate('Competencesmatier',array('Ville.libelle LIKE'=>'%'.$q.'%')));   //, 'Ville.user_id' => $this->Auth->user('id')
	$regions = $this->Competencesmatier->Region->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Region.user_id' => $this->Auth->user('id'))));
		$this->set(compact('regions'));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		$idu= $this->Ville->find('first', array(
        	'conditions' => array('Ville.id' => $id)));

		if (!$this->Ville->exists($id) || $idu['Ville']['user_id'] != $this->Auth->user('id')) {
			//throw new NotFoundException(__('Invalid candidat'));
			$this->Session->setFlash(__('Cette Ville est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}
*/
/**
 * add method
 *
 * @return void
 */
	public function add() {
          $this->layout = null;
		
			
		
		
		if ($this->request->is('post')) {
			debug("y1");
			//$this->request->data['Competencesmatier']['matiers_id']=$this->request->data['Competencesmatier']['id'];
			$this->request->data['Competencesmatier']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Competencesmatier->create();
			
			if ($this->Competencesmatier->save($this->request->data)) {
				$this->Session->setFlash(__('La ddddd a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La ville n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		/*$regions = $this->Ville->Region->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Region.user_id' => $this->Auth->user('id'))));
		$this->set(compact('regions'));*/
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null,$q = null) {
		$this->layout = null;
		debug($id);
		debug($q);
		//$this->layout = null;
		$idu= $this->Competencesmatier->find('first', array(
        	'conditions' => array('Competencesmatier.id' => $id)));

		if (!$this->Competencesmatier->exists($id)) {
			$this->Session->setFlash(__('Cette competence est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		 $this->request->data['matiers_id']=$q;
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Competencesmatier->save($this->request->data)) {
				$this->Session->setFlash(__('La competence a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La competence n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Competencesmatier.' . $this->Competencesmatier->primaryKey => $id));
			$this->request->data = $this->Competencesmatier->find('first', $options);
		}
		/*$regions = $this->Ville->Region->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Region.user_id' => $this->Auth->user('id'))));
		$this->set(compact('regions'));*/
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
	
		
		$this->Competencesmatier->id = $q;
		$idu= $this->Competencesmatier->find('first', array(
        	'conditions' => array('Competencesmatier.id' => $id)));

		if (!$this->Competencesmatier->exists($id)|| $idu['Competencesmatier']['user_id']!= $this->Auth->user('id')) {  
			$this->Session->setFlash(__('Cette Competencesmatier est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Competencesmatier->delete()) {
				$this->Session->setFlash(__('La Competencesmatier a été supprimée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('La ville n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Competencesmatier->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}


}
