<div class="villes view">
<h2><?php echo __('Ville'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ville['Ville']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($ville['Ville']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pay'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ville['Pay']['libelle'], array('controller' => 'pays', 'action' => 'view', $ville['Pay']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($ville['Company']['raison_sociale'], array('controller' => 'companies', 'action' => 'view', $ville['Company']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Ville'), array('action' => 'edit', $ville['Ville']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ville'), array('action' => 'delete', $ville['Ville']['id']), null, __('Are you sure you want to delete # %s?', $ville['Ville']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List regions'), array('controller' => 'regions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pay'), array('controller' => 'pays', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Filiales'), array('controller' => 'filiales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Filiale'), array('controller' => 'filiales', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Filiales'); ?></h3>
	<?php if (!empty($ville['Filiale'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Libelle'); ?></th>
		<th><?php echo __('Ville Id'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($ville['Filiale'] as $filiale): ?>
		<tr>
			<td><?php echo $filiale['id']; ?></td>
			<td><?php echo $filiale['libelle']; ?></td>
			<td><?php echo $filiale['ville_id']; ?></td>
			<td><?php echo $filiale['company_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'filiales', 'action' => 'view', $filiale['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'filiales', 'action' => 'edit', $filiale['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'filiales', 'action' => 'delete', $filiale['id']), null, __('Are you sure you want to delete # %s?', $filiale['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Filiale'), array('controller' => 'filiales', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
