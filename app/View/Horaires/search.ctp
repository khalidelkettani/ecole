	
			<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Du');?></th>
							<th><?php echo __('Au');?></th>
							<th><?php echo __('Période');?></th>
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($horaires as $horaires): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($horaires['Horaire']['du']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($horaires['Horaire']['au']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($horaires['Horaire']['periode']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'horaires','action'=>'edit', $horaires['Horaire']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $horaires['Horaire']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $horaires['Horaire']['du']+ ' '+$horaires['Horaire']['au'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
	<?php echo $this->element('spaginate'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		  
			//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});
</script>