<?php
App::uses('AppController', 'Controller');
/**
 * Villes Controller
 *
 * @property Ville $Ville
 * @property PaginatorComponent $Paginator
 */
class ProfsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des Professeurs'));
		$this->Prof->recursive = 0;
		$data = $this->Paginator->paginate('Prof',array('Prof.user_id' => $this->Auth->user('id')));
    	$this->set('profs', $data);
    	$matiers = $this->Prof->Matier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Matier.user_id' => $this->Auth->user('id'))));
    	$typesprofs = $this->Prof->Typesprof->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Typesprof.user_id' => $this->Auth->user('id'))));
		$civilites = array('Mlle' => 'Mlle', 'Mme' => 'Mme', 'Mr' => 'Mr');
		$this->set(compact('typesprofs','civilites','matiers'));
	}
	
public function infosprofs() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des Professeurs'));
		$this->Prof->recursive = 0;
		$data = $this->Paginator->paginate('Prof',array('Prof.user_id' => $this->Auth->user('id')));
    	$this->set('profs', $data);
    	$matiers = $this->Prof->Matier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Matier.user_id' => $this->Auth->user('id'))));
    	$typesprofs = $this->Prof->Typesprof->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Typesprof.user_id' => $this->Auth->user('id'))));
		$civilites = array('Mlle' => 'Mlle', 'Mme' => 'Mme', 'Mr' => 'Mr');
		$this->set(compact('typesprofs','civilites','matiers'));
	}
	public function search($q = null) {
		$this->layout=null;
		$this->Prof->recursive = 0;
		$this->set('profs', $this->Paginator->paginate('Prof',array('Prof.nom LIKE'=>'%'.$q.'%')));   //, 'Ville.user_id' => $this->Auth->user('id')
	    $matiers = $this->Prof->Matier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Matier.user_id' => $this->Auth->user('id'))));
    	$typesprofs = $this->Prof->Typesprof->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Typesprof.user_id' => $this->Auth->user('id'))));
		$civilites = array('Mlle' => 'Mlle', 'Mme' => 'Mme', 'Mr' => 'Mr');
		$this->set(compact('typesprofs','civilites','matiers'));

	}
public function matier() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des Professeurs'));
		$this->Prof->recursive = 0;
		$data = $this->Paginator->paginate('Prof',array('Prof.user_id' => $this->Auth->user('id')));
    	$this->set('profs', $data);
    	$matiers = $this->Prof->Matier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Matier.user_id' => $this->Auth->user('id'))));
    	$typesprofs = $this->Prof->Typesprof->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Typesprof.user_id' => $this->Auth->user('id'))));
		$civilites = array('Mlle' => 'Mlle', 'Mme' => 'Mme', 'Mr' => 'Mr');
		$this->set(compact('typesprofs','civilites','matiers'));
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		$idu= $this->Ville->find('first', array(
        	'conditions' => array('Ville.id' => $id)));

		if (!$this->Ville->exists($id) || $idu['Ville']['user_id'] != $this->Auth->user('id')) {
			//throw new NotFoundException(__('Invalid candidat'));
			$this->Session->setFlash(__('Cette Ville est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}
*/
/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = null;
		if ($this->request->is('post')) {
			$this->request->data['Prof']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Prof->create();
			
			if ($this->Prof->save($this->request->data)) {
				$this->Session->setFlash(__('Le Professeur  a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le Professeur n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		
		$matiers = $this->Prof->Matier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Matier.user_id' => $this->Auth->user('id'))));
		$typesprofs = $this->Prof->Typesprof->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Typesprof.user_id' => $this->Auth->user('id'))));
		$civilites = array('Mlle' => 'Mlle', 'Mme' => 'Mme', 'Mr' => 'Mr');
		$this->set(compact('typesprofs','civilites','matiers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = null;
		$idu= $this->Prof->find('first', array(
        	'conditions' => array('Prof.id' => $id)|| $idu['Prof']['user_id'] != $this->Auth->user('id')));

		if (!$this->Prof->exists($id)) {
			$this->Session->setFlash(__('Ce profosseur est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Prof->save($this->request->data)) {
				$this->Session->setFlash(__('Le professeur a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le professeur n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Prof.' . $this->Prof->primaryKey => $id));
			$this->request->data = $this->Prof->find('first', $options);
		}
		$matiers = $this->Prof->Matier->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Matier.user_id' => $this->Auth->user('id'))));
    	$typesprofs = $this->Prof->Typesprof->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Typesprof.user_id' => $this->Auth->user('id'))));
		$civilites = array('Mlle' => 'Mlle', 'Mme' => 'Mme', 'Mr' => 'Mr');
		$this->set(compact('typesprofs','civilites','matiers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Prof->id = $id;
		$idu= $this->Prof->find('first', array(
        	'conditions' => array('Prof.id' => $id)));

		if (!$this->Prof->exists($id)|| $idu['Prof']['user_id']!= $this->Auth->user('id')) {  
			$this->Session->setFlash(__('Ce prof est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Prof->delete()) {
				$this->Session->setFlash(__('Le prof a été supprimée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('Le prof n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function isAuthorized($user) {
       // Tous les users inscrits peuvent ajouter les posts
       if ($this->action === 'add') {
          return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Prof->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}


}
