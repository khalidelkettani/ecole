<?php echo $this->Form->create('Horaire'); ?>
	<?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Modifierjj'); ?></h4>
				</div>
				<div class="modal-body">

			   
						<div class="input-group input-append bootstrap-timepicker"> <!---->
							<label><?php echo __('Du'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>

							<?php echo $this->Form->input('du',array('class'=>'form-control timepicker','div'=>false,'placeholder'=>__('Du'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
							<span class="add-on input-group-addon"><i class="fa fa-clock-o"></i></span>
						</div>
						<div class="input-group input-append bootstrap-timepicker"> <!---->
							<label><?php echo __('Au'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>

							<?php echo $this->Form->input('au',array('class'=>'form-control timepicker','div'=>false,'placeholder'=>__('Au'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
							<span class="add-on input-group-addon"><i class="fa fa-clock-o"></i></span>
						</div>
						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Période'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('periode',['Matinée'=>'Matinée','Après-midi'=>'Après-midi','Soirie'=>'Soirie'],array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				<?php echo $this->Form->end(); ?>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->


