<?php
App::uses('Salle', 'Model');

/**
 * Salle Test Case
 */
class SalleTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.salle'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Salle = ClassRegistry::init('Salle');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Salle);

		parent::tearDown();
	}

}
