<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'villes','action' => '/dashboard')); ?>"><?php echo __('Accueil');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Liste des competences');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des competences par matiers');?><small></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<!--  <h2 class="pull-left"><?php echo __('Villes');?></h2>-->
					<!-- <div class="filter-block pull-right">
						<div class="form-group pull-left">
							<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div>	

						<a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouvelle ville');?>
						</a>
					</div> -->
			</header>
		<div class="row">
			<div class="col-lg-12">
				<div class="main-box clearfix">
					<header class="main-box-header clearfix">
						<h2><?php echo __('Liste des competences');?></h2>
					</header>
										
					<div class="main-box-body clearfix">
						<div class="panel-group accordion" id="accordion">
							<?php foreach ($matiers as $var): ?>
							    <tbody> 
							    
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion"   href="#<?php echo $var['Matier']['id']; ?>">
										<div   class="main-box-body clearfix">
			                                <?php echo $var['Matier']['libelle']; ?>
			                                <!-- <input type="text" id="f<?php echo $var['Matier']['id']; ?>" value="<?php echo $var['Matier']['id']; ?>"> -->	
			                                
		                                </div>
		                                	
										</a>
									</h4>
								</div>
								<div id="<?php echo $var['Matier']['id']; ?>"  value="<?php echo $var['Matier']['id']; ?>"class="panel-collapse collapse" >
									
									<div class="panel-body">
										<div class="main-box-body clearfix">
											<div class="table-responsive xtable">
												<!-- afiche details -->
												
												
											</div>
										</div>			
									</div>
								</div>
							</div>
							 </tbody>
					<?php endforeach; ?> 
					<?php echo $this->Form->end(); ?>			
												
											</div>
										
										</div>
									</div>
								</div>
							</div>

	</div>
</div>
</div>
	


<!--  Model form add ville-->
<!--  Model form add -->

	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title"><?php echo __('Ajouter'); ?></h4>
				</div>
				<div class="modal-body">
 
			    <?php echo $this->Form->create('Competencesmatier',array('url'=> array('controller' => 'competencesmatiers','action' => 'add' ) )); ?>

						<div class="form-group"> <!---->
							<label><?php echo __('Nom de la Competence'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de la Competence'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<?php echo $this->Form->input('matiers_id', array('id'=>'id1','type'=>'text'));?>
	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				<?php echo $this->Form->end(); ?>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->	
	

<!--  Model form edit ville-->
<div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>


	<script type="text/javascript">

	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();

			var url = '<?php echo Router::url(array('controller'=>'villes','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/LoaderIcon.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

            $(".collapse").on("shown.bs.collapse", function(){
           	       
                var q=this.getAttribute('id');
	     		document.getElementById("id1" ).value = q;
          // $(this).data( "id", q );
          // alert($(this).data('id'));
			//var q=document.getElementById("first").value ;
			//var q=$(this).val();
			//var h_id = $(this).data('id');
            //var  q = document.getElementById("#id").value;
//q.style.backgroundColor="#f00";
//.innerHTML
//var q=$("#z").val();
			//dalert(q);
			 
			var url = '<?php echo Router::url(array('controller'=>'Competencesmatiers','action'=>'affichedetails')) ?>'+'/'+q;
			
				/*$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/LoaderIcon.gif' ?>"></div>');*/
				
				/*$.get(url, function(data) {
					alert("gg");
					$(".xtable").html(data);
				});*/
              $.ajax({
            dataType: "html",
            type: "POST",
            //evalScripts: true,
	            url: '<?php echo Router::url(array('controller'=>'Competencesmatiers','action'=>'affichedetails'));?>'+'/'+q,
	                       
		            success: function(data) {
		            	
				        $(".xtable").html(data);
				      }
            

        });return false;
			});
		//edit function
		
			

	});
	//edit function
		
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	     		 alert(url);
	            $.get(url, function(data) 
	            {
	            	
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});
	$(function($) {
		$('#sel2').select2();
	});

	</script>





















