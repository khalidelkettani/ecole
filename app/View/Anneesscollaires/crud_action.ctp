<div class="modal" id="myModal" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="col-lg-12" style="text-align:right">
						<button class="btn btn-primary btn-lg" data-toggle="modal" id="bt" data-target="#">
  								<?php echo __('Nouveau Horaire');?>
						</button>

					</div>
					<h4 class="modal-title"><?php echo __('Liste des horaires'); ?></h4>
				</div>
				
				<div class="modal-body">
				<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Du');?></th>
							<th><?php echo __('Au');?></th>
							<th><?php echo __('Période');?></th>
							<th>Actions</th>
						</tr>
					</thead>
					<?php 
					foreach ($horaires as $horaire): 
							$id=$horaire['Horaire']['id'];?>
					<tbody>

						<tr>
							<td>
								<!-- <input type="text" value="<?php echo h($horaire['Horaire']['du']); ?>"> -->
								<?php echo h($horaire['Horaire']['du']); ?>&nbsp;
							</td>
							<td>
								<!--<input type="text" value="<?php echo h($horaire['Horaire']['au']); ?>">-->
									<?php echo h($horaire['Horaire']['au']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($horaire['Horaire']['periode']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'villes','action'=>'edit', $horaire['Horaire']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>
	<button style="width: 25%;" type="text"  id="delete" name="<?php echo h($horaire['Horaire']['id']); ?>">
									<!--<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i id="delete" name='.$id.'  class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span> ', array(), array('escape' => false,'class' => 'table-link danger')); ?>-->
									
	
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>

				<?php echo $this->element('paginate'); ?>
				
			</div>
					<div class="row" id="editheure">
					<div class="col-lg-12">
						<div class="form_style">
							

				<div class="form-group" id="frmAdd">
								<!-- <div class="col-lg-3">
								<input type="text" name="txtau"  id="txtdu" >
								</div>
								<div class="col-lg-3">
								<input type="text" name="txtdu"  id="txtau" >
								</div>
								<div class="col-lg-3">
								<input type="text" name="txtpriode"  id="txtp" value="matinee">
								</div> -->
					<div class="col-lg-12">
						<div class="col-lg-6">
						<label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('du',array('class'=>'form-control','id'=>'txtdu','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="col-lg-6">
						<div class="main-box">
                          <label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('au',array('class'=>'form-control','id'=>'txtau','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						</div>
						<div class="col-lg-6">
						<div class="main-box">
                          <label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('periode',array('class'=>'form-control','value'=>'matinee','id'=>'txtp','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						</div>
						
					</div>	
								<div class="col-lg-3">
							<p><button class="btn btn-default" id="btnAddAction" name="submit" onClick="callCrudAction('add','')"><?php echo __('Ajouter'); ?></button></p>
							
							<!-- <p><button id="btnAddAction" name="submit" onClick="callCrudAction('delete','9')">delete</button></p> -->

						</div>
							</div>
							<img src=<img src=<?php echo $this->webroot.'img/ajax_loader.gif' ?> id="loaderIcon" style="display:none" />
						</div>
						
					</div>	
				</div>			
			</div>
			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
			</div>
			</div>
 
		</div>
