<!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>
		<?php echo $this->fetch('title'); ?> | <?php echo __('Ecole'); ?>

	</title>
	<!-- google font libraries -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>
	<!-- bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/bootstrap/bootstrap.min.css" />
	<!-- RTL support - for demo only -->
	<script src="<?php echo $this->webroot;?>rhq/js/demo-rtl.js"></script>
	<!-- libraries -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/nanoscroller.css" />
	<!-- global styles -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/compiled/theme_styles.css" />
	<!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/daterangepicker.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/datepicker.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/daterangepicker.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/bootstrap-timepicker.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/select2.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/parsley.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/jquery.nouislider.css">

	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/jquery.nouislider.css">
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/weather-icons.css" type="text/css" />
															     
	<!-- Favicon -->
	<link type="image/x-icon" href="<?php echo $this->webroot;?>rhq/favicon.png" rel="shortcut icon"/>

	<!-- global scripts -->
	<script src="<?php echo $this->webroot;?>rhq/js/demo-skin-changer.js"></script> <!-- only for demo -->
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nanoscroller.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/demo.js"></script> <!-- only for demo -->
	<!-- this page specific scripts -->
	<script src="<?php echo $this->webroot;?>rhq/js/moment.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/daterangepicker.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nouislider.js"></script>
	<!-- theme scripts -->
	<script src="<?php echo $this->webroot;?>rhq/js/scripts.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/pace.min.js"></script>
	
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.maskedinput.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/moment.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/daterangepicker.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/select2.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/hogan.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/typeahead.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.pwstrength.js"></script>

	<script src="<?php echo $this->webroot;?>rhq/js/jquery.hotkeys.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap-wysiwyg.js"></script>
	
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nouislider.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/ckeditor/ckeditor.js"></script>
	
	<script src="<?php echo $this->webroot;?>rhq/js/parsley.min.js"></script>
</head>
<body class="theme-blue fixed-header fixed-footer">
	<div id="theme-wrapper">
		<header class="navbar" id="header-navbar">
			<?php echo  $this->element('rhq/header'); ?>
		</header>
		<div id="page-wrapper" class="container">
			<div class="row">
				<div id="nav-col">
					<?php echo  $this->element('rhq/nav-col'); ?>
				</div>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-lg-12">
							<?php echo $this->Session->flash(); ?>
                			<?php echo $content_for_layout; ?>					
						</div>
					
					<footer id="footer-bar" class="row">
						<p id="footer-copyright" class="col-xs-12">
							 &copy; 2017 <a href="http://j2dinfo.com" target="_blank">J2dInfo.com</a><?php echo __(' Tous droits réservés. Par'); ?> <a href="http://dfc-technology.com" target="_blank"> J2D Technology.</a>
						</p>
					</footer>
				</div>
			</div>
		</div>
	</div>
</div>

	
</body>
</html>