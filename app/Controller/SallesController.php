<?php
App::uses('AppController', 'Controller');
/**
 * Salles Controller
 */
class SallesController extends AppController {

/**
 * Scaffold
 *
 * @var mixed
 */
public $components = array('Paginator','Session');

	//public $scaffold;
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des villes'));
		$this->Salle->recursive = 0;
		$data = $this->Paginator->paginate('Salle');
    	$this->set('salles', $data);
 

	}
	public function search($q = null) {
		$this->layout=null;
		$this->Salle->recursive = 0;
		$this->set('salles', $this->Paginator->paginate('Salle',array('Salle.libelle LIKE'=>'%'.$q.'%')));   //, 'Ville.user_id' => $this->Auth->user('id')
	}


	public function add() {
		$this->layout = null;
		if ($this->request->is('post')) {
			$this->request->data['Salle']['user_id'] = $this->Auth->user('id');   
			$this->Salle->create();
			
			if ($this->Salle->save($this->request->data)) {
				$this->Session->setFlash(__('La salle a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La salle n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		
	}

	public function edit($id = null) {
		$this->layout =null;
		//$this->layout = __('layout_admin_cube');
	   // $this->set('title_for_layout', __('Liste des villes'));
		$idu= $this->Salle->find('first', array(
        	'conditions' => array('Salle.id' => $id)));

		if (!$this->Salle->exists($id)) {
			$this->Session->setFlash(__('Cette salle est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Salle->save($this->request->data)) {
				$this->Session->setFlash(__('La salle a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La salle n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Salle.' . $this->Salle->primaryKey => $id));
			$this->request->data = $this->Salle->find('first', $options);
		}
		
	}
	public function delete($id = null) {
		$this->Salle->id = $id;
		$idu= $this->Salle->find('first', array(
        	'conditions' => array('Salle.id' => $id)));

		if (!$this->Salle->exists($id)|| $idu['Salle']['user_id'] != $this->Auth->user('id') ) {   //
			$this->Session->setFlash(__('Cette salle est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Salle->delete()) {
				$this->Session->setFlash(__('La salle a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('La salle n\'a pas pu être supprimée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Salle->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}

}
		
		