<div class="container">
				<a href="index.html" id="logo" class="navbar-brand">
					<img src="<?php echo $this->webroot;?>/img/logo/logo.png" alt="" class="normal-logo logo-white"/>
					<!-- <img src="<?php echo $this->webroot;?>/img/logo.png" alt="" class="normal-logo logo-black"/>
					<img src="<?php echo $this->webroot;?>/img/logo.png" alt="" class="small-logo hidden-xs hidden-sm hidden"/> -->
				</a>
				
				<div class="clearfix">
				<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button>
				
				<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
					<ul class="nav navbar-nav pull-left">
						<li>
							<a class="btn" id="make-small-nav">
								<i class="fa fa-bars"></i>
							</a>
						</li>
						
						<li class="dropdown hidden-xs">
								<?php echo $this->Html->link('<span> <img alt="passif" src="'.$this->webroot.'rhq/img/fr.png"></span> Français', array_merge(array('language'=>'fre'),array_values($this->params['pass'])), array('escape' => false, 'class' => 'btn', 'target' => '_self'));?>
						</li>
						<li class="dropdown hidden-xs">
							<?php echo $this->Html->link('<span> <img alt="passif" src="'.$this->webroot.'rhq/img/en.png"></span> English', array_merge(array('language'=>'eng'),array_values($this->params['pass'])), array('escape' => false, 'class' => 'btn', 'target' => '_self'));?>
						</li>
					</ul>
				</div>
				
				<div class="nav-no-collapse pull-right" id="header-nav">
					<ul class="nav navbar-nav pull-right">
						<li class="dropdown profile-dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?php echo $this->webroot.'app/webroot/img/users/'.AuthComponent::user('id'). '.' .AuthComponent::user('logo'); ?>" alt=""/>
								<!-- <img src="https://secure.gravatar.com/avatar/<?php echo md5("youssef@gmail.com") ?>?s=550&d=mm" alt=""/> -->
								<span class="hidden-xs"><?php echo AuthComponent::user('username'); ?></span> <b class="caret"></b>
								<!-- <span class="status">
										<i class="fa fa-circle"></i> <?php echo __('En ligne'); ?>
									</span> -->
							</a>
							<ul class="dropdown-menu dropdown-menu-right">
								<!-- <li>
									<a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'profile')); ?>"><i class="fa fa-institution"></i><?php echo __('Informations entreprise');?>
									</a>
								</li> -->
								<!-- <li>
									<a href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'change_password')); ?>"><i class="fa fa-user"></i><?php echo __('Changer mot de passe');?>
									</a>
								</li> -->
								<li>
									<a href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'logout')); ?>"><i class="fa fa-power-off"></i><?php echo __('Déconnexion');?></a>
								</li>
							</ul>
						</li>
						<li class="hidden-xxs">
							<a href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'logout')); ?>" class="btn">
								<i class="fa fa-power-off"></i>
							</a>
						</li>
					</ul>
				</div>
				</div>
			</div>