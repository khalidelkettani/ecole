<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'anneesscollaires','action' => '/home')); ?>"><?php echo __('Accueil');?></a></li>
			<!--<li><span><?php echo __('Recrutement');?></span></li>-->
			<li><span><?php echo __('Année');?></span></li>
			<!--<li class="active"><span><?php echo __('Ajouter une année');?></span></li>-->
		</ol>									
		<h1><?php echo __('Ajouter une année');?><small></small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
				<h2>Ajout</h2>
			</header>						
			<div class="main-box-body clearfix">
				 <?php echo $this->Form->create('Anneesscollaire',array('url'=> array('controller' => 'anneesscollaires','action' => 'add' ) )); ?>
				
					<div class="form-group">
						<label><?php echo __('Nom de l\'année'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							    <?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de de l\'année'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					</div>
	
																	
				
				<div class="form-group">
						<div class="span12">
							<label><?php echo __('Date début'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

							<?php echo $this->Form->input('datedebut',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
						
						    </div>
						</div>
				</div>
					<div class="form-group">
						<div class="span12">
							<label><?php echo __('Date fin'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

							<?php echo $this->Form->input('datefin',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate1', 'required'=> false));?>
						
						    </div>
						</div>
					</div>

				</div>
				<h1></h1>
				<div class="col-lg-12" style="text-align:right">
					<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('controller' => 'anneesscollaires','action' => 'index')); ?>"><?php echo __('ANNULER'); ?></a>
					<button type="submit" data-loading-text="Loading..." class="btn btn-primary btn-lg" id="btn-loading-demo"><?php echo __('ENREGISTRER'); ?>
					</button>

				</div>
				<?php echo  $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(function($) {
		
		$('#datepickerDate1').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
<script>
	$(function($) {
		$('#sel2').select2();
		$('#sel3').select2();
		$('#sel4').select2();

		
	});
</script>



<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'anneesscollaires','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>


