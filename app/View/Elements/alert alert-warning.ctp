<div class="col-md-12">
	<div class="row row-wrap">
		<div class="alert alert-warning">
			<button class="close" type="button" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
			</button>
			<p class="text-small"><?php echo $message; ?> </p>
		</div>
	</div>
</div>