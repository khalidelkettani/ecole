<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>"><?php echo __('Accueil');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Liste des profs');?></span></li>
		</ol>								
		<h1><?php echo __('Liste des Profs');?><small></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left"><?php echo __('Profs');?></h2>
					<div class="filter-block pull-right">
						<div class="form-group pull-left">
							<input id="nom" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div>			
						<a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouveau Prof');?>
						</a>
					</div>
			</header>
										
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Nom');?></th>
							<th><?php echo __('Préom');?></th>
							<th><?php echo __('Civilité');?></th>
							<th><?php echo __('Matiere');?></th>
                            <th><?php echo __('Date d\'entrée de l\'élève');?></th>
							<th><?php echo __('Téléphone');?></th>
							<th><?php echo __('E-Mail');?></th>
							<th><?php echo __('Type');?></th>
							
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($profs as $var): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($var['Prof']['nom']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Prof']['prenom']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Prof']['civilite']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Matier']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Prof']['date_entree']); ?>&nbsp;
							</td>
							<td>
								<?php echo date(" d/m/Y ", strtotime(h($var['Prof']['date_entree'])));?>
							</td>
							<td>
								<?php echo h($var['Prof']['portable']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Prof']['email']); ?>&nbsp;
							</td>
							<!-- <td>
								<?php echo h($var['Prof']['type']); ?>&nbsp;
							</td> -->

							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="glyphicon glyphicon-list"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'profs','action'=>'matier', $var['Prof']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'profs','action'=>'edit', $var['Prof']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Prof']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Prof']['nom'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo $this->element('paginate'); ?>
			</div>
		</div>
	</div>
</div>
</div>
	

<!--  Model form add -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Ajouter un professeur'); ?></h4>
				</div>
				<div class="modal-body">

			    <?php echo $this->Form->create('Prof',array('url'=> array('controller' => 'profs','action' => 'add' ) )); ?>
						<div class="form-group"> <!---->
							<label><?php echo __('Nom'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('nom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom '),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
                        <div class="form-group"> <!---->
							<label><?php echo __('Prénom'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('prenom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prénom de professeur'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
 	
 	                    <div class="form-group"> <!---->
							<label><?php echo __('Civilite '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->select('civilite',$civilites,array('class'=>'form-control','div'=>false,'placeholder'=>__('Civilite'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>

						</div>
						

						 <div class="form-group form-group-select2">
							<label>
								<?php echo __('Matières'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('matier_id',$matiers,array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div>
                        <div class="form-group form-group-select2">
							<label><?php echo __('Date d\'entrée de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

							<?php echo $this->Form->input('date_entree',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
					    </div>
						date_entree 
						<div class="form-group"> <!---->
							<label><?php echo __('Portable '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('portable',array('class'=>'form-control','div'=>false,'placeholder'=>__('Portable'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group"> <!---->
							<label><?php echo __('Email '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('email',array('class'=>'form-control','div'=>false,'placeholder'=>__('Email'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Type'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('type',$typesprofs,array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div> 
						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Adresse'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->input('adresse',array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1','label'=> false, 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div> 
						


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				<?php echo $this->Form->end(); ?>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<!--  Model form edit -->
<div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
	<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'profs','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>





















