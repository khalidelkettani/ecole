<ul class="pagination pull-right">
<p>
<?php
echo $this->Paginator->counter(array(
  'format' => __('Page {:page} / {:pages}, montrant {:current} / {:count} au total')
));
?>
</p>
<?php
  echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>', array('tag' => 'li', 'escape' => false,'class'=>'spaginate'), '<a href="#" onclick="return false"><i class="fa fa-chevron-left"></i></a>', array('class' => 'prev disabled spaginate', 'tag' => 'li', 'escape' => false));
  echo $this->Paginator->numbers(array('onclick'=>'return false','separator' => '', 'tag' => 'li', 'currentLink' => true, 'currentClass' => 'active', 'currentTag' => 'a','class'=>'spaginate'));
  echo $this->Paginator->next('<i class="fa fa-chevron-right"></i>', array('tag' => 'li', 'escape' => false,'class'=>'spaginate'), '<a href="#"><i class="fa fa-chevron-right"></i></a>', array('class' => 'prev disabled spaginate', 'tag' => 'li', 'escape' => false));
?>
</ul>
<script>
			$('.spaginate a').on('click', function(){
				var url = $(this).attr('href');
				if(url!='#')
				{
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
				return false;
				}
			});
</script>