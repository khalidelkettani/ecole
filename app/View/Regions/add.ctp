
                    <div class="row-fluid">
                     
                        <div class="span12">
							<h3 class="heading"><?php echo __('Ajouter une région');?></h3>
							<?php echo $this->Form->create('Pay',array('class'=>'form_validation_reg')); ?>
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Nom de la  région');?> <span class="f_req">*</span></label>
											<?php echo $this->Form->input('libelle',array('class'=>'span12','label'=> false, 'type' =>'text', 'placeholder'=>__('Entrer le nom de la région')));?>
										</div>
										
									</div>
								<div class="formSep">
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Pays d\'appartenance'); ?> <span class="f_req">*</span></label>
											<?php echo $this->Form->select('pay_id', $pays, array('class' => 'span12', 'empty' =>__('Sélectionnez le pays'))); ?>
										</div>
										
									</div>
								</div>
								<div class="form-actions">
									<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER');?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'regions','action' => 'index')); ?>"><?php echo __('ANNULER');?></a>
								</div>
							<?php echo  $this->Form->end(); ?>
                        </div>
                        

                    </div>
