<?php
App::uses('Niveaux', 'Model');

/**
 * Niveaux Test Case
 */
class NiveauxTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.niveaux',
		'app.class',
		'app.classes_niveaux'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Niveaux = ClassRegistry::init('Niveaux');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Niveaux);

		parent::tearDown();
	}

}
