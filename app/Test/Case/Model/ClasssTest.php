<?php
App::uses('Classs', 'Model');

/**
 * Classs Test Case
 */
class ClasssTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.classs',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Classs = ClassRegistry::init('Classs');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Classs);

		parent::tearDown();
	}

}
