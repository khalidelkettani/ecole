<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'anneesscollaires','action' => '/index')); ?>"><?php echo __('Accueil');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Année scollaires');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des années');?><small></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left"><?php echo __('Année scollaires');?> </h2>
					<div class="filter-block pull-right">
						<div class="form-group pull-left">
							<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div>			
						<a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouvelle année');?>
						</a>
					</div>
			</header>
										
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Année scollaire'); ?>&nbsp;</th>
							<th><?php echo __('Date début'); ?>&nbsp;</th>
							<th><?php echo __('Date fin'); ?>&nbsp;</th>
							<th><?php echo __('Actions'); ?>&nbsp;</th>
						</tr>
					</thead>
					<?php foreach ($anneesscollaires as $var): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($var['Anneesscollaire']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Anneesscollaire']['datedebut']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Anneesscollaire']['datefin']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'anneesscollaires','action'=>'edit', $var['Anneesscollaire']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Anneesscollaire']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Anneesscollaire']['libelle'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo $this->element('paginate'); ?>
			</div>
		</div>
	</div>
</div>
</div>

<!--  Model form add -->
                <?php echo $this->Form->create('Anneesscollaire',array('role'=>'form','data-parsley-validate'=>'data-parsley-validate','action'=>'add','data-validate'=>'parsley')); ?>
						<div class="form-group">
							<label><?php echo __('Nom de l\'année'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de  l\'année'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Veuillez saisir  l\'année!')));?>
						</div>
						<div class="col-lg-12">
		
				           <div class="col-lg-6">
                                <div class="form-group form-group-select2">
							    <label><?php echo __('Date début de l\'année'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
     						     <?php echo $this->Form->input('datedebut',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
					          </div>
				           	</div>
				            <div class="col-lg-6">
                                <div class="form-group form-group-select2">
							    <label><?php echo __('Date début de l\'année'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
     						     <?php echo $this->Form->input('datedebut',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
					          </div>
				           	</div>
										
						</div>
								
						<div class="form-actions">
							<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER');?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'pays','action' => 'index')); ?>"><?php echo __('ANNULER');?></a>
						</div>
						<?php echo  $this->Form->end(); ?>

                  	


<!--  Model form edit region-->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>
<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
	<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'anneesscollaires','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

			//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});
	$(function($) {
		$('#sel2').select2();
	});
	</script>












