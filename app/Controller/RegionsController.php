<?php
App::uses('AppController', 'Controller');
/**
 * Regions Controller
 */
class RegionsController extends AppController {

/**
 * Scaffold
 *
 * @var mixed
 */
public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des régions'));
		//$this->Paginator->settings = $this->paginate;
		$this->Region->recursive = 0;
		$data = $this->Paginator->paginate('Region',array('Region.user_id' => $this->Auth->user('id')));
    	$this->set('regions', $data);
    	$pays = $this->Region->Pay->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		$this->set(compact('pays'));

    
	}
	/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = null;
		if ($this->request->is('post')) {
			$this->request->data['Region']['user_id'] = $this->Auth->user('id');
			$this->Region->create();
			if ($this->Region->save($this->request->data)) {
				$this->Session->setFlash(__('La region a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La region n\'a pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		$pays = $this->Region->Pay->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		$this->set(compact('pays'));
	}
/**
* @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = null;
		$idu= $this->Region->find('first', array(
        	'conditions' => array('Region.id' => $id)|| $idu['Region']['user_id'] != $this->Auth->user('id')));

		if (!$this->Region->exists($id)) {
			$this->Session->setFlash(__('Cette région est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Region->save($this->request->data)) {
				$this->Session->setFlash(__('La région a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La région n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Region.' . $this->Region->primaryKey => $id));
			$this->request->data = $this->Region->find('first', $options);
		}
		$pays = $this->Region->Pay->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		$this->set(compact('pays'));
	}
	public function search($q = null) {
		$this->layout=null;
		$this->Region->recursive = 0;
		$this->set('regions', $this->Paginator->paginate('Region',array('Region.libelle LIKE'=>'%'.$q.'%')));
	$pays = $this->Region->Pay->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		$this->set(compact('pays'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Region->id = $id;
		$idu= $this->Region->find('first', array(
        	'conditions' => array('Region.id' => $id)));

		if (!$this->Region->exists($id)|| $idu['Region']['user_id'] != $this->Auth->user('id') ) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id')
			$this->Session->setFlash(__('Cette region est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->Region->delete()) {
				$this->Session->setFlash(__('La region a été supprimée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('La region n\'a pas pu être supprimée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	//public $scaffold;
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Region->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}
