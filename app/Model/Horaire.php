<?php
App::uses('AppModel', 'Model');
/**
 * Nationalite Model
 *
 * @property Pay $Pay
 */
class Horaire extends AppModel {

	public function isOwnedBy($post, $user) {
    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
}



/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'periode' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		/*'slug' => array(
            'rule' => '/^[a-z0-9\-]+$/',
            'allowEmpty' => true,
            'message' => "L'URL n'est pas valide"
            ),
        'name' => array(
            'rule' => 'notEmpty',
            'message' => "Vous devez préciser un titre"
            ),
        'content' => array(
            'rule' => 'notEmpty',
            'message' => "Vous devez entrer votre contenu"
            )
        );*/
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
