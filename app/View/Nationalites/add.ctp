  <div class="row-fluid">
                     
                        <div class="span12">
							<h3 class="heading"><?php echo __('Ajouter une salle');?></h3>
							<?php echo $this->Form->create('Salle',array('class'=>'form_validation_reg')); ?>
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Nom de la salle');?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span></label>
											<?php echo $this->Form->input('libelle',array('class'=>'span12','label'=> false, 'type' =>'text', 'placeholder'=>__('Entrer le nom de la salle')));?>
										</div>
										
									</div>
								
								<div class="form-actions">
									<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER');?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'salles','action' => 'index')); ?>"><?php echo __('ANNULER');?></a>
								</div>
							<?php echo  $this->Form->end(); ?>
                        </div>
                        

                    </div>
