<!-- <div class="classses form">
<?php echo $this->Form->create('Classs'); ?>
	<fieldset>
		<legend><?php echo __('Add Classs'); ?></legend>
	<?php
		echo $this->Form->input('libelle');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Classses'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div> -->


<div class="row-fluid">
                     
                        <div class="span12">
							<h3 class="heading"><?php echo __('Ajouter une classe');?></h3>
							<?php echo $this->Form->create('Classe',array('class'=>'form_validation_reg')); ?>
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Nom de la classe');?> <span class="f_req">*</span></label>
											<?php echo $this->Form->input('libelle',array('class'=>'span12','label'=> false, 'type' =>'text', 'placeholder'=>__('Entrer le nom de la classe')));?>
										</div>
										
									</div>
									<div class="form-group">
									    <label><?php echo __('Niveau de la classe'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
				                                             <?php echo $this->Form->select('level_id',$levels,array('class'=>'form-control','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
									</div>
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Capacité de la classe');?> <span class="f_req">*</span></label>
											<?php echo $this->Form->input('capacite',array('class'=>'span12','label'=> false, 'type' =>'text', 'placeholder'=>__('Entrer la Capacité de la classe')));?>
									    </div>
										
									</div>
									
								
								<div class="form-actions">
									<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER');?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'classses','action' => 'index')); ?>"><?php echo __('ANNULER');?></a>
								</div>
							<?php echo  $this->Form->end(); ?>
                        </div>
                        

                    </div>

