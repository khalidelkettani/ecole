
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'users','action' => '/dashboard')); ?>"><?php echo __('Accueil');?></a></li>
            <li><span><?php echo __('Admin');?></span></li>
            <li class="active"><span><?php echo __('Liste des utilisateurs');?></span></li>
        </ol>
                                    
        <h1><?php echo __('Liste des utilisateurs');?><small></small></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="main-box clearfix">
            <header class="main-box-header clearfix">
                <h2 class="pull-left"><?php echo __('Utilisateurs');?></h2>
                    <div class="filter-block pull-right">
                        <div class="form-group pull-left">
                            <input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
                            <i class="fa fa-search search-icon"></i>
                        </div>          
                        <a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 
                            <?php echo __('Nouveau utilisateur');?>
                        </a>
                    </div>
            </header>
                                        
        <div class="main-box-body clearfix">
            <div class="table-responsive xtable">
                <table class="table">
                    <thead>
                        <tr>
                            <th><?php echo __('Surname');?></th>
                            <th><?php echo __('Email');?></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <?php foreach ($users as $ville): ?>
                    <tbody>
                        <tr>
                            <td>
                                <?php echo h($ville['User']['username']); ?>&nbsp;
                            </td>
                            <td>
                                <?php echo h($ville['User']['email']); ?>&nbsp;
                            </td>
                            <td style="width: 15%;">
                                <?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'users','action'=>'edit', $ville['User']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

                                <?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $ville['User']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $ville['User']['username'])); ?>
                            </td>
                        </tr>
                    </tbody>
                    <?php endforeach; ?>
                </table>
                <?php echo $this->element('paginate'); ?>
            </div>
        </div>
    </div>
</div>
</div>
    

<!--  Model form add -->


<!--  Model form edit ville-->
<div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>


    <script type="text/javascript">
    $(document).ready(function(){
          
            $('body').on('keyup', '.search', function(){
            var q=$(this).val();
            var url = '<?php echo Router::url(array('controller'=>'villes','action'=>'search')) ?>'+'/'+q;
                $(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
                $.get(url, function(data) {
                    $(".xtable").html(data);
                });
            });

        //edit function
            $(".modalEd").on('click', function(){
                 var url=$(this).attr('href'); 
                $.get(url, function(data) 
                {
                  $("#modalEdit").html(data);
                  $("#modalEdit").modal();
                });
            return false;
            });

    });

    $(function($) {
        $('#sel2').select2();
    });

    </script>
































