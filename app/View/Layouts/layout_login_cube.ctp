<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title><?php echo $this->fetch('title'); ?> | <?php echo __('Gestion Ecole'); ?></title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/bootstrap/bootstrap.min.css" />
	<script src="<?php echo $this->webroot;?>rhq/js/demo-rtl.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/nanoscroller.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/compiled/theme_styles.css" />
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>
  	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/parsley.css">
	<link type="image/x-icon" href="<?php echo $this->webroot;?>rhq/favicon.png" rel="shortcut icon"/>

	<script src="<?php echo $this->webroot;?>rhq/js/demo-skin-changer.js"></script> 
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nanoscroller.min.js"></script>	
	<script src="<?php echo $this->webroot;?>rhq/js/demo.js"></script> 
	<script src="<?php echo $this->webroot;?>rhq/js/scripts.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/parsley.min.js"></script>
</head>
<body id="login-page">
	<div id="login-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div id="login-box">
						<div id="login-box-holder">
							<div class="row">
								<div class="col-xs-12">
									<?php //echo $this->Session->flash('auth');?>
									<?php echo $this->Session->flash();?>
									<header id="login-header">
										<div id="login-logo">
											<strong><h5>Gestion Ecole</h5></strong>
											<!-- <img src="<?php echo $this->webroot;?>rhq/img/RHQ_logo_web_w.png" alt=""/> -->
										</div>
									</header>
									<?php echo $content_for_layout; ?>
								</div>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	

	
</body>
</html>