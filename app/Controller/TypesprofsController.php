<?php
App::uses('AppController', 'Controller');
/**
 * Villes Controller
 *
 * @property Ville $Ville
 * @property PaginatorComponent $Paginator
 */
class TypesprofsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des types'));
		$this->Typesprof->recursive = 0;

		$data = $this->Paginator->paginate('Typesprof',array('Typesprof.user_id' => $this->Auth->user('id')));
    	$this->set('typesprofs', $data);
    	//$pays = $this->Ville->Pay->find('list',array('fields'=>array('id','libelle'),
						//'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		


	}

	public function search($q = null) {
		$this->layout=null;
		$this->Typesprof->recursive = 0;
		$this->set('typesprofs', $this->Paginator->paginate('Typesprof',array('Typesprof.libelle LIKE'=>'%'.$q.'%')));   //, 'Ville.user_id' => $this->Auth->user('id')
	

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		$idu= $this->Ville->find('first', array(
        	'conditions' => array('Ville.id' => $id)));

		if (!$this->Ville->exists($id) || $idu['Ville']['user_id'] != $this->Auth->user('id')) {
			//throw new NotFoundException(__('Invalid candidat'));
			$this->Session->setFlash(__('Cette Ville est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}
*/
/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = null;
		if ($this->request->is('post')) {
			$this->request->data['Typesprof']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Typesprof->create();
			
			if ($this->Typesprof->save($this->request->data)) {
				$this->Session->setFlash(__('Le type a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le type n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = null;
		$idu= $this->Typesprof->find('first', array(
        	'conditions' => array('Typesprof.id' => $id)|| $idu['Typesprof']['user_id'] != $this->Auth->user('id')));

		if (!$this->Typesprof->exists($id)) {
			$this->Session->setFlash(__('Ce type est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Typesprof->save($this->request->data)) {
				$this->Session->setFlash(__('Le type a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le type n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Typesprof.' . $this->Typesprof->primaryKey => $id));
			$this->request->data = $this->Typesprof->find('first', $options);
		}
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Typesprof->id = $id;
		$idu= $this->Typesprof->find('first', array(
        	'conditions' => array('Typesprof.id' => $id)));

		if (!$this->Typesprof->exists($id)|| $idu['Typesprof']['user_id']!= $this->Auth->user('id')) {  
			$this->Session->setFlash(__('Ce type est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Typesprof->delete()) {
				$this->Session->setFlash(__('Le type a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('Le type n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }
    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Typesprof->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}


}
