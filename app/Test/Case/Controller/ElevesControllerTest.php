<?php
App::uses('ElevesController', 'Controller');

/**
 * ElevesController Test Case
 */
class ElevesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.elef',
		'app.class',
		'app.eleves_class'
	);

}
