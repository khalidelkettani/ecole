<?php
App::uses('AppController', 'Controller');
/**
 * Villes Controller
 *
 * @property Ville $Ville
 * @property PaginatorComponent $Paginator
 */
class HorairesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste d\'horaires'));
		$this->Horaire->recursive = 0;

		$data = $this->Paginator->paginate('Horaire');
    	$this->set('horaires', $data);
 
	}

	public function search($q=null) {
		
		$this->layout=null;
		$this->Horaire->recursive = 0;
		$data = $this->Paginator->paginate('Horaire');
		if($q=="0"){
			$this->set('horaires', $this->Paginator->paginate('Horaire'));   //, 'Ville.user_id' => $this->Auth->user('id')
          
		}else{
			$this->set('horaires', $this->Paginator->paginate('Horaire',array('Horaire.periode LIKE'=>'%'.$q.'%'))); 
		}
		
		//$this->set('horaires', $data);
		
	    
       
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		$idu= $this->Ville->find('first', array(
        	'conditions' => array('Ville.id' => $id)));

		if (!$this->Ville->exists($id) || $idu['Ville']['user_id'] != $this->Auth->user('id')) {
			//throw new NotFoundException(__('Invalid candidat'));
			$this->Session->setFlash(__('Cette Ville est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}
*/
/**
 * add method
 *
 * @return void
 */
	public function add() {
		//debug($id);
		$this->layout = null;
		if ($this->request->is('post')) {
			$this->request->data['Horaire']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Horaire->create();
			
			if ($this->Horaire->save($this->request->data)) {
				$this->Session->setFlash(__('Horaire a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Horaire n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		debug($id);
		$this->layout = null;
		$idu= $this->Horaire->find('first', array(
        	'conditions' => array('Horaire.id' => $id)));

		if (!$this->Horaire->exists($id)) {
			$this->Session->setFlash(__('Horaire est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Horaire->save($this->request->data)) {
				$this->Session->setFlash(__('Horaire a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Horaire n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else { //s il ne sajet ni de post ni de put charger les donnees ds les imput
			$options = array('conditions' => array('Horaire.' . $this->Horaire->primaryKey => $id));
			$this->request->data = $this->Horaire->find('first', $options);
		}
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Horaire->id = $id;
		$idu= $this->Horaire->find('first', array(
        	'conditions' => array('Horaire.id' => $id)));

		if (!$this->Horaire->exists($id)) {  
			$this->Session->setFlash(__('Cet horaire est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Horaire->delete()) {
				$this->Session->setFlash(__('L\'horaire a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('L\'horaire n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Horaire->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}


}
