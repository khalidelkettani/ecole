<?php
App::uses('Classe', 'Model');

/**
 * Classe Test Case
 */
class ClasseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.classe',
		'app.prof',
		'app.classes_prof',
		'app.elef',
		'app.eleves_class'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Classe = ClassRegistry::init('Classe');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Classe);

		parent::tearDown();
	}

}
