<?php
App::uses('Elef', 'Model');

/**
 * Elef Test Case
 */
class ElefTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.elef',
		'app.class',
		'app.eleves_class'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Elef = ClassRegistry::init('Elef');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Elef);

		parent::tearDown();
	}

}
