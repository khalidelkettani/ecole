<?php
App::uses('ClassesController', 'Controller');

/**
 * ClassesController Test Case
 */
class ClassesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.class'
	);

}
