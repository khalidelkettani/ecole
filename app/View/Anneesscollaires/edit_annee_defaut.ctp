<div class="row">
				<div class="col-lg-12">
					<header id="email-header" class="clearfix">
						<ol class="breadcrumb">
							<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/dashboard')); ?>"><?php echo __('Accueil');?></a></li>
							<li><span><?php echo __('ADMIN');?></span></li>
							<li><span><?php echo __('Paramétrages');?></span></li>

						</ol>

					</header>
					<!-- Bouton execution modal -->
<!-- <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Lancer la modal
</button> -->
<!-- Modal -->
<?php echo $this->Form->create('Anneesscollaire'); ?>
        <div class="row" >

				<!--<?php echo $this->Form->create('Anneesscollaire',array('url'=> array(
                                            'controller' => 'anneesscollaires','action' => 'edit_annee_defaut'
                                                  ))
                                                  ) 
                                                  ;?>-->
                <?php echo $this->Form->input('id', array('id'=>'id','type'=>'hidden'));?>
                <?php echo $this->Form->input('idm', array('id'=>'idm','type'=>'hidden'));?>
						<div class="col-lg-6">
							
							<h3><span><?php echo __('Géneral'); ?></span></h3>

								<div class="form-group">
									<label><?php echo __('Année scollaire par defaut'); ?> <span class="f_req"></span></label>
										
								<?php  echo $this->Form->input(__('Année scollaire'), array(
                              	'class' => 'form-control  ch_filtrer filtrer','required' => false,
										  'options' => array($annees),
										  //'empty'=>$options,
										  'value'=> $ida,
										  
										  ));?>  
								</div>
		<div class="form-group">
									
			<h3><span><?php echo __('Mois de l\'année scollaire'); ?></span></h3>					
									
				<?php /*echo $this->Form->input(__('Janvier'),array(
                        'class' => 'form-control  ch_filtrerMJ filtrer','type'=>'checkbox','required' => 'false','checked'=>$idm[1]))*/

						foreach ($mois as $k => $moi) {
								if($moi['Moi']['checked']==1){
									$chek='checked';
								}else{
									$chek='false';
						}
								//echo "<input type='checkbox' checked='".$chek."' name='data[Anneesscollaire][Mois][".$moi['Moi']['id']."][]' ></input><br>";
								?>
						<div class="checkbox-nice">
							<div class="col-lg-3">
								
						    <?php
						    $lab=" ";
						   switch ($moi['Moi']['mois']) {
								case 'janvier':   $lab= '&nbsp;&nbsp;&nbsp;'.__('Janvier');
							}
						    //$lab="-".$moi['Moi']['mois'];'&nbsp;&nbsp;&nbsp;'.
								echo $this->Form->input($moi['Moi']['mois']
									,array(
                       			 'class' => 'form-control ','type'=>'checkbox','required' => 'false','label'=>'&nbsp;&nbsp;&nbsp;'.ucfirst($moi['Moi']['mois']),'checked'=>$moi['Moi']['checked'],'name'=>'data[Anneesscollaire][Mois]['.$moi['Moi']['id'].'][]'));

							?>

							</div>
						</div>
						<?php						
							   }
                    	?>


								
		</div>
			
	</div>

			<!--</div>	-->				
		<div class="col-lg-12">	
			 <div class="form-group">
				<h3><span><?php echo __('Jours de la semaine scollaire'); ?></span></h3>
				 <?php /*echo $this->Form->input(__('Janvier'),array(
                        'class' => 'form-control  ch_filtrerMJ filtrer','type'=>'checkbox','required' => 'false','checked'=>$idm[1]))*/

								

							foreach ($jours as $k => $moi) {
								if($moi['Jour']['checked']==1){
									$chek='checked';
								}else{
									$chek='false';
								}
								//echo "<input type='checkbox' checked='".$chek."' name='data[Anneesscollaire][Mois][".$moi['Moi']['id']."][]' ></input><br>";
								?>
						<div class="checkbox-nice">
							<div class="col-lg-3">
								
						    <?php
						    
						   $lab=ucfirst($moi['Jour']['jours']); 
						 // echo $lab;
						       //;'&nbsp;&nbsp;&nbsp;'.
								echo $this->Form->input($moi['Jour']['jours'],array(
                       			 'class' => 'form-controlJ ','type'=>'checkbox','required' => 'false','label'=>'&nbsp;&nbsp;&nbsp;'.ucfirst($moi['Jour']['jours']),'checked'=>$moi['Jour']['checked'],'name'=>'data[Anneesscollaire][Jours]['.$moi['Jour']['id'].'][]'));

							?>

							</div>
						</div>
							<?php						
							   }
                    	      ?>

				
			</div> 
			</div> 
			<div class="form-group">
					
							
				<div class="col-lg-12">
					<h3><span><?php echo __('Horaire de la semaine scollaire'); ?></span></h3>
					<div class="col-lg-3">	
							<h1><?php echo __('Matinée'); ?></h1>				
						<div class="form-group" style='text-align:center'>
								<label><?php echo __('Horaire '); ?></label>
								<div class="form-group" id="liste11">
									<select multiple class="form-control modalEd"  style='text-align:center' >
									<?php
										foreach ($horairesM as $hk => $horair) {
											$du=  substr($horair['Horaire']['du'], 0, 5);
											$au=  substr($horair['Horaire']['au'], 0, 5);
											?><option><?php echo $du.'&nbsp;&nbsp;&nbsp;'.$au ?></option>
											<?php
									
										}	
									?>			
									</select>
								</div>
						</div>
					</div>

					<div class="col-lg-3">	
							<h1><?php echo __('Après-midi'); ?></h1>				
						<div class="form-group" style='text-align:center'>
								<label><?php echo __('Horaire '); ?></label>
								<div class="form-group" id="liste11">
									<select multiple class="form-control modalEd"  style='text-align:center' >
									<?php
										foreach ($horairesAP as $hk => $horair) {
											$du=  substr($horair['Horaire']['du'], 0, 5);
											$au=  substr($horair['Horaire']['au'], 0, 5);
											?><option><?php echo $du.'&nbsp;&nbsp;&nbsp;'.$au ?></option>
											<?php
									
										}	
									?>			
									</select>
								</div>
						</div>
					</div>
					<div class="col-lg-3">	
							<h1><?php echo __('Soirie'); ?></h1>				
						<div class="form-group" style='text-align:center'>
								<label><?php echo __('Horaire '); ?></label>
								<div class="form-group" id="liste11">
									<select multiple class="form-control modalEd"  style='text-align:center' >
									<?php
										foreach ($horairesS as $hk => $horair) {
											$du=  substr($horair['Horaire']['du'], 0, 5);
											$au=  substr($horair['Horaire']['au'], 0, 5);
											?><option><?php echo $du.'&nbsp;&nbsp;&nbsp;'.$au ?></option>
											<?php
										}	
									?>			
									</select>
								</div>
						</div>
					</div>
				</div>


						</div>

				    <div class="col-lg-12" style="text-align:right">
					<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('controller' => 'parametrages','action' => 'index')); ?>"><?php echo __('FERMER'); ?></a>
					 
					<button type="submit" data-loading-text="Loading..." class="btn btn-primary btn-lg" id="btn-loading-demo" ><?php echo __('ENREGISTRER'); ?>
					</button>

				</div>
</div>					
		<?php echo $this->Form->end(); ?>
<div class="modal" id="myModal" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close"  data-dismiss="modal" aria-hidden="true">&times;</button>
					<div class="col-lg-12" style="text-align:right">
						<button class="btn btn-primary btn-lg" data-toggle="modal" id="bt" data-target="#">
  								<?php echo __('Nouveau Horaire');?>
						</button>

					</div>
					<h4 class="modal-title"><?php echo __('Liste des horaires'); ?></h4>
				</div>
				
				<div class="modal-body">
				<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Du');?></th>
							<th><?php echo __('Au');?></th>
							<th><?php echo __('Période');?></th>
							<th>Actions</th>
						</tr>
					</thead>
					<?php 
					foreach ($horaires as $horaire): 
							$id=$horaire['Horaire']['id'];?>
					<tbody>

						<tr>
							<td>
								<!-- <input type="text" value="<?php echo h($horaire['Horaire']['du']); ?>"> -->
								<?php echo h($horaire['Horaire']['du']); ?>&nbsp;
							</td>
							<td>
								<!--<input type="text" value="<?php echo h($horaire['Horaire']['au']); ?>">-->
									<?php echo h($horaire['Horaire']['au']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($horaire['Horaire']['periode']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'villes','action'=>'edit', $horaire['Horaire']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>
	<button style="width: 25%;" type="text"  id="delete" name="<?php echo h($horaire['Horaire']['id']); ?>">
									<!--<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i id="delete" name='.$id.'  class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span> ', array(), array('escape' => false,'class' => 'table-link danger')); ?>-->
									
	
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>

				<?php echo $this->element('paginate'); ?>
				
			</div>
					<div class="row" id="editheure">
					<div class="col-lg-12">
						<div class="form_style">
							

				<div class="form-group" id="frmAdd">
								<!-- <div class="col-lg-3">
								<input type="text" name="txtau"  id="txtdu" >
								</div>
								<div class="col-lg-3">
								<input type="text" name="txtdu"  id="txtau" >
								</div>
								<div class="col-lg-3">
								<input type="text" name="txtpriode"  id="txtp" value="matinee">
								</div> -->
					<div class="col-lg-12">
						<div class="col-lg-6">
						<label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('du',array('class'=>'form-control','id'=>'txtdu','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="col-lg-6">
						<div class="main-box">
                          <label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('au',array('class'=>'form-control','id'=>'txtau','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						</div>
						<div class="col-lg-6">
						<div class="main-box">
                          <label><?php echo __('Heure départ'); ?> </label>
							    <?php echo $this->Form->input('periode',array('class'=>'form-control','value'=>'matinee','id'=>'txtp','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						</div>
						
					</div>	
								<div class="col-lg-3">
							<p><button class="btn btn-default" id="btnAddAction" name="submit" onClick="callCrudAction('add','')"><?php echo __('Ajouter'); ?></button></p>
							
							<!-- <p><button id="btnAddAction" name="submit" onClick="callCrudAction('delete','9')">delete</button></p> -->

						</div>
							</div>
							<img src=<img src=<?php echo $this->webroot.'img/ajax_loader.gif' ?> id="loaderIcon" style="display:none" />
						</div>
						
					</div>	
				</div>			
			</div>
			<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
			</div>
			</div>
 
		</div>

</div>
 

<script  type="text/javascript">
	
			$(document).ready(function(){
				$(".ch_filtrer").change(function(){
				//alert("eq");
			     var q=$(this).val();
			    alert(q);
			      document.getElementById("id").value =q;
			      //$idp=q; 	 
			    // $(document).elements["text"].value="NOUVEAU"
                 });
				$(".ch_filtrerMJ").change(function(){
				//alert("eq");
			     var qm=$(this).val();
			    // alert(q);
			      document.getElementById("idm").value =qm;
			      //$idp=q; 	 
			   
                 }); 
				$("#liste1").on('click', function(){

				
			      $('#myModal').modal('show');
			      //$idp=q; 	 
			   
                 });
  
     		});

          
         /*$('#myModal').on('loaded.bs.modal', function (e) {
         	//alert("load");
  		 //var modal = $(this);
  		// alert(modal);
  		// $('#editheure, .modal').hide();
 
	     });*/

    $('#myModal').on('show.bs.modal', function (event) {
  		 var modal = $(this);
  		// alert(modal);
  		document.getElementById('editheure').style.display = "none";
  		 /*$(".modal").on('click', function(){
   				//var button = $(event.relatedTarget);
   				//alert("button");
				//alert("bouton");			
				//$('#modalEdit').modal('show')
     		});*/
 
	});
   $('#myModal').on('shown.bs.modal', function (event) {

   var modal = $(this);
  // alert(modal);
   //alert("afficher");
   
   $("#bt").on('click', function(){
   	/*var button = $(event.relatedTarget);
   alert(button);*/
				//alert("boutondd");
				hideThis('editheure');
				//$('#myModal').modal('show');
				id=null;

     		}); 

   $("#delete").on('click', function(){
				alert("dd");
				var name=$("#delete").attr('name');
				var obj = document.getElementById("delete");
				alert(name);
				
			     // $('#myModal').modal('show');
			      //$idp=q; 	 
			 alert($("#delete").val());
			 if(window.confirm("Etes-vous sûr que vous voulez supprimer ")){
                //if confirmed, submit the form
               callCrudAction("delete",name);
               var url = '<?php echo Router::url(array('controller'=>'anneesscollaires','action'=>'crud_action')) ?>/0/0/0/0/0';
                alert("-x");
                $.get(url, function(data) {
					$(".xtable").html(data);
					//alert("Data: " + data);
					alert("x");
				});
                //$('#myModal').modal('show');
                //$('#myModal, a.close').remove(); 
                //$('#myModal').modal('show');

	      	return false;
     		
            }else{
                //cancel logic here
            }
                 });
});

   function hideThis(_div){
  // 	alert("hide");
    var obj = document.getElementById(_div);
    if(obj.style.display == "block")
        obj.style.display = "none";
    else
        obj.style.display = "block";
}
(function($) {
   $.fn.ApplyTask = function() {
       var h_id = $(this).data('id');
       var h_du= $(this).data('du');
       var h_a= $(this).data('au');
       var h_periode= $(this).data('periode');

       $.ajax({
           url: 'applyTask',
           cache: false,
           type: 'POST',
           data: {
               id: h_id,
               du: h_du,
               au: h_a,
               periode: h_periode
              
           },
           success: function(data) {

               }
           });
       };
   })($);

function callCrudAction(action,id) {
	//$("#loaderIcon").show();
//alert($('#myModal').attr('action'));
	var queryString;
	var du;
	var au;
	var periode;
if(action=="add"){
	du= $("#txtdu").val();
	au= $("#txtau").val();
	periode= $("#txtp").val();
    id=$("#delete").attr('name');
}
if(action=="delete"){
   	alert("test");
   	du= "0";
	au= "0";
	periode="0";
}
	alert("ajax"+id+action+du);
        $.ajax({
            dataType: "html",
            type: "POST",
            //evalScripts: true,
            url: '<?php echo Router::url(array('controller'=>'anneesscollaires','action'=>'crud_action'));?>'+'/'+du+'/'+au+'/'+periode+'/'+action+'/'+id,
            data: ({du:du,au:au,periode:periode}),
           
            success: function(data) {
            	document.getElementById('editheure').style.display = "none";
		        $('#rfrmAdd').html(data);
		      }
            

        });return false;
	//$('#myModal').modal('show');
}
   
	</script>



