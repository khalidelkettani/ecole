<div class="salles view">
<h2><?php echo __('Salle'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($salle['Salle']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($salle['Salle']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Capacite'); ?></dt>
		<dd>
			<?php echo h($salle['Salle']['capacite']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Equipements'); ?></dt>
		<dd>
			<?php echo h($salle['Salle']['equipements']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Salle'), array('action' => 'edit', $salle['Salle']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Salle'), array('action' => 'delete', $salle['Salle']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $salle['Salle']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Salles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salle'), array('action' => 'add')); ?> </li>
	</ul>
</div>
