 <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <div class="container-fluid">
                            <a class="brand" href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => 'dashboard')); ?>"><i class="icon-home icon-white"></i> <?php echo __('RH QUEBEC'); ?></a>
                            <ul class="nav user_menu pull-right">
                                <li>
                                        <?php  echo $this->Html->link('FR', array_merge(array('language'=>'fre'),array_values($this->params['pass']))); ?>
                                      <?php //echo __('Français'); ?></b>
                                    
                                </li>
                                <li class="divider-vertical hidden-phone hidden-tablet"></li>
                                <li>
                                    <?php  echo $this->Html->link('EN', array_merge(array('language'=>'eng'),array_values($this->params['pass']))); ?>
                                </li>
                                  
                                <li class="divider-vertical hidden-phone hidden-tablet"></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> <?php echo AuthComponent::user('username'); ?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                         <li>
                                                    <a href="<?php echo $this->Html->url(array('controller' => 'offres','action' => 'jobs',AuthComponent::user('id'))); ?>" target="_blank"><i class="splashy-folder_classic_down"></i> <?php echo __('Espace Recrutement');?></a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'change_password')); ?>"><i class="splashy-lock_large_unlocked"></i> <?php echo __('Changer mot de passe');?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'logout')); ?>"><i class="splashy-lock_large_locked"></i> <?php echo __('Déconnexion');?>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <a data-target=".nav-collapse" data-toggle="collapse" class="btn_menu">
                                <span class="icon-align-justify icon-white"></span>
                            </a>
                            <nav>
                                <div class="nav-collapse">
                                    <ul class="nav">
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/dashboard')); ?>"><i class="icon-list-alt icon-white"></i> <?php echo __('Tableaux de Bord');?></a>
                                          
                                        </li>
                                      
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'offres','action' => 'index')); ?>"><i class="icon-list-alt icon-white"></i> <?php echo __('Missions');?>
                                            </a>
                                          
                                        </li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-th icon-white"></i> <?php echo __('CV-thèque');?> <b class="caret"></b></a>
                                           
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/index')); ?>"><i class="splashy-group_grey_edit"></i> <?php echo __('Candidatures');?></a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidats','action' => '/index')); ?>"><i class="splashy-group_grey"></i> <?php echo __('Candidats');?></a>
                                                </li>
                                             
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/report')); ?>"><i class="icon-list-alt icon-white"></i> <?php echo __('Rapport');?></a>
                                          
                                        </li>
                                      

                                           <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="splashy-sprocket_light"></i> <?php echo __('Config');?><b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                
                                            <li class="dropdown">
                                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-th"></i> <?php echo __('Questions');?> <b class="caret-right"></b></a>
                                                    
                                                    <ul class="dropdown-menu">
                                                        <!-- <li>
                                                            <a href="<?php //echo $this->Html->url(array('plugin' => null,'controller' => 'tags','action' => 'index')); ?>"><i class="splashy-tag"></i> <?php echo __('Tags de questions');?></a>
                                                        </li> -->
                                                        <li>
                                                            <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'questions','action' => 'index')); ?>">
                                                                <i class="splashy-documents"></i> <?php echo __('Groupe de questions');?>
                                                            </a>
                                                        </li>
                                                        
                                                    </ul>
                                                </li>
                                                <li class="divider"></li>
                                                   <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="splashy-sprocket_dark"></i> <?php echo __('Paramétrage');?> <b class="caret-right"></b></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'responsables','action' => 'index')); ?>"><i class="splashy-document_copy"></i> <?php echo __('Chargés de recrutement');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'contrats','action' => 'index')); ?>"><i class="splashy-document_copy"></i> <?php echo __('Types des contrats');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                 <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'postes','action' => 'index')); ?>"><i class="splashy-hcard"></i> <?php echo __('Liste des postes');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'secteurs','action' => 'index')); ?>"><i class="splashy-documents"></i> <?php echo __('Secteurs d\'activité');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'filaires','action' => 'index')); ?>"><i class="splashy-home_grey"></i> <?php echo __('Liste des filiéres');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'villes','action' => 'index')); ?>"><i class="splashy-marker_rounded_grey_2"></i> <?php echo __('Liste des villes');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'pays','action' => 'index')); ?>"><i class="splashy-marker_rounded_grey_2"></i> <?php echo __('Liste des pays');?>
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" style="font-weight:bolder" href="#"> <?php echo __('PIM');?> <b class="caret"></b></a>
                                           
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'index')); ?>">
                                                     <?php echo __('Liste des employés');?></a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'addemp')); ?>">
                                                     <?php echo __('Ajouter un employé');?></a>
                                                </li>
                                                <li class="divider"></li>
                                                <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="splashy-sprocket_dark"></i> <?php echo __('Paramétrage');?> <b class="caret-right"></b></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'unites','action' => 'index')); ?>"> <?php echo __('Sous Unités');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'methodologiesreporters','action' => 'index')); ?>"> <?php echo __('Méthodologie de Reporting');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php //echo $this->Html->url(array('plugin' => null,'controller' => 'contrats','action' => 'index')); ?>"> <?php echo __('Raisons de résiliation');?>
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                 <li>
                                                    <a href="<?php //echo $this->Html->url(array('plugin' => null,'controller' => 'postes','action' => 'index')); ?>"> <?php echo __('Modèles de document');?>
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                             
                                            </ul>
                                        </li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>



