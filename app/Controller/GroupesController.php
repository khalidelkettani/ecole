<?php
App::uses('AppController', 'Controller');
/**
 * Villes Controller
 *
 * @property Ville $Ville
 * @property PaginatorComponent $Paginator
 */
class GroupesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des groupes'));
		$this->Groupe->recursive = 0;

		$data = $this->Paginator->paginate('Groupe',array('Groupe.user_id' => $this->Auth->user('id')));
    	$this->set('groupes', $data);
    	//$pays = $this->Ville->Pay->find('list',array('fields'=>array('id','libelle'),
						//'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		$filaires = $this->Groupe->Filaire->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Filaire.user_id' => $this->Auth->user('id'))));
		$this->set(compact('filaires'));


	}

	public function search($q = null) {
		$this->layout=null;
		$this->Groupe->recursive = 0;
		$this->set('groupes', $this->Paginator->paginate('Groupe',array('Groupe.libelle LIKE'=>'%'.$q.'%')));   //, 'Ville.user_id' => $this->Auth->user('id')
	    $filieres = $this->Groupe->Flaire->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Groupe.user_id' => $this->Auth->user('id'))));
		$this->set(compact('filieres'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		$idu= $this->Ville->find('first', array(
        	'conditions' => array('Ville.id' => $id)));

		if (!$this->Ville->exists($id) || $idu['Ville']['user_id'] != $this->Auth->user('id')) {
			//throw new NotFoundException(__('Invalid candidat'));
			$this->Session->setFlash(__('Cette Ville est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}
*/
/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = null;
		if ($this->request->is('post')) {
			$this->request->data['Groupe']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Groupe->create();
			
			if ($this->Groupe->save($this->request->data)) {
				$this->Session->setFlash(__('Le Groupe a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le Groupe n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		$filaires = $this->Groupe->Filaire->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Filaire.user_id' => $this->Auth->user('id'))));
		$this->set(compact('filaires'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = null;
		$idu= $this->Groupe->find('first', array(
        	'conditions' => array('Groupe.id' => $id)|| $idu['Groupe']['user_id'] != $this->Auth->user('id')));

		if (!$this->Groupe->exists($id)) {
			$this->Session->setFlash(__('Ce Groupe est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Groupe->save($this->request->data)) {
				$this->Session->setFlash(__('Le Groupe a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le Groupe n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Groupe.' . $this->Groupe->primaryKey => $id));
			$this->request->data = $this->Groupe->find('first', $options);
		}
		$filaires = $this->Groupe->Filaire->find('list',array('fields'=>array('id','libelle'),'conditions' => array('Filaire.user_id' => $this->Auth->user('id'))));
		$this->set(compact('filaires'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Groupe->id = $id;
		$idu= $this->Groupe->find('first', array(
        	'conditions' => array('Groupe.id' => $id)));

		if (!$this->Groupe->exists($id)|| $idu['Groupe']['user_id']!= $this->Auth->user('id')) {  
			$this->Session->setFlash(__('Ce Groupe est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Groupe->delete()) {
				$this->Session->setFlash(__('Le Groupe a été supprimée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('Le Groupe n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Groupe->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}


}
