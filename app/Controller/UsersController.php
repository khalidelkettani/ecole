<?php

class UsersController extends AppController {

   public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
    $this->Auth->allow('add', 'logout');
}

public $components = array('Paginator','Session');
/*public $components = array( 'Auth' => array(
        'authenticate' => array(
            'Blowfish' => array(
                'fields' => array('username' => 'email'))
            ),
        )
        , 'Session', 'Email','Security','DebugKit.Toolbar');*/
    
    public function index() {
         $this->layout = __('layout_admin_cube');
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
         $roles = array('Admin' => 'Admin', 'User' => 'User'); 
        $this->set(compact('users','roles' ));
    }

    public function view($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('User', $this->User->findById($id));
        $roles = array('Admin' => 'Admin', 'User' => 'User'); 
        $this->set(compact('users','roles' ));
    }

    public function add() {
        $this->layout = __('layout_admin_cube');
        if ($this->request->is('post')) {
            $this->User->create();
            //debug($this->request->data);die();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        }
        $roles = array('Admin' => 'Admin', 'User' => 'User'); 
        $this->set(compact('users','roles'));
       
    }

    public function edit($id = null) {
         $this->layout = __('layout_admin_cube');
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    public function delete($id = null) {
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');

        $this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Flash->success(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Flash->error(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

public function login() {
    //$this->layout = null;
       $this->layout = ('layout_login_cube');
        $this->set('title_for_layout', __('Connexion'));
        if ($this->request->is('post')) {
         //$this->Flash->error(__('test username or password is ok'));
        if ($this->Auth->login()) {
            return $this->redirect($this->Auth->redirectUrl());
        }
        $this->Session->setFlash(__('Invalide  mot de passe. S\'il vous plaît essayer de nouveau!'),'alert alert-error',array('class'=>'alert alert-error'));
        /*$this->Flash->error(__('Invalid username or password, try again'), 
            ['key' => 'auth']);*/
    }

}

/*public function login()
{
     $this->layout = __('layout_admin_cube');
    if ($this->request->is('post')) {
        $user = $this->Auth->identify();
        if ($user) {
            $this->Auth->setUser($user);
            return $this->redirect($this->Auth->redirectUrl());
        } else {
            $this->Flash->error(__("Nom d'utilisateur ou mot de passe incorrect"), [
                'key' => 'auth'
            ]);
        }
    }
}*/
public function logout() {
    return $this->redirect($this->Auth->logout());
}
}

