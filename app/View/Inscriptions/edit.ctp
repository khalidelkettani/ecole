 <?php echo $this->Form->create('Inscription'); ?>
 <?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
 <div class="row-fluid">
                        <div class="span3">
							&nbsp;
                        </div>
                        <div class="col-lg-6">
                        <div class="span6">
							<h2 class="heading"><?php echo __('Modifier inscription'); ?></h2>
							<?php echo $this->Form->create('Inscription',array('class'=>'form_validation_reg')); ?>
							<div class="form-group">	
								<div class="formSep">
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Eléve '); ?> <span class="f_req">*</span></label>
											<?php echo $this->Form->select('eleves_id', $eleves, array('class'=>'form-control','empty' =>__('Sélectionnez un élève'),'required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis'))); ?>
										</div>
									</div>
								</div>
							</div>

						<div class="form-group">
							

							<div class="span12">
								<label><?php echo __('Date d\'inscription de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
								<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

								<?php echo $this->Form->input('date',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
							
							    </div>
							</div>
					</div>
					<div class="form-group">
							<label><?php echo __('Remarques'); ?> *</label>
							    <?php echo $this->Form->input('remarques',array('class'=>'form-control','div'=>false,'placeholder'=>__('Remarques'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            <!-- Fin adresse -->  
						</div>
								
								
                        </div>
                         <div class="span3">
							&nbsp;
                        </div>
							<div class="form-actions">
									<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER'); ?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'inscriptions','action' => 'index',$idC,$idA)); ?>"><?php echo __('ANNULER'); ?></a>
							</div>
							<?php echo  $this->Form->end(); ?>
                    </div>
                    </div>
<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>