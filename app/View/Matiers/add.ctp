                    <div class="row-fluid">
                        <div class="span3">
							&nbsp;
                        </div>
                        <div class="span6">
							<h2 class="heading"><?php echo __('Ajouter une matiére'); ?></h2>
							<?php echo $this->Form->create('Matier',array('class'=>'form_validation_reg')); ?>
								<div class="formSep">
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Nom de la matiére'); ?> <span class="f_req">*</span></label>
											<?php echo $this->Form->input('libelle',array('class'=>'span12','label'=> false, 'type' =>'text', 'placeholder'=>__('Entrer le nom de la matiére')));?>
										</div>
										
									</div>
								</div>
								<div class="formSep">
									<div class="row-fluid">
										<div class="span12">
											<label><?php echo __('Region d\'appartenance'); ?> <span class="f_req">*</span></label>
											<?php echo $this->Form->select('region_id', $regions, array('class' => 'span12', 'empty' =>__('Sélectionnez la region'))); ?>
										</div>
										
									</div>
								</div>
								
								
								<div class="form-actions">
									<button class="btn btn-success" type="submit"><?php echo __('ENREGISTRER'); ?></button>
									<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'villes','action' => 'index')); ?>"><?php echo __('ANNULER'); ?></a>
								</div>
							<?php echo  $this->Form->end(); ?>
                        </div>
                         <div class="span3">
							&nbsp;
                        </div>

                    </div>
