<?php
App::uses('AppController', 'Controller');
/**
 * Nationalites Controller
 *
 * @property Nationalite $Nationalite
 * @property PaginatorComponent $Paginator
 */
class NationalitesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des nationalités'));

		$this->Nationalite->recursive = 0;
		$data = $this->Paginator->paginate('Nationalite');
    	$this->set('nationalites', $data);
		$pays = $this->Nationalite->Pay->find('list',array('fields'=>array('id','libelle')));
		$this->set(compact('pays'));
	}

	public function search($q = null) {
		$this->layout=null;
		$this->Nationalite->recursive = 0;
		$this->set('nationalites', $this->Paginator->paginate('Nationalite',array('Nationalite.libelle LIKE'=>'%'.$q.'%')));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Nationalite->exists($id)) {
			throw new NotFoundException(__('Invalid nationalite'));
		}
		$options = array('conditions' => array('Nationalite.' . $this->Nationalite->primaryKey => $id));
		$this->set('nationalite', $this->Nationalite->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout=null;
		if ($this->request->is('post')) {
			$this->request->data['Nationalite']['user_id'] = $this->Auth->user('id'); 
			$this->Nationalite->create();
			if ($this->Nationalite->save($this->request->data)) {
				$this->Session->setFlash(__('La nationalité a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La nationalité n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		$users = $this->Nationalite->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout=null;
		if (!$this->Nationalite->exists($id)) {
			throw new NotFoundException(__('Invalid nationalite'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Nationalite->save($this->request->data)) {
				$this->Session->setFlash(__('La nationalité a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La nationalité n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Nationalite.' . $this->Nationalite->primaryKey => $id));
			$this->request->data = $this->Nationalite->find('first', $options);
		}
		$pays = $this->Nationalite->Pay->find('list',array('fields'=>array('id','libelle')));
		$this->set(compact('pays'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Nationalite->id = $id;
		$idu= $this->Nationalite->find('first', array(
        	'conditions' => array('Nationalite.id' => $id)));
		if (!$this->Nationalite->exists($id)|| $idu['Nationalite']['user_id'] != $this->Auth->user('id') ) {
			throw new NotFoundException(__('Invalid nationalite'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Nationalite->delete()) {
				$this->Session->setFlash(__('La nationalité a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('La nationalité n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Nationalite->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}
