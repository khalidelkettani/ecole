<?php
App::uses('InscriptionsController', 'Controller');

/**
 * InscriptionsController Test Case
 */
class InscriptionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.inscription',
		'app.eleves',
		'app.classes',
		'app.annesscollaires'
	);

}
