<?php
App::uses('AppModel', 'Model');
/**
 * Inscription Model
 *
 * @property Eleves $Eleves
 * @property Classes $Classes
 * @property Annesscollaires $Annesscollaires
 */
class Inscription extends AppModel {

public function isOwnedBy($post, $user) {
    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
}

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'eleves_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'classes_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'date' => array(
			'date' => array(
				'rule' => array('date'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'anneesscollaires_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Elef' => array(
			'className' => 'Elef',
			'foreignKey' => 'eleves_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Classses' => array(
			'className' => 'Classses',
			'foreignKey' => 'classes_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Anneesscollaires' => array(
			'className' => 'Anneesscollaires',
			'foreignKey' => 'annesscollaires_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/*public function nombre($id,$userID){
		debug($userID);die();
		$result = $this->Inscription->find('all', array(
			'conditions' => array(
			'Inscription.id' => $id,
			'Inscription.user_id' => $userID,
			/*'Inscription.recurence' => 1,
			'Inscription.type' => 17,
		),
			'fields' => array('COUNT(Inscription.id) AS ma_Nbre'),
		));
		return  $result[0][0]['ma_Nbre'];
	}*/
}
