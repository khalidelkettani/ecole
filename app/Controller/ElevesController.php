<?php
App::uses('CakeEmail', 'Network/Email');
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
/**
 * Eleves Controller
 */
class ElevesController extends AppController {

/**
 * Scaffold
 *
 * @var mixed
 */
	//public $scaffold;
public $components = array('Paginator','Session');

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Eleves';
/**
 * Preset vars
 *
 * @var array $presetVars
 * @link https://github.com/CakeDC/search
 */
	public $presetVars = true;

	public $ch='';


	public function SetChaine($chaine=null){
		$this->ch=$chaine;

	}
	public function GetChaine(){
		return $this->ch;

	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des élèves'));

		$this->Elef->recursive = 0;
		$data = $this->Paginator->paginate('Elef');
    	$this->set('eleves', $data);

    	$villes = $this->Elef->Ville->find('list',array('fields'=>array('id','libelle')));
    	$nationalites = $this->Elef->Nationalite->find('list',array('fields'=>array('id','libelle')));
    	$tableau = array('Masculin', 'Feminin'); 
    	$this->set(compact('villes','tableau' ,'nationalites'));
	}
	public function search($q = null) {
		$this->layout=null;
		$this->Elef->recursive = 0;
		$this->set('eleves', $this->Paginator->paginate('Elef',array('Elef.nom LIKE'=>'%'.$q.'%')));   //, 'Ville.user_id' => $this->Auth->user('id')
	

	}

	public function view($id = null) {
		$this->layout = __('layout_admin_cube');
		$idu= $this->Elef->find('first', array(
        	'conditions' => array('Elef.id' => $id)));

		if (!$this->Elef->exists($id) ) {
			$this->Session->setFlash(__('Cet élève est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$options = array('conditions' => array('Elef.' . $this->Elef->primaryKey => $id));
		$this->request->data = $this->Elef->find('first', $options);
		$villes = $this->Elef->Ville->find('list',array('fields'=>array('id','libelle')));
		$sexes = array('Masculin' => 'Masculin', 'Feminin' => 'Feminin'); 
		//$pays = $this->Elef->Pay->find('list',array('fields'=>array('id','libelle')));
    	$this->set(compact('villes','sexes' ));
	}

/*
	public function edit($id = null) {
		$this->layout = __('layout_admin_cube');
		//$this->set('title_for_layout', __('Liste des élèves'));

		$idu= $this->Elef->find('first', array(
        	'conditions' => array('Elef.id' => $id)|| $idu['Elef']['user_id'] != $this->Auth->user('id')));

		if (!$this->Elef->exists($id) ) {
			$this->Session->setFlash(__('Cet élève est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {

            $oldlogo=$idu['Elef']['logo'];
           // debug($id.$oldlogo);die();
            $cfile=strtolower($this->request->data['Elef']['avatar_logo']['name']);
            $extension=strtolower(pathinfo($this->request->data['Elef']['avatar_logo']['name'],PATHINFO_EXTENSION));
            $tmpfile=$this->request->data['Elef']['avatar_logo']['tmp_name'];

			if ($this->Elef->save($this->request->data)) {
				if (!empty($tmpfile)){
					 //$extension=strtolower(pathinfo($this->request->data['Elef']['avatar_logo']['name'],PATHINFO_EXTENSION));
               debug($extension);die();
           }else{
           	 debug('ttttttttttttttt');die();
           }
              
               if($this->SavePicture($idu)){
// debug($id.$oldlogo);die();
                 	if($this->imageExist( $id.$oldlogo)){
	                 $this->afterDelete( $id.$oldlogo);
                  	}
                 
               	    $id_article = $id;
                    $logo=$idu['Elef']['logo'];
                    //On met à jour le champ img_principal
                    $this->Elef->saveField('logo', $id_article.'.'. $extension, array('validate'=>false, 'callbacks'=>false));  
                    $this->Session->setFlash(__('L\' élève a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				    return $this->redirect(array('action' => 'index'));

               }else  {
                	$this->Session->setFlash(__('Vous ne pouvez envoyer ce type de fichier.'),'alert alert-success',array('class'=>'alert alert-success'));
                }
	 
			} else {
				$this->Session->setFlash(__('L\' élève n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Elef.' . $this->Elef->primaryKey => $id));
			$this->request->data = $this->Elef->find('first', $options);
		}
		$villes = $this->Elef->Ville->find('list',array('fields'=>array('id','libelle')));
		$nationalites = $this->Elef->Nationalite->find('list',array('fields'=>array('id','libelle')));
    	  	
		$sexes = array('Masculin' => 'Masculin', 'Feminin' => 'Feminin'); 
		//$pays = $this->Elef->Pay->find('list',array('fields'=>array('id','libelle')));
    	$this->set(compact('villes','sexes','nationalites' ));
		//$pays = $this->Elef->Pay->find('list',array('fields'=>array('id','libelle'),
						//'conditions' => array('Pay.user_id' => $this->Auth->user('id'))));
		//$this->set(compact('pays'));
	}
	*/
	public function add($id = null) {
		$this->layout = __('layout_admin_cube');

		if (!empty($this->request->is('post'))) {
			//debug($this->request->data);die();
			
			$this->request->data['Elef']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Elef->create();
			//debug($this->request->data);die();
			if ($this->Elef->save($this->request->data)) 
			{
				//$this->Elef->afterSave(true);
                 $this->Session->setFlash(__('Le Professeur  a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				 return $this->redirect(array('action' => 'index'));

              			
			} else {
				$this->Session->setFlash(__('Le Professeur n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		
		$villes = $this->Elef->Ville->find('list',array('fields'=>array('id','libelle')));
		$nationalites = $this->Elef->Nationalite->find('list',array('fields'=>array('id','libelle')));
		$sexes = array('Masculin' => 'Masculin', 'Feminin' => 'Feminin'); 
		$this->set(compact('villes','sexes','nationalites' ));
	}

	public function edit($id = null) {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des élèves'));

		$idu= $this->Elef->find('first', array(
        	'conditions' => array('Elef.id' => $id)|| $idu['Elef']['user_id'] != $this->Auth->user('id')));

		if (!$this->Elef->exists($id) ) {
			$this->Session->setFlash(__('Cet élève est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Elef']['validated']='true';
             if ($this->Elef->save($this->request->data)) {
             
				$this->Session->setFlash(__('L\' élève a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
			$this->Session->setFlash(__('L\' élève n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Elef.' . $this->Elef->primaryKey => $id));
			$this->request->data = $this->Elef->find('first', $options);
		}
		$villes = $this->Elef->Ville->find('list',array('fields'=>array('id','libelle')));
		$nationalites = $this->Elef->Nationalite->find('list',array('fields'=>array('id','libelle'))); 	
		$sexes = array('Masculin' => 'Masculin', 'Feminin' => 'Feminin'); 
	   	$this->set(compact('villes','sexes','nationalites' ));
	}
	
/**
 * add method
 *
 * @return void
 */
	
/*
public function add($id = null) {
		$this->layout = __('layout_admin_cube');

		if (!empty($this->request->is('post'))) {
			//debug($this->request->data);die();
			
			$this->request->data['Elef']['user_id'] = $this->Auth->user('id');   //['user_id'] = $this->Auth->user('id')
			$this->Elef->create();
			
			if ($this->Elef->save($this->request->data)) 
			{
				$extension=strtolower(pathinfo($this->request->data['Elef']['avatar_logo']['name'],PATHINFO_EXTENSION));
              
               if (!empty($this->request->data['Elef']['avatar_logo']['tmp_name']) &&
               	 in_array($extension,array('jpg','png'))){
 
                 move_uploaded_file($this->request->data['Elef']['avatar_logo']['tmp_name'], IMAGES.'eleves'.DS.$this->Elef->id.'.'.$extension);
                 $id_article = $id;
                
                //On met à jour le champ img_principal
              $this->Elef->saveField('logo', $id_article.'.'. $extension, array('validate'=>false, 'callbacks'=>false));  
                // $this->SomeModel->saveAll($this->request->data, array('fieldList' => array('logo',$extension)));
             //$this->Elef->saveFilde('logo', $extension);
                 $this->Session->setFlash(__('Le Professeur  a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				 return $this->redirect(array('action' => 'index'));

                }else  {
                	$this->Session->setFlash(__('Vous ne pouvez envoyer ce type de fichier.'),'alert alert-success',array('class'=>'alert alert-success'));
                }
				
			} else {
				$this->Session->setFlash(__('Le Professeur n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		
		$villes = $this->Elef->Ville->find('list',array('fields'=>array('id','libelle')));
		$nationalites = $this->Elef->Nationalite->find('list',array('fields'=>array('id','libelle')));
		$sexes = array('Masculin' => 'Masculin', 'Feminin' => 'Feminin'); 
		$this->set(compact('villes','sexes','nationalites' ));
	}
*/
	
public function delete($id = null) {
		$this->Elef->id = $id;
		
		$idu= $this->Elef->find('first', array(
        	'conditions' => array('Elef.id' => $id)|| $idu['Elef']['user_id'] != $this->Auth->user('id')));
        $logo=$idu['Elef']['logo'];
		if (!$this->Elef->exists($id)) {
			$this->Session->setFlash(__('Cet élève est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Elef->delete()) {
				$this->Session->setFlash(__('Cet élève a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
			
			//	debug($id.$logo);die();
		} else {
				$this->Session->setFlash(__('La ville n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	// app/Controller/PostsController.php

public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Elef->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}


	public  function IsImage($data=array()) 
		{
					
				if(getimagesize($data['logo']['tmp_name'])) {return true;} 
				else {return false;}
					
				
	}	


	/*public  function SavePicture($idu=null) 
		{ 		
							    
				$extension=strtolower(pathinfo($idu['Elef']['avatar_logo']['name'],PATHINFO_EXTENSION));
              debug($extension);die();
               if (!empty($this->request->data['Elef']['avatar_logo']['tmp_name']) &&
               	 in_array($extension,array('jpg','png'))){
 
                // move_uploaded_file($this->request->data['Elef']['avatar_logo']['tmp_name'], IMAGES.'eleves'.DS.$this->Elef->id.'.'.$extension);
                 $id_article = $id;
                debug($this->request->data['Elef']['avatar_logo']['tmp_name'], IMAGES.'eleves'.DS.$this->Elef->id.'.'.$extension);die();
                //On met à jour le champ img_principal
             // $this->Elef->saveField('logo', $id_article.'.'. $extension, array('validate'=>false, 'callbacks'=>false));  
                // $this->So
															
			if( move_uploaded_file($this->request->data['Elef']['avatar_logo']['tmp_name'], IMAGES.'eleves'.DS.$this->Elef->id.'.'.$extension)){
											return true;
										}
										else {return false;}
					
			}			
		}*/

	public  function random($car=null) 
		{
			$string = "";
				$chaine = "abcdefghijklmnpqrstuvwxy1238754311320";
				srand((double)microtime()*1000000);
				for($i=0; $i<$car; $i++) {
				$string .= $chaine[rand()%strlen($chaine)];
				}
				return $string;
		}

/*public function afterDelete($check=null){

	$oldefile=IMAGES.'eleves'.DS.$check;
	//debug($oldefile);die();
	if(!empty($oldefile)){
		
        if(file_exists($oldefile)){
             unlink($oldefile);
         }
	}
 }*/

// function to check if file already exists
/*public function imageExist($check) {
 //   $picturename = $check['logo'];
  //  $path = IMAGES.'eleves'.DS.$this->Elef->id.'.'.Elef->logo;
    //$file = $check;

    if (file_exists($check)) {
        return false;
    } else {
        return true;
    }
}*/


}
