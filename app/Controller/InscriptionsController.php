<?php
App::uses('AppController', 'Controller');
/**
 * Inscriptions Controller
 */
class InscriptionsController extends AppController {

public $components = array('Paginator','Session');

/**
 * Scaffold
 *
 * @var mixed
 */
public function index($idC=null,$idA=null,$prs=null) {
	//debug($prs);
		$this->layout = __('layout_admin_cube');
		$this->loadModel('Classs');
		$this->loadModel('Anneesscollaire');
		//$this->set('title_for_layout', __('Liste des classes'));
		//$this->Inscription->recursive = 2;
		$data = $this->Paginator->paginate(array('classes_id' => $idC,'annesscollaires_id' => $idA));  //array('Classs.user_id' => $this->Auth->user('id'))
    	$this->set('inscriptions', $data);
    	$classes=$this->Classs->find('first',array('fields'=>array('id','libelle','capacite'),
    		                                      'conditions'=>array('Classs.id'=>$idC)));
    	$annees=$this->Anneesscollaire->find('first',array('fields'=>array('id','libelle'),
    		                                      'conditions'=>array('Anneesscollaire.id'=>$idA)));
        $this->set(compact('classes','prs','annees','idC','idA'));
	}
	/**
 * delete method
  *
 */
	public function delete($id = null,$idC = null,$prs=null,$idA = null) {
		debug($prs);
		debug($id);
		$this->loadModel('Classs');
		$this->loadModel('Anneesscollaire');
		$this->Inscription->id = $id;
		$idu= $this->Inscription->find('first', array(
        	'conditions' => array('Inscription.id' => $id)));

		if (!$this->Inscription->exists($id) || $idu['Inscription']['user_id'] != $this->Auth->user('id')) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id'))
			$this->Session->setFlash(__('Cette inscription est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index',$idC,$idA,$prs));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->Inscription->delete()) {
				$this->Session->setFlash(__('L\'inscription a été supprimée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		
		} else {
				$this->Session->setFlash(__('L\'inscription n\'a pas pu être supprimée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index',$idC,$idA,$prs));
		$classes=$this->Classs->find('first',array('fields'=>array('id','libelle','capacite'),
    		                                      'conditions'=>array('Classs.id'=>$id)));
    	$annees=$this->Anneesscollaire->find('first',array('fields'=>array('id','libelle'),
    		                                      'conditions'=>array('Anneesscollaire.id'=>$idA)));
        $this->set(compact('classes','annees','idC','idA','prs'));
	}

/**
 * add method
 *
 * @return void
 */
//$this->params['named']['page']
	public function add($idC=null,$idA=null,$prs=null) {
		//debug($prs);
		//debug($ido);
		$this->layout = __('layout_admin_cube');
		$this->loadModel('Elef');

		if ($this->request->is('post')) {
			$this->request->data['Inscription']['user_id'] = $this->Auth->user('id');
			$this->request->data['Inscription']['classes_id'] =$idC;
			$this->request->data['Inscription']['annesscollaires_id'] =$idA;
			$this->Inscription->create();
			if ($this->Inscription->save($this->request->data)) {
				$this->Session->setFlash(__('L\'inscription a étée enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index',$idC,$idA,$prs));
			} else {
				$this->Session->setFlash(__('L\'inscription a n\'a pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		$eleves = $this->Inscription->Elef->find('list',array('fields'=>array('id','nom','date_sortie')));
		 	$this->set(compact('eleves','idC','idA','prs'));
	}

	/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null,$idC=null,$idA=null) {
		
		$this->layout = __('layout_admin_cube');
		$this->loadModel('Elef');
		$idu= $this->Inscription->find('first', array(
        	'conditions' => array('Inscription.id' => $id)|| $idu['Inscription']['user_id'] != $this->Auth->user('id')));
     
		if (!$this->Inscription->exists($id)) {
			
			$this->Session->setFlash(__('Cette inscription est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index',$idC,$idA));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Inscription->save($this->request->data)) {

				$this->Session->setFlash(__('L\' inscription a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index',$idC,$idA));
			} else {
				$this->Session->setFlash(__('L\'inscription n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Inscription.' . $this->Inscription->primaryKey => $id));
			$this->request->data = $this->Inscription->find('first', $options);
		}

		$eleves = $this->Inscription->Elef->find('list',array('fields'=>array('id','nom','date_sortie')));
		 	$this->set(compact('eleves','idC','idA'));
	}


public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Inscription->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}
