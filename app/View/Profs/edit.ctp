<?php echo $this->Form->create('Prof'); ?>
	<?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Modifier un prof'); ?></h4>
				</div>
				<div class="modal-body">
						<div class="form-group"> <!---->
							<label><?php echo __('Nom'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('nom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom '),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
                        <div class="form-group"> <!---->
							<label><?php echo __('Prénom'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('prenom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prénom de professeur'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
 	
 	                    <div class="form-group"> <!---->
							<label><?php echo __('Civilite '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->select('civilite',$civilites,array('class'=>'form-control','div'=>false,'placeholder'=>__('Civilite'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>

						</div>
						

						 <div class="form-group form-group-select2">
							<label>
								<?php echo __('Matièr'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('matier_id',$matiers,array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div> 
						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Date d\'entrée'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->input('date_entree',array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div> 
						<div class="form-group"> <!---->
							<label><?php echo __('Portable '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('portable',array('class'=>'form-control','div'=>false,'placeholder'=>__('Portable'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group"> <!---->
							<label><?php echo __('Email '); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('email',array('class'=>'form-control','div'=>false,'placeholder'=>__('Email'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Type'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('type',$typesprofs,array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div> 
						<div class="form-group"> <!---->
							<label><?php echo __('Adresse'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('adresse',array('class'=>'form-control','div'=>false,'placeholder'=>__('Portable'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						
									
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
<?php echo $this->Form->end(); ?>
