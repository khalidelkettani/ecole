<?php
App::uses('AppController', 'Controller');

/**
 * Classses Controller
 */
class ClasssesController extends AppController {

//var $uses=array('Inscription');
/**
 * Scaffold
 *
 * @var mixed
 */
	//public $scaffold;
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des classes'));
		//$this->Classs->recursive = 0;
		$data = $this->Paginator->paginate('');  //array('Classs.user_id' => $this->Auth->user('id'))
    	$this->set('classses', $data);
    	$levels=$this->Classs->Level->find('list',array('fields'=>array('id','libelle')));
        $this->set(compact('levels'));
	}
public function inscription($id=null) {
	  
	    $this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des classes'));	
		
		$data = $this->Paginator->paginate('');  //,array('Classs.user_id' => $this->Auth->user('id'))
    	$this->set('classses', $data);
    	$this->loadModel('Anneesscollaire');
    	$levels=$this->Classs->Level->find('list',array('fields'=>array('id','libelle')));
    	$annees=$this->Anneesscollaire->find('list',array(
    		                           'fields'=>array('id','libelle')
    		                          // 'empty'=>array('id','conditions' => array('Anneesscollaire.default_year'=>'true')),
    		                           )
    	);
    		
     	$options = $this->Anneesscollaire->find('all',array(
						'conditions' => array('Anneesscollaire.default_year'=>'true'),
                        'fields'=>array('id','libelle')
                        )
     );
     	//$ida=$options->Anneesscollaire->['id'];
 
 $ida=$options[0]['Anneesscollaire']['id'];
    	$resultat_de_ta_requete = $this->Classs->Inscription->find('count',array(
						'conditions' => array('Inscription.classes_id'=>$id)));

		$this->set(compact('resultat_de_ta_requete','annees','nombre','levels','options','ida'));
	    }
	public function search($q = null) {
		$this->layout=null;
		$this->Classs->recursive = 0;
		$this->set('Classs', $this->Paginator->paginate('Classs',array('Classs.libelle LIKE'=>'%'.$q.'%')));

	}
	public function getRowCout($id =0,$idAnnee =0)
{

	//idAnnee venant de  la fonction filebaanne en parametre compact
    $id = $this->params['id']; // remember!
    $idAnnee = $this->params['ido'];
    //debug($idAnnee);
	//debug($id);
    $total = $this->Classs->Inscription->find('all', array(
    'fields' => array('count(Inscription.id) as TOTAL'),
    'conditions' => array('Inscription.classes_id' => $id,'Inscription.annesscollaires_id' => $idAnnee)));
    $total_available = $total[0][0]['TOTAL'];
    return $total_available;
}
public function filterbyannee($ido=null){
// id de l annee selectionnee venant de script 

		$this->layout = null;
		$this->loadModel('Anneesscollaire');
    	$this->loadModel('Inscription');
    	$this->loadModel('Level');
		
		
		$this->Classs->recursive = 2;
		$data = $this->Paginator->paginate('');  //,array('Classs.user_id' => $this->Auth->user('id'))
    	$this->set('classses', $data);
    	$levels=$this->Level->find('list',array('fields'=>array('id','libelle')));
    	$annees=$this->Anneesscollaire->find('list',array(
    		                           'fields'=>array('id','libelle'))
    	);
     //$options = array('value' => 'Homme');
    /* $options = $this->Anneesscollaire->find('first',array(
						'conditions' => array('Anneesscollaire.default_year'=>'true'),
                        'fields'=>array('id','libelle'),
                        
                        )
     );*/
        
        /*$resultat_de_ta_requete = $this->Inscription->find('count',array(
						'conditions' => array('Inscription.classes_id'=>$id,
							                  'Inscription.annesscollaires_id'=>$ido)
						)
        );*/

		$this->set(compact('resultat_de_ta_requete','annees','nombre','levels','ido'));
	    
			/*$ByAnnee = array();
			debug($id);
			if($id>0){
				$done = "ok";
				$ByAnnee=array('Inscription.annesscollaires_id'=>$id);
				}
			
			
			
			$conditions = array_merge($ByAnnee);
			if( isset($done) && !empty($done))
				{		
					    $this->Classs->recursive = 2;
					    $this->paginate = array('limit' => 25,
											'order' => array('Iscription.created' => 'desc'), 
												'conditions' => $conditions);
					    $this->set('classses', $this->paginate(''));

				} 

			else{
				$this->Classs->recursive = 2;
				$this->paginate = array('limit' => 25,
											'order' => array('Iscription.created' => 'desc'), 
												'conditions' => array('Classs.user_id' => $this->Auth->user('id')));
				$this->set('classses', $this->paginate('Classs'));
				}*/
			
		}		
/*public function nombre(){
		//debug($userID);die();
	$this->loadModel('Inscription');
		$result = $this->Inscription->find('all', array(
			'conditions' => array('Inscription.user_id' => $this->Auth->user('id'))
			/*'Inscription.recurence' => 1,
			'Inscription.type' => 17,
		),
			'fields' => array('COUNT(Inscription.id) AS ma_Nbre'),
		));
		debug($this->Classs('id'));die();
		return  $result[0][0]['ma_Nbre'];

	}*/

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = __('layout_admin');
		if ($this->request->is('post')) {
			$this->request->data['Classs']['user_id'] = $this->Auth->user('id');
			$this->Classs->create();
			if ($this->Classs->save($this->request->data)) {
				$this->Session->setFlash(__('La classe a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La classe n\'a pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
		$levels=$this->Classs->Level->find('list',array('fields'=>array('id','libelle')));
        $this->set(compact('levels'));
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
     	$this->layout = __('layout_admin');
		$idu= $this->Classs->find('first', array(
        	'conditions' => array('Classs.id' => $id)));

		if (!$this->Classs->exists($id) ) {  //|| $idu['Classs']['user_id'] != $this->Auth->user('id')
			
			$this->Session->setFlash(__('Cette classe est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is(array('post', 'put'))) {

			if ($this->Classs->save($this->request->data)) {
				$this->Session->setFlash(__('La classe a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La classe n\'a pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Classs.' . $this->Classs->primaryKey => $id));
			$this->request->data = $this->Classs->find('first', $options);
		}
		$levels=$this->Level->find('list',array('fields'=>array('id','libelle')));
        $this->set(compact('levels'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit_annee_defaut($id = null) {
     	$this->layout = __('layout_admin');
     	$id = $this->params['id'];
     	
     	debug($id);
     	die();
		$idu= $this->Classs->find('first', array(
        	'conditions' => array('Classs.id' => $id)));

		if (!$this->Classs->exists($id) ) {  //|| $idu['Classs']['user_id'] != $this->Auth->user('id')
			
			$this->Session->setFlash(__('Cette classe est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is(array('post', 'put'))) {

			if ($this->Classs->save($this->request->data)) {
				$this->Session->setFlash(__('La classe a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La classe n\'a pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Classs.' . $this->Classs->primaryKey => $id));
			$this->request->data = $this->Classs->find('first', $options);
		}
		$levels=$this->Level->find('list',array('fields'=>array('id','libelle')));
        $this->set(compact('levels'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Classs->id = $id;
		$idu= $this->Classs->find('first', array(
        	'conditions' => array('Classs.id' => $id)));

		if (!$this->Classs->exists($id) || $idu['Classs']['user_id'] != $this->Auth->user('id')) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id'))
			$this->Session->setFlash(__('Cette classe est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->Classs->delete()) {
				$this->Session->setFlash(__('La classe a été supprimée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('La classe n\'a pas pu être supprimée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Classs->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}



