<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {


    public $name = 'User';
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Un nom d\'utilisateur est requis'
            )
        ),
        'avatar_logo'=> array(
                'rule' => array( 'fileExtension',array('jpg','png'),true),
                'message'=>'Vous ne pouver pas envoyer que des jpg ou des png',
                'required' => false,
                    ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Un mot de passe est requis'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Un Eamil  est requis'
            )
        ),
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'auteur')),
                'message' => 'Merci de rentrer un rôle valide',
                'allowEmpty' => false
            )
        )
    );

   public function isOwnedBy($post, $user) {
    return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
}

public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new BlowfishPasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return true;
}

public function fileExtension($check,$extensions,$allowEmpty=true){
        $file=current($check);
                 //debug($file['tmp_name']);die();
               if ($allowEmpty&&!empty($file['tmp_name'])){
                    return true;

 
                 }
                // debug('rr');die();
                    $extension=strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
                    return in_array($extension, $extensions);

                 }

public function afterSave($created, $options = array()){  //1

if(isset($this->data[$this->alias]['avatar_logo'])){
    $file=$this->data[$this->alias]['avatar_logo'];
    $extension=strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
    
       if(!empty($file['tmp_name'])){  //3
                $oldestensions=$this->field('logo');
                $oldfile=IMAGES.'users'.DS.$this->id.'.'.$oldestensions; 
        if(file_exists($oldfile)){
          unlink($oldfile);
        }  //2
         move_uploaded_file($file['tmp_name'], IMAGES.'users'.DS.$this->id.'.'.$extension);
         $this->saveField('logo', $extension);
} //1
}
}
public function beforeDelete($cascade=true){
        $oldestensions=$this->field('logo');
        $oldfile=IMAGES.'users'.DS.$this->id.'.'.$oldestensions; 
        if(file_exists($oldfile)){
          unlink($oldfile);
        }  //2

}

}



