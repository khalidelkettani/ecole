<section id="col-left" class="col-left-nano">
						<div id="col-left-inner" class="col-left-nano-content">
							<div id="user-left-box" class="clearfix hidden-sm hidden-xs dropdown profile2-dropdown">
								<!-- <img alt="" src="https://secure.gravatar.com/avatar/youssef@gmail.com) ?>?s=550&d=mm" /> -->
								<img src="<?php echo $this->webroot.'app/webroot/img/users/'.AuthComponent::user('id'). '.' .AuthComponent::user('logo'); ?>" alt=""/>
								<div class="user-box">
									<span class="name">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<?php echo AuthComponent::user('username'); ?> 
											<i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu">
											<li>
												<a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'profile')); ?>"><i class="fa fa-institution"></i><?php echo __('Informations entreprise');?>
												</a>
											</li>
											<!-- <li>
												<a href="<?php echo $this->Html->url(array('plugin' => 'users','controller' => 'users','action' => 'change_password')); ?>"><i class="fa fa-user"></i><?php echo __('Changer mot de passe');?>
												</a>
											</li> -->
											<li>
												<a href="<?php echo $this->Html->url(array('controller' => 'users','action' => 'logout')); ?>"><i class="fa fa-power-off"></i><?php echo __('Déconnexion');?></a>
											</li>
										</ul>
									</span>
									<span class="status">
										<i class="fa fa-circle"></i> <?php echo __('En ligne'); ?>
									</span>
								</div>
							</div>
							<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">	
								<ul class="nav nav-pills nav-stacked">
									<li class="<?php echo ((($this->params['controller']==='homes') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>">
											<i class="fa fa-dashboard"></i>
											<span><?php echo __('Tableaux de Bord');?></span>
										</a>
									</li>

									<li class="<?php echo ((($this->params['controller']==='users') && ($this->params['action']=='index')) || (($this->params['controller']==='anneesscollaires') && ($this->params['action']=='index')) || (($this->params['controller']==='parametrages') && ($this->params['action']=='parametrage')))?'active' :'' ?>">
										<a href="#" class="dropdown-toggle">
											<i class="fa fa-user"></i>
											<span><?php echo __('Admin');?></span>
											<i class="fa fa-angle-right drop-icon"></i>
										</a>
										<ul class="submenu">
											<li class="<?php echo ((($this->params['controller']==='anneesscollaires') && ($this->params['action']=='edit_annee_defaut')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'anneesscollaires','action' => 'edit_annee_defaut')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Configurations');?>
                                                </a>
                                            </li>
											<li class="<?php echo ((($this->params['controller']==='users') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'users','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Utilisateurs');?>
                                                </a>
                                            </li>
											<li class="<?php echo ((($this->params['controller']==='anneesscollaires') && ($this->params['action']=='index')))?'active' :'' ?>">
												<a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'anneesscollaires','action' => 'index')); ?>">	  <i class="fa fa-paw"></i> 
													<?php echo __('Années scollaires');?>
                                                </a>
											</li>
											
										</ul>
									</li> 
									<!-- parametrages-->
									<li class="<?php echo ((($this->params['controller']==='horaires') && ($this->params['action']=='index')) ||(($this->params['controller']==='villes') && ($this->params['action']=='index')) || (($this->params['controller']==='regions') && ($this->params['action']=='index')) || (($this->params['controller']==='filaires') && ($this->params['action']=='index')) || (($this->params['controller']==='matiers') && ($this->params['action']=='index'))|| (($this->params['controller']==='competencesmatiers') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="#" class="dropdown-toggle">
											<i class="fa fa-cogs"></i>
											<span><?php echo __('Paramétrages');?></span>
											<i class="fa fa-angle-right drop-icon"></i>
										</a>
										<ul class="submenu">
											<li class="<?php echo ((($this->params['controller']==='villes') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'villes','action' => 'index')); ?>">
                                                	<i class="fa fa-paper-plane"></i> 
                                                	<?php echo __('Liste des villes');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='horaires') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'horaires','action' => 'index')); ?>">
                                                	<i class="fa fa-paper-plane"></i> 
                                                	<?php echo __('Liste des Horaires');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='regions') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'regions','action' => 'index')); ?>">
                                                 <i class="fa fa-flag"></i> 
                                                 <?php echo __('Liste des régions');?>
                                                </a>
                                            </li>
											<li class="<?php echo ((($this->params['controller']==='filaires') && ($this->params['action']=='index')))?'active' :'' ?>">
												<a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'filaires','action' => 'index')); ?>">	  <i class="fa fa-paw"></i> 
													<?php echo __('Liste des filières');?>
                                                </a>
											</li>

											<li class="<?php echo ((($this->params['controller']==='matiers') && ($this->params['action']=='index')))?'active' :'' ?>">
												<a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'matiers','action' => 'index')); ?>">	  <i class="fa fa-paper-plane"></i> 
													<?php echo __('Liste des matiers');?>
                                                </a>
											</li>
											<li class="<?php echo ((($this->params['controller']==='competencesmatiers') && ($this->params['action']=='index')))?'active' :'' ?>">
												<a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'competencesmatiers','action' => 'index')); ?>">	  <i class="fa fa-paper-plane"></i> 
													<?php echo __('Liste des competences');?>
                                                </a>
											</li>
											
                                            <li class="<?php echo ((($this->params['controller']==='nationalites') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'nationalites','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Liste des nationalités');?>
                                                </a>
                                            </li>
                                            
                                            
                                            <li class="<?php echo ((($this->params['controller']==='salles') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'salles','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Liste des salles');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='classses') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'classses','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Liste des classes');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='levels') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'levels','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Liste des niveaux');?>
                                                </a>
                                            </li>
										</ul>
									</li>
									<!-- parametrages-->
									<li class="<?php echo ((($this->params['controller']==='eleves') && ($this->params['action']=='index')) || (($this->params['controller']==='types_profs') && ($this->params['action']=='index')) || (($this->params['controller']==='groupes') && ($this->params['action']=='index')) ||(($this->params['controller']==='profs') && ($this->params['action']=='index'))||(($this->params['controller']==='matiersprofs') && ($this->params['action']=='index')) )?'active' :'' ?>">
										<a href="#" class="dropdown-toggle">
											<i class="fa fa-user"></i>
											<span><?php echo __('Traitements');?></span>
											<i class="fa fa-angle-right drop-icon"></i>
										</a>
										<ul class="submenu">
											<li class="<?php echo ((($this->params['controller']==='profs') && ($this->params['action']=='index')) || (($this->params['controller']==='types_profs') && ($this->params['action']=='index')) |(($this->params['controller']==='matiersprofs') && ($this->params['action']=='index')))?'active' :'' ?>">
												<a href="#" class="dropdown-toggle">
													<i class="fa fa-user"></i>
													<span><?php echo __('Professeurs');?></span>
													<i class="fa fa-angle-right drop-icon"></i>
												</a>
										<ul class="submenu">
												<li class="<?php echo ((($this->params['controller']==='types_profs') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'types_profs','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Types');?>
                                                </a>
                                            </li>


                                            <li class="<?php echo ((($this->params['controller']==='profs') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'profs','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Liste Professeurs');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='matiersprofs') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'matiersprofs','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Informations');?>
                                                </a>
                                            </li>
										</ul>	

                                            
                                           </li>
                                            
										</ul>
										<ul class="submenu">
											<li class="<?php echo ((($this->params['controller']==='eleves') && ($this->params['action']=='index')) || (($this->params['controller']==='groupes') && ($this->params['action']=='index')) )?'active' :'' ?>">
												<a href="#" class="dropdown-toggle">
													<i class="fa fa-user"></i>
													<span><?php echo __('Elèves');?></span>
													<i class="fa fa-angle-right drop-icon"></i>
												</a>
												<ul class="submenu">
												<li class="<?php echo ((($this->params['controller']==='groupes') && ($this->params['action']=='index')))?'active' :'' ?>">
												<a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'groupes','action' => 'index')); ?>">	  <i class="fa fa-paw"></i> 
													<?php echo __('Liste des groupes');?>
                                                </a>
											</li>


                                            <li class="<?php echo ((($this->params['controller']==='eleves') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'eleves','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Liste des élèves');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='eleves') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'classses','action' => 'inscription')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Inscriptions');?>
                                                </a>
                                            </li>
                                             <li class="<?php echo ((($this->params['controller']==='eleves') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'eleves','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Emploi du temps');?>
                                                </a>
                                            </li>
                                            <li class="<?php echo ((($this->params['controller']==='eleves') && ($this->params['action']=='index')))?'active' :'' ?>">
                                                <a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'eleves','action' => 'index')); ?>">
                                                 <i class="fa fa-flag-checkered"></i> 
                                                 <?php echo __('Casiers');?>
                                                </a>
                                            </li>
                                            </ul>
                                            </li> <!-- eleves-->
										</ul>	
									</li>  <!-- Traitement-->
									
									<!-- <li>
										<center>
											<img src="http://www.rhquebec.com/site/wp-content/uploads/2015/09/RHQ_logo_108.png">
										</center>
										
									</li> -->
									
									
									
								</ul>
							</div>
						</div>
					</section>
					<div id="nav-col-submenu"></div>