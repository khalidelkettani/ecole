<div class="email-nav-nano-content">
							
							<?php if($idu['User']['logo'] == null): ?>
                                    <img src="https://secure.gravatar.com/avatar/<?php echo md5($idu['User']['email']) ?>?s=550&d=mm" alt="" title="" class="profile-img img-responsive center-block" style="max-height: 140px; min-height: 140px; max-width: 140px;    min-width: 140px; border-radius: 10px; margin-bottom: 5px;" />
                                <?php else: ?>
                                    <img src="<?php echo $this->webroot.'app/webroot/'.$idu['User']['logo']; ?>" class="profile-img img-responsive center-block" style="max-height: 140px; min-height: 140px; max-width: 140px;    min-width: 140px; border-radius: 10px; margin-bottom: 5px;" />
                                <?php endif; ?>

								<div class="profile-label" style="text-align:center">
									<span class="label label-danger">
										<?php echo $idu['User']['firstname'].' '.$idu['User']['lastname']; ?>
									</span>
								</div>	<br>			
							<ul id="email-nav-items" class="clearfix">
								<li class="<?php echo ((($this->params['plugin']==='users') && ($this->params['controller']==='users') && ($this->params['action']=='editemp')))?'active' :'' ?>">
									<a href="<?php echo $this->Html->url(array('plugin' => 'users','language'=>$this->params['language'],'controller' => 'users','action' => 'editemp',$id)); ?>">
										<i class="fa fa-user"></i>
										<?php echo  __('Informations Employé'); ?> 
									</a>
								</li>
								<li class="<?php echo ((($this->params['plugin']==='users') && ($this->params['controller']==='users') && ($this->params['action']=='editcoord')))?'active' :'' ?>">
									<a href="<?php echo $this->Html->url(array('plugin' => 'users','language'=>$this->params['language'],'controller' => 'users','action' => 'editcoord',$id)); ?>">
										<i class="fa fa-map-marker"></i>
										<?php echo  __('Coordonnées'); ?> 
									</a>
								</li>
								<li class="<?php echo ((($this->params['controller']==='urgencecontacts') && ($this->params['action']=='index')))?'active' :'' ?>">
									<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'urgencecontacts','action' => 'index',$id)); ?>">
										<i class="fa fa-envelope"></i>
										<?php echo  __('Contact d\'urgence'); ?> 
									</a>
								</li>
								<li class="<?php echo ((($this->params['controller']==='personnesacharges') && ($this->params['action']=='index')))?'active' :'' ?>">
									<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'personnesacharges','action' => 'index',$id)); ?>">
										<i class="fa fa-users"></i>
										<?php echo  __('Personnes à charge'); ?> 
									</a>
								</li>
								<li class="<?php echo ((($this->params['controller']==='immigrationinformations') && ($this->params['action']=='index')))?'active' :'' ?>">
									<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'immigrationinformations','action' => 'index',$id)); ?>">
										<i class="fa fa-flag"></i>
										<?php echo  __('Immigration'); ?>  
									</a>
								</li>
							
												
								
								
									<li class="<?php echo ((($this->params['plugin']==='users') && ($this->params['controller']==='users') && ($this->params['action']=='editjob')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => 'users','language'=>$this->params['language'],'controller' => 'users','action' => 'editjob',$id)); ?>">
											<i class="fa fa-tasks"></i>
											<?php echo  __('Emploi'); ?> 
										</a>
									</li>
									<li class="<?php echo ((($this->params['plugin']===null) && ($this->params['controller']==='empsalaires') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'empsalaires','action' => 'index',$id)); ?>">
											<i class="fa fa-dollar"></i>
											<?php echo  __('Salaire'); ?> 
										</a>
									</li>
									<li class="<?php echo ((($this->params['plugin']===null) && ($this->params['controller']==='reporters') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'reporters','action' => 'index',$id)); ?>">
											<i class="fa fa-paw"></i>
											<?php echo  __('Reporter à'); ?> 
										</a>
									</li>
									<li class="<?php echo ((($this->params['controller']==='empexperiences') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'empexperiences','action' => 'index',$id)); ?>">
											<i class="fa fa-university"></i>
											<?php echo  __('Diplômes'); ?> 
										</a>
									</li>
									<li class="<?php echo ((($this->params['controller']==='empadhesions') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'empadhesions','action' => 'index',$id)); ?>">
												<i class="fa fa-briefcase"></i>
												 <?php echo  __('Adhésions'); ?>
												</a>
									</li>
									<li class="<?php echo ((($this->params['controller']==='empdepots') && ($this->params['action']=='index')))?'active' :'' ?>">
										<a href="<?php echo $this->Html->url(array('plugin' => null,'language'=>$this->params['language'],'controller' => 'empdepots','action' => 'index',$id)); ?>">
												<i class="fa  fa-check-square"></i>
												 <?php echo  __('Dépôt direct'); ?>
											</a>
									</li>
									</ul>
						</div>
											
					</div>