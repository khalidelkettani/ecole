<?php
App::uses('AppController', 'Controller');
/**
 * Pays Controller
 *
 * @property Pay $Pay
 * @property PaginatorComponent $Paginator
 */
class PaysController extends AppController {
	
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		$this->set('title_for_layout', __('Liste des régions'));
		//$this->Paginator->settings = $this->paginate;
/**
 * index method
 *
 * pays variable
 *Pay table 
 */
		$this->Pay->recursive = 0;
		$data = $this->Paginator->paginate('Pay',array('Pay.user_id' => $this->Auth->user('id')));
    	$this->set('pays', $data);
    	//$this->set('pays', arry(contain' => array('Pays')));
    
	}

	public function search($q = null) {
		$this->layout=null;
		$this->Pay->recursive = 0;
		$this->set('pays', $this->Paginator->paginate('Pay',array('Pay.libelle LIKE'=>'%'.$q.'%')));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		if (!$this->Pay->exists($id)) {
			throw new NotFoundException(__('Invalid pay'));
		}
		$options = array('conditions' => array('Pay.' . $this->Pay->primaryKey => $id));
		$this->set('pay', $this->Pay->find('first', $options));
	}*/

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = __('layout_admin');
		if ($this->request->is('post')) {
			$this->request->data['Pay']['user_id'] = $this->Auth->user('id');
			$this->Pay->create();
			if ($this->Pay->save($this->request->data)) {
				$this->Session->setFlash(__('Le pays a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le pays n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	$this->layout=null;
		$idu= $this->Pay->find('first', array(
        	'conditions' => array('Pay.id' => $id)));

		if (!$this->Pay->exists($id)|| $idu['Pay']['user_id'] != $this->Auth->user('id') ) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id')
			$this->Session->setFlash(__('Ce pays est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Pay->save($this->request->data)) {
				$this->Session->setFlash(__('Le pays a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Le pays n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Pay.' . $this->Pay->primaryKey => $id));
			$this->request->data = $this->Pay->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Pay->id = $id;
		$idu= $this->Pay->find('first', array(
        	'conditions' => array('Pay.id' => $id)));

		if (!$this->Pay->exists($id) || $idu['Pay']['user_id'] != $this->Auth->user('id')) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id'))
			$this->Session->setFlash(__('Ce pays est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->Pay->delete()) {
				$this->Session->setFlash(__('Le pays a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('Le pays n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Pay->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }

    return parent::isAuthorized($user);
}
}
