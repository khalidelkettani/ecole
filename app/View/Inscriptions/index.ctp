<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>"><?php echo __('Accueil');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Liste des inscriptions');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des inscriptions');?><small></small></h1>
		<!-- <h1><?php echo $id;?></h1>
		<h1><?php echo $ido;?></h1> -->
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				 
				<div class="main-box clearfix project-box green-box">
																					
					<div class="col-lg-4">
						<div class="form-group ">
							<label><?php echo __('Nom de la classe'); ?> </label>
													
									<?php echo $this->Form->input('libelle',array('class'=>'form-control',
							    'div'=>false,'label'=>false,'disabled' => 'disabled','value' => $classes['Classs']['libelle'], 'data-parsley-group'=>'block1'));?>
						</div>
												
					<div class="form-group ">
						<label><?php echo __('Capacité de la classe'); ?> </label>
							<?php echo $this->Form->input('capacite',array('class'=>'form-control',
							    'div'=>false,'label'=>false,'disabled' => 'disabled','value' => $classes['Classs']['capacite'], 'data-parsley-group'=>'block1'));?>
					</div>
												
					</div>
					<div class="col-lg-4">
				        <div class="form-group ">
							<label><?php echo __('Année scollaire'); ?> </label>
								<?php echo $this->Form->input('capacite',array('class'=>'form-control',
							    'div'=>false,'label'=>false,'disabled' => 'disabled','value' => $annees['Anneesscollaire']['libelle'], 'data-parsley-group'=>'block1'));?>
						</div>
					</div>
						<div class="col-lg-4 col-md-6 col-sm-6">
							<div class="main-box clearfix project-box green-box">
								<div class="main-box-body clearfix">
									<div class="project-box-header green-bg">
										<div class="name">
											
												<!--<?php echo __('Remplissage'); ?>-->
										
										</div>
									</div>

									<?php $quantity = $this->requestAction(array(
									'controller' => 'classses',
									'action' => 'getRowCout'),
									array('id' => $idC,'ido' => $idA));

                                 $Half=$classes['Classs']['capacite']/'2';
                                 $pourcentage=$quantity*100/$classes['Classs']['capacite'];
                                 $pourcentage=$this->Number->precision($pourcentage, 2 );
                                 $prs=$pourcentage;?>


									<div class="project-box-content">
										<span class="chart" data-percent=<?php echo $prs; ?>  data-bar-color="#2ecc71">
											<span class="percent"></span>%<br/>
											<span class="lbl">completed</span>
										</span>
									</div>

								</div>
							</div>
								</div>
								</div><!--gg-->
			</header>

		            <div class="filter-block pull-right">
						
						<!--<input class="form-control" id="exampleInputFile" type="text" placeholder="Disabled input here..." disabled>-->
						<div class="col-lg-12">
								<div class="form-group pull-left">
									<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
									<i class="fa fa-search search-icon"></i>
								</div>	
						
							<a href="<?php echo $this->Html->url(array('controller' => 'inscriptions','action' => 'add',$idC,$idA,$prs)); ?>" class="btn btn-primary pull-right">
								<i class="fa fa-plus-circle fa-lg"></i> 
								<?php echo __('Nouvelle inscription');?>
							</a>
						<!-- <a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouvelle inscription');?>
							
						</a> -->
						</div>	
					</div>	
		<div class="col-lg-12">											
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Photo de l\'élève');?></th>
							<th><?php echo __('Nom de l\'élève');?></th>
							<th><?php echo __('Date d\'inscription');?></th>
							<th><?php echo __('Remarque');?></th>
													
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($inscriptions as $var): ?>
					<tbody>
						<tr>
							<td>
								
							<?php if($var['Elef']['logo'] == null): ?>
                                    <img src="https://secure.gravatar.com/avatar/<?php echo md5($var['Elef']['logo']) ?>?s=10&d=mm" alt="" title=""/>
                                <?php else: ?>
                                    <img src="<?php echo $this->webroot.'app/webroot/img/eleves/'.$var['Elef']['id']. '.' .$var['Elef']['logo']; ?>" alt=""/>
                                <?php endif; ?>
                            </td>
							<td>
								<?php echo h($var['Elef']['nom']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Inscription']['date']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Inscription']['remarques']); ?>&nbsp;
							</td>
							

							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'inscriptions','action'=>'edit', $var['Inscription']['id'],$idC,$idA), array('escape' => false));?>
								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('controller'=>'inscriptions','action' => 'delete', $var['Inscription']['id'],$idC,$idA), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Elef']['nom'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
					<?php echo $this->element('paginate'); ?>		
			</div>
		</div>
	</div>
	</div>
</div>
<div class="modal-footer">
					<div class="form-actions">
								<a class="btn btn-danger" href="<?php echo $this->Html->url(array('controller' => 'classses','action' => 'inscription',$idA)); ?>"><?php echo __('ANNULER'); ?></a>
							</div>
				
</div>
				<?php echo $this->Form->end(); ?>
</div>
	

<!--  Model form add -->
<!-- 
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Ajouter une classe'); ?></h4>
				</div>
				<div class="modal-body">

				-parsley-validate'=>'data-parsley-validate','action'=>'add','data-validate'=>'parsley')); ?>
				<?php echo $this->Form->create('Classs',array('url'=> array('controller' => 'classses','action' => 'add' ) )); ?>
						<div class="form-group">
							<label><?php echo __('Nom de la classe'); ?> *</label>
							<?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de la classe'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<div class="form-group">
								<label><?php echo __('Niveau de la classe'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
				                 <?php echo $this->Form->select('level_id',$levels,array('class'=>'form-control','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
									</div>
						<div class="form-group">
							<label><?php echo __('Capacité de la classe'); ?> *</label>
							<?php echo $this->Form->input('capacite',array('class'=>'form-control','div'=>false,'placeholder'=>__('Capacité de la classe'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
												

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>-->

<!--  Model form edit -->

<!-- <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-
hidden="true">

</div> -->


	<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'classses','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>

	<script>
	$(document).ready(function() {
		
	    $('.conversation-inner').slimScroll({
	        height: '352px',
	        alwaysVisible: false,
	        railVisible: true,
	        wheelStep: 5,
	        allowPageScroll: false
	    });
		
		$('.chart').easyPieChart({
			easing: 'easeOutBounce',
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			},
			barColor: '#3498db',
			trackColor: '#f2f2f2',
			scaleColor: false,
			lineWidth: 8,
			size: 130,
			animate: 1500
		});
	    
		//WORLD MAP
		$('#world-map').vectorMap({
			map: 'world_merc_en',
			backgroundColor: '#ffffff',
			zoomOnScroll: false,
			regionStyle: {
				initial: {
					fill: '#e1e1e1',
					stroke: 'none',
					"stroke-width": 0,
					"stroke-opacity": 1
				},
				hover: {
					"fill-opacity": 0.8
				},
				selected: {
					fill: '#8dc859'
				},
				selectedHover: {
				}
			},
			markerStyle: {
				initial: {
					fill: '#e84e40',
					stroke: '#e84e40'
				}
			},
			markers: [
				{latLng: [38.35, -121.92], name: 'Los Angeles - 23'},
				{latLng: [39.36, -73.12], name: 'New York - 84'},
				{latLng: [50.49, -0.23], name: 'London - 43'},
				{latLng: [36.29, 138.54], name: 'Tokyo - 33'},
				{latLng: [37.02, 114.13], name: 'Beijing - 91'},
				{latLng: [-32.59, 150.21], name: 'Sydney - 22'},
			],
			series: {
				regions: [{
					values: gdpData,
					scale: ['#6fc4fe', '#2980b9'],
					normalizeFunction: 'polynomial'
				}]
			},
			onRegionLabelShow: function(e, el, code){
				el.html(el.html()+' ('+gdpData[code]+')');
			}
		});

		/* ANIMATED WEATHER */
		var skycons = new Skycons({"color": "#03a9f4"});
		// on Android, a nasty hack is needed: {"resizeClear": true}

		// you can add a canvas by it's ID...
		skycons.add("current-weather", Skycons.SNOW);

		// start animation!
		skycons.play();
	});
	</script>