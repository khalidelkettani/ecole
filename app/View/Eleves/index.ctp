<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>"><?php echo __('Accueils');?></a></li>
			<li><span><?php echo __('Traitements');?></span></li>
			<li><span><?php echo __('Elèves');?></span></li>
			<li class="active"><span><?php echo __('Liste des élèves');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des élèves');?><small></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left"><?php echo __('Elèves');?></h2>
					<div class="filter-block pull-right">
						<div class="form-group pull-left">
							<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div>			
						<a href="<?php echo $this->Html->url(array('controller' => 'eleves','action' => 'add')); ?>" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouveau éléve');?>
						</a>
					</div>
			</header>
										
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Photo');?></th>
							<th><?php echo __('Nom');?></th>
							<th><?php echo __('Prénom');?></th>
							<th><?php echo __('Sexe');?></th>
							<th><?php echo __('Date de naissance');?></th>
							<th><?php echo __('Lieu naissance');?></th>
							<th><?php echo __('Date d\'entrée');?></th>
							<th><?php echo __('email');?></th>
							<th><?php echo __('Ville');?></th>
							<th><?php echo __('Adresse');?></th>
							<!-- <th><?php echo __('Nom de pére');?></th>
							<th><?php echo __('Téléphone');?></th>
							<th><?php echo __('Nom de mére');?></th>
							<th><?php echo __('Téléphone');?></th> -->

							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($eleves as $var): ?>
					<tbody>
						<tr>
							<td>
								
							<?php if($var['Elef']['logo'] == null): ?>
                                    <img src="https://secure.gravatar.com/avatar/<?php echo md5($var['Elef']['logo']) ?>?s=10&d=mm" alt="" title=""/>
                                <?php else: ?>
                                    <img src="<?php echo $this->webroot.'app/webroot/img/eleves/'.$var['Elef']['id']. '.' .$var['Elef']['logo']; ?>" alt=""/>
                                <?php endif; ?>
                            </td>
							<td>
								
								<?php echo h($var['Elef']['nom']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Elef']['prenom']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Elef']['sexe']); ?>&nbsp;
							</td>
							<td>
								<?php echo date(" d/m/Y ", strtotime(h($var['Elef']['date_naissance'])));?>
								<!-- <?php echo h($var['Elef']['date_naissance']); ?>&nbsp; -->
							</td>
							<td>
								<?php echo h($var['Ville']['libelle']); ?>&nbsp;
							</td>
							<!--<td>
								<?php echo h($var['Pay']['libelle']); ?>&nbsp;
							</td>-->
							<td>
								<?php echo date(" d/m/Y ", strtotime(h($var['Elef']['date_entree'])));?>
								<!-- <?php echo h($var['Elef']['date_entree']); ?>&nbsp; -->
							</td>
							<td>
								<?php echo h($var['Elef']['email']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Ville']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Elef']['adresse']); ?>&nbsp;
							</td>
							<!-- <td>
								<?php echo h($var['Elef']['nom_pere']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Elef']['tel_pere']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Elef']['nom_mere']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Elef']['tel_mere']); ?>&nbsp;
							</td> -->
														
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'eleves','action'=>'edit', $var['Elef']['id']), array('escape' => false, 'class' => 'table-link', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Elef']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Elef']['nom'])); ?>

								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-search-plus fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'eleves','action'=>'view', $var['Elef']['id']), array('escape' => false, 'class' => 'table-link', 'target' => '_self'));?>

							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo $this->element('paginate'); ?>
			</div>
		</div>
	</div>
</div>
</div>
	

<!--  Model form add éléve-->
<!--<?php echo $this->Form->create('Elef',array('role'=>'form','data-parsley-validate'=>'data-parsley-validate','action'=>'add','data-validate'=>'parsley')); ?>-->

	<!--  Model form edit ville-->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
</div>
<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
	<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'eleves','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>