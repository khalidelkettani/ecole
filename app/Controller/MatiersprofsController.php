<?php
App::uses('AppController', 'Controller');

class MatiersprofsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
	    $this->set('title_for_layout', __('Liste des Professeurs'));
		$this->Matiersprof->recursive = 0;
		$data = $this->Paginator->paginate('Matiersprof');
    	$this->set('matiersprofs', $data);
		
	}
}
