<?php
App::uses('AppController', 'Controller');
/**
 * Pays Controller
 *
 * @property Pay $Pay
 * @property PaginatorComponent $Paginator
 */
class AnneesscollairesController extends AppController {
	
	 
    
    /*public function initialize()
        {
            parent::initialize();
            $this->loadComponent('Flash');
            $this->loadComponent('RequestHandler');
 
        }*/
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','Session');
//public $helpers = array('Form', 'Html', 'Js', 'Time');
public $helpers = array('Html', 'Form', 'Js'=>array("Jquery"),"Session");


public function initialize()
        {
            parent::initialize();
            $this->loadComponent('Flash');
            $this->loadComponent('RequestHandler');
 
        }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = __('layout_admin_cube');
		//$this->loadModel('Anneesscollaire');
		//$this->layout = __('layout_admin_cube_employe');
		//$this->set('title_for_layout', __('Liste des Classes'));
		//$this->Paginator->settings = $this->paginate;
		$this->Anneesscollaire->recursive = 0;
		
		$data = $this->Paginator->paginate('');   //,array('Anneesscollaire.user_id' => $this->Auth->user('id')));
    	$this->set('anneesscollaires', $data);
    	//$this->set('pays', arry(contain' => array('Pays')));
    
	}

	public function search($q = null) {
		$this->layout=null;
		$this->Anneesscollaire->recursive = 0;
		$this->set('anneesscollaires', $this->Paginator->paginate('Anneesscollaire',array('Anneesscollaire.libelle LIKE'=>'%'.$q.'%')));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function view($id = null) {
		if (!$this->Pay->exists($id)) {
			throw new NotFoundException(__('Invalid pay'));
		}
		$options = array('conditions' => array('Pay.' . $this->Pay->primaryKey => $id));
		$this->set('pay', $this->Pay->find('first', $options));
	}*/

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = __('layout_admin_cube');
		if ($this->request->is('post')) {
			$this->request->data['Anneesscollaire']['user_id'] = $this->Auth->user('id');
			$this->Anneesscollaire->create();
			if ($this->Anneesscollaire->save($this->request->data)) {
				$this->Session->setFlash(__('L\'année a été enregistré avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('L\'année pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		}
	}

	public function applyTask() {
	    if ($this->request->is('ajax') && !empty($this->request->data)) {
	        $h_id = $this->request->data['id'];
	        $h_du = $this->request->data['du'];
			$h_au = $this->request->data['au'];
	        $h_periode = $this->request->data['periode'];

	        if (!$this->Horaire->findByTaskIdAndUserId($h_id, $h_du,$h_au, $h_periode)) {
	            throw new NotFoundException(__('Invalid tasks_users.'));
	        }

	        $this->Horaire->updateAll(array('is_apply' => TRUE), array('id' => $h_id, 'du' => $h_du , 'au' =>  $h_au, 'periode' =>  $h_periode));
	    } else {
	        throw new MethodNotAllowedException('This method is now allowed.');
	    }
    }

public function index_h() {
		$this->layout = __('layout_admin_cube');
		$this->loadModel('Horaire');
		
		$this->Horaire->recursive = 0;
		
		$data = $this->Paginator->paginate('');   //,array('Anneesscollaire.user_id' => $this->Auth->user('id')));
    	$this->set('horaires', $data);
    	//$this->set('pays', arry(contain' => array('Pays')));
    $horaires= $this->Horaire->find('all');
	$this->set(compact('horaires'));
	}
	public function crud_action($du,$au,$periode,$action,$id) {

		$this->layout = null;
		$this->loadModel('Horaire');
		// $this->layout = 'ajax';
        debug("bonjour");
      
if(!empty($action)) {
	debug($this->request($data));
	switch($action) {
		case "add":
		//echo($du+$au+$periode);
		//echo($periode);
		$h=$this->Horaire;
			$h->du= $du;
			$h->au= $au;
			$h->periode= $periode;
			
			$this->Horaire->create();
			if($this->Horaire->save($h)){
  				 		var_dump("sucssi");
  				 	}
		  	/*$sql = "INSERT INTO horaires (du, au, periode) VALUES ($du,$au,$periode)";
		    return $this->query($sql);
*/
 			break;
		case "delete":
        debug("vous etes ds laction");
        debug($id);
	  //$this->Horaire->query('delete * from horaires where id=8'); 
$this->Horaire->id = $id;
		$idu= $this->Horaire->find('first', array(
        	'conditions' => array('Horaire.id' => $id)));

		$this->request->onlyAllow('post', 'delete');
		if ($this->Horaire->delete()) {
	    //$this->Horaire->id = $id;
		 }
			break;
}

    
	}
	$horaires= $this->Horaire->find('all');
	$this->set(compact('horaires'));
}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		
		$this->layout = __('layout_admin_cube'); 
		$idu= $this->Anneesscollaire->find('first', array(
        	'conditions' => array('Anneesscollaire.id' => $id)));

		if (!$this->Anneesscollaire->exists($id)|| $idu['Anneesscollaire']['user_id'] != $this->Auth->user('id') ) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id')
			$this->Session->setFlash(__('Cette année est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Anneesscollaire->save($this->request->data)) {
				$this->Session->setFlash(__('L\'année a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('L\'année n\'a pas pu être enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Anneesscollaire.' . $this->Anneesscollaire->primaryKey => $id));
			$this->request->data = $this->Anneesscollaire->find('first', $options);
		}
	}


	 /*public function edit_annee_defaut() {

		$this->layout = __('layout_admin_cube'); 
		
		$this->loadModel('Anneesscollaire');
		
		$options = $this->Anneesscollaire->query('select id,libelle from anneesscollaires where default_year = true'); 
		$ida=$options[0]['anneesscollaires']['id'];

		$this->loadModel('Moi');

		$optionsMois = $this->Moi->query('select id,mois from mois ');   //where cheked = true
		$idM=$optionsMois[0]['Moi']['id'];
		if ($this->request->is(array('post', 'put'))) {
			// on remis les default_year a false 
			$this->Anneesscollaire->id = $ida;
			$this->Anneesscollaire->saveField('default_year', false);
  			// id de l'annee scolaire
  			$this->Anneesscollaire->id = $this->request->data['Anneesscollaire']['id'];
  			// sauvegarder l'annee sclectionnee
  			if ($this->Anneesscollaire->saveField('default_year', true)){
  					// on remis tous les checked a False pour les mois
		  			$this->Moi->id = $idM;
		  			$this->Moi->saveField('cheked', false);
		  			//id du mois coché
		  			$this->Moi->id = $this->request->data['Anneesscollaire']['idm'];

		  			if ($this->Moi->saveField('cheked', true)){
		  				$this->Session->setFlash(__('Les \'informations ont été enregistrées avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		  				return $this->redirect(array('action' => 'index'));
		  			}
		  			else {
  					$this->Session->setFlash(__('Les \'informations n\'ont été enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
  					return $this->redirect(array('action' => 'edit_annee_defaut'));
  					}
		  			

  				}
  				else {
  					$this->Session->setFlash(__('Les \'informations n\'ont été enregistré. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
  					return $this->redirect(array('action' => 'edit_annee_defaut'));
  				}

	    }
	
	    $annees=$this->Anneesscollaire->find('list',array(
    		                           'fields'=>array('id','libelle')));
		$mois=$this->Moi->find('list', array(
				'fields'=>array('id','janvier')));

		$this->set(compact('annees','ida','mois','idM'));
}*/

public function edit_annee_defaut() {



	$this->layout = __('layout_admin_cube'); 
	$this->loadModel('Jour');
    $this->loadModel('Moi');
    $this->loadModel('Horaire');
    $options = $this->Anneesscollaire->query('select id,libelle from anneesscollaires where default_year = true'); 
	$ida=$options[0]['anneesscollaires']['id'];

	$mois= $this->Moi->find('all');
	$jours= $this->Jour->find('all');
	$horairesM= $this->Horaire->find('all', array(
				'fields'=>array('id','du','au','periode'),
				'conditions' => array('periode' => 'Matinée')
			));
	$horairesAP= $this->Horaire->find('all', array(
				'fields'=>array('id','du','au','periode'),
				'conditions' => array('periode' => 'Après-midi')
			));
	
	$horairesS= $this->Horaire->find('all', array(
				'fields'=>array('id','du','au','periode'),
				'conditions' => array('periode' => 'Soirie')
			));
	
	

	if ($this->request->is(array('post', 'put'))) {
			// on remis les default_year a false 
		 $this->Anneesscollaire->query('update  anneesscollaires set  default_year = false'); 
			//$this->Anneesscollaire->saveField('default_year', false);
			//$this->Anneesscollaire->id = $id;
  			// id de l'annee scolaire
  			$this->Anneesscollaire->id = $this->request->data['Anneesscollaire']['id'];


  			// sauvegarder l'annee sclectionnee
  			if ($this->Anneesscollaire->saveField('default_year', true)){
  					// on remis tous les checked a False pour les mois
  				 foreach ($this->data['Anneesscollaire']['Mois'] as $key => $m_id) {
  				 	$moi=$this->Moi->findById($key);
  				 	if(count($m_id)==1){
  				 		$moi['Moi']['checked']=$m_id[0];	
  				 	}else{
  				 		$moi['Moi']['checked']=$m_id[1];
  				 	}
  				 
  				 	if($this->Moi->save($moi)){
  				 		
  				 	}
  				 
  				 }
  				  foreach ($this->data['Anneesscollaire']['Jours'] as $key => $m_id) {
  				 	$moi=$this->Jour->findById($key);
  				 	if(count($m_id)==1){
  				 		$moi['Jour']['checked']=$m_id[0];	
  				 	}else{
  				 		$moi['Jour']['checked']=$m_id[1];
  				 	}
  				 
  				 	if($this->Jour->save($moi)){
  				 		
  				 	}
  				 
  				 }
		  							
		return $this->redirect(array('action' => 'edit_annee_defaut'));
	}
}
	$horaires= $this->Horaire->find('all');
	$data = $this->Paginator->paginate('Horaire');  //array('Classs.user_id' => $this->Auth->user('id'))
	$this->set('horaires', $data);
	$comments=$this->Horaire->find('list',array(
    		                           'fields'=>array('id','du','au')));

	$annees=$this->Anneesscollaire->find('list',array(
    		                           'fields'=>array('id','libelle')));
	$this->set(compact('comments','annees','idm','ida','mois','jours','horairesM','horaires','horairesAP','horairesS'));
}

	
	public function edit_H($id = null) {
     	$this->layout = __('layout_admin');
		/*$idu= $this->Classs->find('first', array(
        	'conditions' => array('Classs.id' => $id)));

		if (!$this->Classs->exists($id) ) {  //|| $idu['Classs']['user_id'] != $this->Auth->user('id')
			
			$this->Session->setFlash(__('Cette classe est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		if ($this->request->is(array('post', 'put'))) {

			if ($this->Classs->save($this->request->data)) {
				$this->Session->setFlash(__('La classe a été enregistrée avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La classe n\'a pas pu être enregistrée. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('Classs.' . $this->Classs->primaryKey => $id));
			$this->request->data = $this->Classs->find('first', $options);
		}*/
		$levels=$this->Level->find('list',array('fields'=>array('id','libelle')));
        $this->set(compact('levels'));
	}
/** 
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Anneesscollaire->id = $id;
		$idu= $this->Anneesscollaire->find('first', array(
        	'conditions' => array('Anneesscollaire.id' => $id)));

		if (!$this->Anneesscollaire->exists($id) || $idu['Anneesscollaire']['user_id'] != $this->Auth->user('id')) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id'))
			$this->Session->setFlash(__('Cette année est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->Anneesscollaire->delete()) {
				$this->Session->setFlash(__('L\'année a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				$this->Session->setFlash(__('L\'année n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function deleteH($id = null) {
		$this->loadModel('Horaire');
		$this->Horaire->id = $id;
		$idu= $this->Horaire->find('first', array(
        	'conditions' => array('Horaire.id' => $id)));

		if (!$this->Horaire->exists($id) ) {  //|| $idu['Pay']['user_id'] != $this->Auth->user('id'))
			//$this->Session->setFlash(__('Cette année est invalide ou n\'existe pas. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
			return $this->redirect(array('action' => 'edit_annee_defaut'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->Horaire->delete()) {
				//$this->Session->setFlash(__('L\'année a été supprimé avec succès.'),'alert alert-success',array('class'=>'alert alert-success'));
		} else {
				//$this->Session->setFlash(__('L\'année n\'a pas pu être supprimé. S\'il vous plaît, essayez à nouveau.'),'alert alert-error',array('class'=>'alert alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    public function isAuthorized($user) {
    // Tous les users inscrits peuvent ajouter les posts
    if ($this->action === 'add') {
        return true;
    }

    // Le propriétaire du post peut l'éditer et le supprimer
    if (in_array($this->action, array('edit', 'delete'))) {
        $postId = (int) $this->request->params['pass'][0];
        if ($this->Anneesscollaire->isOwnedBy($postId, $user['id'])) {
            return true;
        }
    }
    return parent::isAuthorized($user);
}
}
