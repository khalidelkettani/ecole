<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'candidatures','action' => '/dashboard')); ?>"><?php echo __('Accueils');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Liste des élèves');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des élèves');?><small></small></h1>
	</div>
</div>
<?php echo $this->Form->create('Elef',array('url'   => array(
               'controller' => 'eleves','action' => 'edit'
           ),'role'=>'form','data-parsley-validate'=>'data-parsley-validate','data-validate'=>'parsley')); ?>

	<?php echo $this->Form->input('id',array('type'=>'hidden')); ?>

<div class="modal-dialog" tyle="opacity: 1;"D>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4><font size="ont-size: 2em" color="#03a9f4"><?php echo __('Informations élève'); ?> </font></h4>
					<h4 class="modal-title"><?php echo __('Informations élève'); ?></h4>
				</div>
				<div class="modal-body" tyle="opacity: 1;">
						<!-- -->
						<!-- Debut tab -->
                             <div class="row" >
								<div class="col-lg-12">
									<div class="main-box clearfix">
										<header class="main-box-header clearfix">
											<h2><?php echo __('Identifiant'); ?></h2>
										</header>
										<div class="main-box-body clearfix">
											<div class="tabs-wrapper">
												<!-- Creation des tabs -->
												<ul class="nav nav-tabs">
													<li class="active"><a href="#tab-home" data-toggle="tab"><?php echo __('Identifiant'); ?></a>
    												</li>
													<li><a href="#tab-Parents" data-toggle="tab">Parents</a>
													</li>
													<li><a href="#tab-Contacts" data-toggle="tab">Personne à contacter</a></li>
													<!--<li ><a href="#tab-note" data-toggle="tab">Note</a>
    												</li>-->
    												<li ><a href="#tab-medical" data-toggle="tab">Medicals</a>
    												</li>
												</ul>
												<!-- Fin de Creation des tabs -->
												<!--  tabs -->
												<div class="tab-content">
					<div class="tab-pane fade in active" id="tab-home">
					<!-- Identification -->
														
						<!-- Nom -->
						<div class="form-group">
							 <label><?php echo __('Nom de l\'élève'); ?> *</label>
							    <?php echo $this->Form->input('nom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>
						<!-- Prenom -->
						<div class="form-group">
							<label><?php echo __('Prenom de l\'élève'); ?> *</label>
							    <?php echo $this->Form->input('prenom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prenom de de l\'élève'),'label'=>false,'data-parsley-required'=>true, 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					        <!-- Fin Prenom --> 
					    </div> 
					    <!-- sexe -->					
                        <div class="form-group">
                            <label><?php echo __('Sexe de l\'élève'); ?> *</label>
                                <?php echo $this->Form->select('sexe',$sexes,array("class"=>"form-control","placeholder"=>__('Sexe de l\'élève'),'label'=>false));
                                ?>
                        </div>
						<!-- FIN SEXE -->                   				  
						<!-- Adresse -->
						<div class="form-group">
							<label><?php echo __('Adresse de l\'élève'); ?> *</label>
							    <?php echo $this->Form->input('adresse',array('class'=>'form-control','div'=>false,'placeholder'=>__('Adresse de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            <!-- Fin adresse -->  
						</div>
						<div class="form-group">
                             <label><?php echo __('Adresse email de l\'élève'); ?> *</label>
							    <?php echo $this->Form->input('email', array('class'=>'form-control','div'=>false,'placeholder'=>__('Adresse email de de l\'élève'),'label'=>false,'type' => 'email','data-parsley-required'=>false,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div> 	 	
									<!-- email -->                	 				
							
						<div class="form-group">
						<label><?php echo __('Date de naissance :'); ?> </label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<?php echo $this->Form->input('date_naissance',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
						</div>
					    </div>
					    <div class="form-group">       
							 <label><?php echo __('lieu naissance de l\'élève'); ?> *</label>
                                           
                                <?php echo $this->Form->select('lieu_naissance',$villes,array('class'=>'form-control','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
                        </div>   
                           
						
					</div>
						<!-- Pere-->	
											
	<div class="tab-pane fade" id="tab-Parents">
		<h3><font size="ont-size: 2em" color="#03a9f4"><?php echo __('Informations de pére'); ?> </font></h3>
		<!--organisation des champs-->
<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<div class="main-box-body clearfix">
				<div class="row">
					<div class="col-lg-6">
						<div class="row">
							<div class="form-group col-md-12">
								<div class="form-group">
							        <label><?php echo __('Nom de pére'); ?> *</label>
							            <?php echo $this->Form->input('nom_pere',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom  de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						        </div> 
								<div class="form-group">
                                    <label><?php echo __('Téléphone   de pére'); ?> *</label>
							            <?php echo $this->Form->input('tel_pere', array('class'=>'form-control','div'=>false,'placeholder'=>__('Téléphone  de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						        </div> 
						        <div class="form-group">
                                     <label><?php echo __('Téléphone portable de pére'); ?> *</label>
							            <?php echo $this->Form->input('portable_pere', array('class'=>'form-control','div'=>false,'placeholder'=>__('Téléphone portable de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						        </div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="row">
							<div class="form-group col-md-12">
								<div class="form-group">
						            <label><?php echo __('Prenom de pére'); ?> *</label>
							            <?php echo $this->Form->input('prenom_pere',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prenom de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					        <!-- Fin Prenom --> 
					            </div> 
							<div class="form-group">
                                <label><?php echo __('Téléphone 2 de pére'); ?> </label>
							        <?php echo $this->Form->input('tel2_pere', array('div'=>false,'placeholder'=>__('Téléphone 2 de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div>
						           

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Adresse -->
				<div class="form-group">
				 <?php echo $this->Form->input('adresse_pere',array('class'=>'form-control','div'=>false,'placeholder'=>__('Adresse de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
				<!-- Fin adresse -->
				</div>
				<div class="form-group">
												<!--<label>Checkboxes</label>-->
										 <div class="col-lg-12" style="text-align:left">
					                     <div class="checkbox-nice">
						                   <?php echo $this->Form->input('recevoir_email_pere', array('type'=>'checkbox','label'=>'Accepter de recevoire un email','class'=>'checkbox-nice','div' => false)); ?>
					                     </div>
												<div class="checkbox-nice">
													<?php echo $this->Form->input('recevoir_sms_pere', array('recevoir_sms_pere'=>'checkbox-1', 'type'=>'checkbox','label'=>'Accepter de recevoire un SMS','class'=>'checkbox-nice','div' => false)); ?>
												</div>
										</div>
						                 
						                                <!-- Fin adresse -->  
									</div>
				</div>

				</div>	

				</div>

		<!--Fin d organisationnchamps-->
		<!--Fin pere-->			    
		<!-- mere-->						
	<h3><font size="ont-size: 2em" color="#03a9f4"><?php echo __('Informations de la mére'); ?> </font></h3>
<div class="row">
		<div class="col-lg-12">
			<div class="main-box1">
				<div class="main-box1-body clearfix">
					<div class="row">
						<div class="col-lg-6">
							<div class="row">
								<div class="form-group col-md-12">
									<div class="form-group">
							            <label><?php echo __('Nom de mére'); ?> *</label>
							                <?php echo $this->Form->input('nom_mere',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom  de mére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div> 
									<div class="form-group">
                                             <label><?php echo __('Téléphone   de mére'); ?> *</label>
							                 <?php echo $this->Form->input('tel_mere', array('class'=>'form-control','div'=>false,'placeholder'=>__('Téléphone  de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div> 
						            <div class="form-group">
                                        <label><?php echo __('Téléphone portable de mére'); ?> *</label>
							            <?php echo $this->Form->input('portable_mere', array('class'=>'form-control','div'=>false,'placeholder'=>__('Téléphone portable de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div>
								</div>
								</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<div class="form-group col-md-12">
											<div class="form-group">
						                        <label><?php echo __('Prenom de mére'); ?> *</label>
							                         <?php echo $this->Form->input('prenom_mere',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prenom de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					        <!-- Fin Prenom --> 
					                        </div> 
											<div class="form-group">
                                            <label><?php echo __('Téléphone 2 de mére'); ?> *</label>
							                    <?php echo $this->Form->input('tel2_mere', array('class'=>'form-control','div'=>false,'placeholder'=>__('Téléphone 2 de pére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						                    </div>
						                        
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- Adresse -->
										
						                <div class="form-group">
							                <label><?php echo __('Adresse de mére'); ?> *</label>
								            <!-- Adresse -->
				                              <?php echo $this->Form->input('adresse_mere',array('class'=>'form-control','div'=>false,'placeholder'=>__('Adresse de de mére'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
				<!-- Fin adresse -->
						            
						                </div>
						                <div class="form-group">
												<!--<label>Checkboxes</label>-->
										 <div class="col-lg-12" style="text-align:left">
					                     <div class="checkbox-nice">
						                   <?php echo $this->Form->input('recevoir_email_mere', array('type'=>'checkbox','label'=>'Accepter de recevoire un email','class'=>'checkbox-nice','div' => false)); ?>
					                     </div>
										<div class="checkbox-nice">
													<?php echo $this->Form->input('recevoir_sms_mere', array('recevoir_sms_mere'=>'checkbox-1', 'type'=>'checkbox','label'=>'Accepter de recevoire un SMS','class'=>'checkbox-nice','div' => false)); ?>
										</div>
										
										</div>
						                 </div>
						                                <!-- Fin adresse -->  
									</div>

								</div>	

							</div>

		<!--Fin d organisationnchamps-->
		<!--Fin mere-->			    
						<!--!debut-->
 				</div>
					<!-- FIN parents -->			
							<div class="tab-pane fade" id="tab-Contacts">
								<ol class="breadcrumb">
										<li class="active"><span><h3><?php echo __('Personne à contacter en cas d \'urgence') ;?> </h3></span></li>
								</ol>
								<div class="form-group">
										<!--<label>Checkboxes</label>-->
									 <div class="col-lg-12" style="text-align:left">
										
                                        <div class="checkbox-nice ">
                                             <?php echo $this->Form->input('contacter_urgent_pere', array('type'=>'checkbox','div' => false,'label'=>'Contacter le pére en cas d \'urgence','div' => false)); ?>
                                        </div>
                                        <div class="checkbox-nice ">
                                             <?php echo $this->Form->input('contacter_urgent_mere', array('type'=>'checkbox','div' => false,'label'=>'Contacter la mére en cas d \'urgence','div' => false)); ?>
                                        </div>
					                     <div class="checkbox-nice ">
                                          <?php echo $this->Form->input('contacter_urgent_autre', array('class'=>'form-control contacter_urgent_autre','onchange'=>'valueChanged()','class'=>'contacter_urgent_autre','type'=>'checkbox','label'=>'Autre personné à Contacter  en cas d \'urgence','div' => false)); ?>
                                         </div>
                                        
                                        <div class="form-group option">
							                <label><?php echo __('Nom '); ?> *</label>
							                <?php echo $this->Form->input('nom_autre',array('class'=>'form-control','div'=>false,'placeholder'=>__('Le nom'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						                </div> 
						                <div class="form-group option">
							                <label><?php echo __('Prénom '); ?> *</label>
							                <?php echo $this->Form->input('prenom_autre',array('class'=>'form-control','div'=>false,'placeholder'=>__('Le Prénom'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div> 
						            <div class="form-group option">
							            <label><?php echo __('Téléphone '); ?> *</label>
							                <?php echo $this->Form->input('tel_autre',array('class'=>'form-control','div'=>false,'placeholder'=>__('Le Téléphone'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div> 
						            <div class="form-group option">
							            <label><?php echo __('GSM '); ?> *</label>
							                <?php echo $this->Form->input('gsm_autre',array('class'=>'form-control','div'=>false,'placeholder'=>__('Le GSM'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						            </div> 

									
										</div>
						            </div>
                                 
							</div>				
					<!-- debut informations Contacts -->
					<!-- FIN parents -->	
					<!--Note-->		
							
					<!--<div class="tab-pane fade" id="tab-note">
						<div class="row">
                             <div class="col-lg-12">
					             <div class="form-group">
						             <label><?php echo __('Description de la note'); ?> *</span></label>
						              <?php echo $this->Form->input('description',array('class'=>'form-control ckeditor','label'=> false, 'id'=>'exampleTextarea', 'type' =>'textarea', 'placeholder'=>__('Entrer la description de la note'), 'rows'=>'3', 'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
					            </div>
				             </div>
				             </div>
				            

					</div>	-->
							<!--Fin note-->	medical
					<div class="tab-pane fade" id="tab-medical">
						
				             <p> hgfhgfhhghg </p>

					</div>	
							<!--Medical-->
							<!--Fin medical-->			
					<!-- debut informations note -->
					
									<!-- FIN informations parents -->

											</div>	<!-- Fin tabs -->
										  </div>	<!--"tabs-wrapper"-->
										</div>  <!--"main-box-body clearfix"-->
									</div>  <!--"main-box clearfix"-->
								</div>  <!--"col-lg-12"-->
							</div>  <!--row-->
						<!-- Fin tab -->	
					    <!-- 	-->
					
				</div> <!-- /.modal-body -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div> <!-- "modal-footer" -->
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
<?php echo $this->Form->end(); ?>

<script>
	$(function($) {
		//datepicker
		$('#datepickerDate').datepicker({
		  format: 'dd-mm-yyyy'
		});
		$('#sel2').select2();
		$('#sel3').select2();
		$('#sel4').select2();
		$('#sel5').select2();
		$('#sel6').select2();
		$('#sel7').select2();
	});
</script>
<script>
	$(function(){
		function initToolbarBootstrapBindings() {
			var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
						'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
						'Times New Roman', 'Verdana'],
				fontTarget = $('[title=Font]').siblings('.dropdown-menu');
			
			$.each(fonts, function (idx, fontName) {
				fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
			});
			$('a[title]').tooltip({container:'body'});
			$('.dropdown-menu input').click(function() {return false;})
				.change(function () {$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');})
				.keydown('esc', function () {this.value='';$(this).change();});

			$('[data-role=magic-overlay]').each(function () { 
				var overlay = $(this), target = $(overlay.data('target')); 
				overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
			});
			if ("onwebkitspeechchange"	in document.createElement("input")) {
				var editorOffset = $('#editor').offset();
				$('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
			} else {
				$('#voiceBtn').hide();
			}
		};
		function showErrorAlert (reason, detail) {
			var msg='';
			if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
			else {
				console.log("error uploading file", reason, detail);
			}
			$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
			 '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
		};
			initToolbarBootstrapBindings();	
		$('#editor').wysiwyg({ fileUploadError: showErrorAlert} );
	});
	</script>


<script type="text/javascript">
$(document).ready(function(){
   // $(".option").hide();
});

function valueChanged()
{
    if($('.contacter_urgent_autre').is(":checked"))   
        $(".option").show();
    else
        $(".option").hide();
}
</script>
