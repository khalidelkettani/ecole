<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Nom du Pays'); ?>&nbsp;</th>
							<th><?php echo __('Actions'); ?>&nbsp;</th>
						</tr>
					</thead>
					<?php foreach ($pays as $pay): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($pay['Pay']['libelle']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'pays','action'=>'edit', $pay['Pay']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $pay['Pay']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $pay['Pay']['libelle'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
	<?php echo $this->element('spaginate'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		
		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});
	</script>