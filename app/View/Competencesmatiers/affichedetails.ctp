<div class="row" style="width: 95%;">
	<div class="col-lg-12">

			
<div class="main-box-body clearfix">
	<header class="main-box-header clearfix">
				<!-- <h2 class="pull-left"><?php echo __('Competenses');?></h2> -->
					<div class="filter-block pull-right">
						<div class="form-group pull-left">
							
							<!-- <i class="fa fa-search search-icon"></i> -->
						</div>			
						<a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouveau');?>
						</a>
					</div>
			</header>
<div class="table-responsive xtable">
<table class="table">

					<thead>
						<tr>
							<th><?php echo __('Competence');?></th> 
						
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($competencesmatiers as $var): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($var['Competencesmatier']['libelle']); ?>&nbsp;
							</td>
							
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'competencesmatiers','action'=>'edit', $var['Competencesmatier']['id'],$q), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Competencesmatier']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Competencesmatier']['libelle'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo $this->element('paginate'); ?>
				</div>
				</div>
</div>
</div>	
		
<!-- <div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div> -->


	















