
<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>"><?php echo __('Accueils');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Liste des années');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des années');?><small></small></h1>
	</div>
</div>


<div class="row">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left"><?php echo __('Année scollaire');?></h2>
					<div class="filter-block pull-right">
						<div class="form-group pull-left">
							<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div>			
						<a href="<?php echo $this->Html->url(array('controller' => 'anneesscollaires','action' => 'add')); ?>" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouvelle année');?>
						</a>
					</div>
			</header>
										
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Année scollaire'); ?>&nbsp;</th>
							<th><?php echo __('Date début'); ?>&nbsp;</th>
							<th><?php echo __('Date fin'); ?>&nbsp;</th>
							<th><?php echo __('Actions'); ?>&nbsp;</th>
						</tr>
					</thead>

					<?php foreach ($anneesscollaires as $var): ?>
					<tbody>
						<tr>
							
							<td>
								<!--<?php debug($var['Anneesscollaire']['default_year']);?>-->
								
							<span <?php if ($var['Anneesscollaire']['default_year']==true){
									echo 'class="label label-success"'; 
 								} //else echo 'class="label label-danger"'
 								?>>
                               <?php echo h($var['Anneesscollaire']['libelle']); ?>
					        </span> 
	
							</td>
							<td>
								<?php echo h($var['Anneesscollaire']['datedebut']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Anneesscollaire']['datefin']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'anneesscollaires','action'=>'edit', $var['Anneesscollaire']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Anneesscollaire']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Anneesscollaire']['libelle'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<div class="modal-footer">
                    <span class="label label-success">
                    <h> <?php echo __('Année scollaire courante'); ?></h>    
						   
					</span> 
				
				</div>	
				<?php echo $this->element('paginate'); ?>
			</div>
		</div>
	</div>
</div>


	

<!--  Model form add 
<?php echo $this->Form->create('Anneesscollaire',array('role'=>'form','data-parsley-validate'=>'data-parsley-validate','action'=>'add','data-validate'=>'parsley')); ?>-->
<!--  Model form edit -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
</div>
<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
<script>
	$(function($) {
		
		$('#datepickerDate1').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
	<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'eleves','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>
