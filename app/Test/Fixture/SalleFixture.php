<?php
/**
 * Salle Fixture
 */
class SalleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'libelle' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'armscii8_general_ci', 'charset' => 'armscii8'),
		'capacite' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'equipements' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'armscii8_general_ci', 'charset' => 'armscii8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'libelle' => 'Lorem ipsum dolor sit amet',
			'capacite' => 1,
			'equipements' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);

}
