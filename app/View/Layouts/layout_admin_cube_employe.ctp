<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>
		<?php echo $this->fetch('title'); ?> | <?php echo __('Gestion Ecole'); ?>
	</title>
	<!-- google font libraries -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>
	<!-- bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/bootstrap/bootstrap.min.css" />
	<!-- RTL support - for demo only -->
	<script src="<?php echo $this->webroot;?>rhq/js/demo-rtl.js"></script>
	<!-- libraries -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/nanoscroller.css" />
	<!-- global styles -->
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/compiled/theme_styles.css" />
	<!-- this page specific styles -->
    <link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/daterangepicker.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/datepicker.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/daterangepicker.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/bootstrap-timepicker.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/libs/select2.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $this->webroot;?>rhq/css/parsley.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/jquery.nouislider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>rhq/css/libs/jquery.nouislider.css">
	<!-- Favicon -->
	<link type="image/x-icon" href="<?php echo $this->webroot;?>rhq/favicon.png" rel="shortcut icon"/>

	<!-- global scripts -->
	<script src="<?php echo $this->webroot;?>rhq/js/demo-skin-changer.js"></script> <!-- only for demo -->
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nanoscroller.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/demo.js"></script> <!-- only for demo -->
	<!-- this page specific scripts -->
	<script src="<?php echo $this->webroot;?>rhq/js/moment.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/daterangepicker.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nouislider.js"></script>
	<!-- theme scripts -->
	<script src="<?php echo $this->webroot;?>rhq/js/scripts.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/pace.min.js"></script>
	
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.maskedinput.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/moment.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/daterangepicker.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap-timepicker.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/select2.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/hogan.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/typeahead.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.pwstrength.js"></script>

	<script src="<?php echo $this->webroot;?>rhq/js/jquery.hotkeys.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/bootstrap-wysiwyg.js"></script>
	
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.nouislider.js"></script>
	
	<script src="<?php echo $this->webroot;?>rhq/js/parsley.min.js"></script>

	<script src="<?php echo $this->webroot;?>rhq/js/jquery.slimscroll.min.js"></script>
	<script src="<?php echo $this->webroot;?>rhq/js/jquery.magnific-popup.min.js"></script>
	
</head>
<body class="theme-blue fixed-header fixed-footer boxed-layout  pace-done">
<div id="theme-wrapper">
		<header class="navbar" id="header-navbar">
			<?php echo  $this->element('rhq/header'); ?>
		</header>
		<div id="page-wrapper" class="container nav-small">
			<div class="row">
				<div id="nav-col">
					<section id="col-left" class="col-left-nano">
						<?php echo  $this->element('rhq/nav-col'); ?>
					</section>
					<div id="nav-col-submenu"></div>
				</div>
				<div id="content-wrapper" class="email-inbox-wrapper">
					<?php echo $this->Session->flash(); ?>
                	<?php echo $content_for_layout; ?>	
					
					<footer id="footer-bar" class="row">
						<p id="footer-copyright" class="col-xs-12">
							 &copy; 2015 <a href="http://rhquebec.com" target="_blank">rhquebec.com</a> <?php echo __('Tous droits réservés. Par'); ?> <a href="http://dfc-technology.com" target="_blank">DFC Technology.</a>
						</p>
					</footer>
				</div>
			</div>
		</div>
	</div>

	
</body>
</html>