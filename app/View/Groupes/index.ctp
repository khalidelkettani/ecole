<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'homes','action' => '/index')); ?>"><?php echo __('Accueil');?></a></li>
			<li><span><?php echo __('Admin');?></span></li>
			<li class="active"><span><?php echo __('Liste des groupes');?></span></li>
		</ol>
									
		<h1><?php echo __('Liste des groupes');?><small></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix">
			<header class="main-box-header clearfix">
				<h2 class="pull-left"><?php echo __('Groupes');?></h2>
					<div class="filter-block pull-right">
						<div class="form-group pull-left">
							<input id="libelle" class="form-control search" type="text" placeholder="<?php echo __('Rechercher ...'); ?>">
							<i class="fa fa-search search-icon"></i>
						</div>			
						<a data-toggle="modal" data-backdrop="static" href="#myModal2" class="btn btn-primary pull-right">
							<i class="fa fa-plus-circle fa-lg"></i> 
							<?php echo __('Nouveau groupe');?>
						</a>
					</div>
			</header>
										
		<div class="main-box-body clearfix">
			<div class="table-responsive xtable">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Nom de groupe');?></th>
							<th><?php echo __('Filiére');?></th>
							<th>Actions</th>
						</tr>
					</thead>
					<?php foreach ($groupes as $var): ?>
					<tbody>
						<tr>
							<td>
								<?php echo h($var['Groupe']['libelle']); ?>&nbsp;
							</td>
							<td>
								<?php echo h($var['Filaire']['libelle']); ?>&nbsp;
							</td>
							<td style="width: 15%;">
								<?php echo $this->Html->link('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i></span> ', array('controller'=>'groupes','action'=>'edit', $var['Groupe']['id']), array('escape' => false, 'class' => 'modalEd table-link','data-toggle'=>'modal', 'target' => '_self'));?>

								<?php echo $this->Form->postLink(__('<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i></span>'), array('action' => 'delete', $var['Groupe']['id']), array('escape' => false,'class' => 'table-link danger'), __('Etes-vous sûr que vous voulez supprimer # %s?', $var['Groupe']['libelle'])); ?>
							</td>
						</tr>
					</tbody>
					<?php endforeach; ?>
				</table>
				<?php echo $this->element('paginate'); ?>
			</div>
		</div>
	</div>
</div>
</div>
	

<!--  Model form add ville-->

	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title"><?php echo __('Ajouter un groupe'); ?></h4>
				</div>
				<div class="modal-body">

			    <?php echo $this->Form->create('Groupe',array('url'=> array('controller' => 'groupes','action' => 'add' ) )); ?>

						<div class="form-group"> <!---->
							<label><?php echo __('Nom de  groupe'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<?php echo $this->Form->input('libelle',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de groupe'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
						</div>

						<div class="form-group form-group-select2">
							<label>
								<?php echo __('Nom de la filiére'); ?> <span class="f_req"><font size="ont-size: 2em" color="#e51c23">*</font></span>
							</label>
							<?php echo $this->Form->select('filaire_id',$filaires,array('class'=>'form-control','style'=>'width:566px','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
						</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Fermer'); ?></button>
					<button type="submit" class="btn btn-primary"><?php echo __('Sauvegarder'); ?></button>
				</div>
				<?php echo $this->Form->end(); ?>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<!--  Model form edit -->
<div class="modal" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>


	<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'groupes','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>





















