<div class="pays view">
<h2><?php echo __('Pays'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pay['Pay']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Libelle'); ?></dt>
		<dd>
			<?php echo h($pay['Pay']['libelle']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pay['Company']['raison_sociale'], array('controller' => 'companies', 'action' => 'view', $pay['Company']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pay'), array('action' => 'edit', $pay['Pay']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pay'), array('action' => 'delete', $pay['Pay']['id']), null, __('Are you sure you want to delete # %s?', $pay['Pay']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Pays'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pay'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('controller' => 'villes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville'), array('controller' => 'villes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Villes'); ?></h3>
	<?php if (!empty($pay['Ville'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="table table-bordered">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Libelle'); ?></th>
		<th><?php echo __('Pay Id'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($pay['Ville'] as $ville): ?>
		<tr>
			<td><?php echo $ville['id']; ?></td>
			<td><?php echo $ville['libelle']; ?></td>
			<td><?php echo $ville['pay_id']; ?></td>
			<td><?php echo $ville['company_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'villes', 'action' => 'view', $ville['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'villes', 'action' => 'edit', $ville['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'villes', 'action' => 'delete', $ville['id']), null, __('Are you sure you want to delete # %s?', $ville['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Ville'), array('controller' => 'villes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
