<?php
App::uses('NiveauxesController', 'Controller');

/**
 * NiveauxesController Test Case
 */
class NiveauxesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.niveaux',
		'app.class',
		'app.classes_niveaux'
	);

}
