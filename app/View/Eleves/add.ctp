<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><a href="<?php echo $this->Html->url(array('plugin' => null,'controller' => 'eleves','action' => '/index')); ?>"><?php echo __('Accueil');?></a></li>
			 <?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
			<li><span><?php echo __('Traitements');?></span></li>
			<li><span><?php echo __('Elèves');?></span></li>
			<li class="active"><span><?php echo __('Ajouter un élève');?></span></li>
			<!--<li class="active"><span><?php echo __('Ajouter un élève');?></span></li>-->
		</ol>									
		<h1><?php echo __('Ajouter un élève');?><small></small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box">
			<header class="main-box-header clearfix">
				<h2>Ajout</h2>
			</header>						
			<div class="main-box-body clearfix">
				 <?php echo $this->Form->create('Elef',array('type' => 'file')); ?>
				 <!--  <?php echo $this->Form->input('id',array('type'=>'hidden')); ?> -->
				<div class="col-lg-6">
					<div class="form-group">
						<label><?php echo __('Nom de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							                  <?php echo $this->Form->input('nom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Nom de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					</div>
					<div class="form-group">
						<label><?php echo __('Prenom de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							                <?php echo $this->Form->input('prenom',array('class'=>'form-control','div'=>false,'placeholder'=>__('Prenom de de l\'élève'),'label'=>false,'data-parsley-required'=>true, 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					</div>
					<div class="form-group">
						    <label><?php echo __('Nationnalité de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                                                 <?php echo $this->Form->select('nationalite_id',$nationalites,array('class'=>'form-control','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
					</div>
					<div class="form-group">
						<label><?php echo __('Sexe de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                                            <?php echo $this->Form->select('sexe',$sexes,array("class"=>"form-control","placeholder"=>__('Sexe de l\'élève'),'label'=>false));?>
					</div>
					<div class="form-group">
						<label><?php echo __('Adresse de l\'élève'); ?><font size="ont-size: 2em" color="#e51c23">*</font></label>
						<?php echo $this->Form->input('adresse',array('class'=>'form-control','div'=>false,'placeholder'=>__('Adresse de de l\'élève'),'label'=>false,'data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					</div>
                    <div class="form-group">
						<label><?php echo __('Photo'); ?> </label>
                        <?php echo $this->Form->input('avatar_logo',array('label'=> false, 'type' =>'file','data-parsley-required'=>false, 'placeholder'=>__('Photo'),'required'=> false, 'data-parsley-error-message' =>__('Ce champ est requis')));?>
					    </div>
					<!-- <div class="form-group">
						<label><?php echo __('Photo'); ?> <span class="f_req">*</span></label>
                        <?php echo $this->Form->input('avatar_logo',array('label'=> false, 'type' =>'file', 'placeholder'=>__('Photo') ));?>
					</div> -->

					<div class="form-group">
						<label><?php echo __('Adresse email de l\'élève'); ?> </label>
							                    <?php echo $this->Form->input('email', array('class'=>'form-control','div'=>false,'placeholder'=>__('Adresse email de de l\'élève'),'label'=>false,'data-parsley-group'=>'block1'));?>
					</div>
												
				</div>
				<div class="col-lg-6">	
					<div class="form-group">
						<div class="span12">
							<label><?php echo __('Date de naissance de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
							<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

							<?php echo $this->Form->input('date_naissance',array('class'=>'form-control','label'=> false, 'type' =>'text','id'=>'datepickerDate', 'required'=> false));?>
						
						    </div>
						</div>
					</div>
					<div class="form-group">
						    <label><?php echo __('Lieu naissance de l\'élève'); ?> <font size="ont-size: 2em" color="#e51c23">*</font></label>
                                                 <?php echo $this->Form->select('lieu_naissance',$villes,array('class'=>'form-control','required'=>'required','id'=>'sel2','data-parsley-required'=>true,'data-parsley-group'=>'block1', 'data-parsley-error-message' => __('Ce champ est requis')));?>
					</div>
					</div>
					
					
				</div>
				<h1></h1>
				<div class="col-lg-12" style="text-align:right">
					<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('controller' => 'eleves','action' => 'index')); ?>"><?php echo __('ANNULER'); ?></a>
					<button type="submit" data-loading-text="Loading..." class="btn btn-primary btn-lg" id="btn-loading-demo"><?php echo __('ENREGISTRER'); ?>
					</button>

				</div>
				<?php echo  $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<script>
	$(function($) {
		
		$('#datepickerDate').datepicker({
		  format: 'yyyy-mm-dd'
		});
		
	});
</script>
<script>
	$(function($) {
		$('#sel2').select2();
		$('#sel3').select2();
		$('#sel4').select2();

		
	});
</script>



<script type="text/javascript">
	$(document).ready(function(){
		  
			$('body').on('keyup', '.search', function(){
			var q=$(this).val();
			var url = '<?php echo Router::url(array('controller'=>'eleves','action'=>'search')) ?>'+'/'+q;
				$(".xtable").html('<div style="padding-top:20px;padding-bottom:20px;text-align:center;"><img src="<?php echo $this->webroot.'img/ajax_loader.gif' ?>"></div>');
				$.get(url, function(data) {
					$(".xtable").html(data);
				});
			});

		//edit function
			$(".modalEd").on('click', function(){
	     		 var url=$(this).attr('href'); 
	            $.get(url, function(data) 
	            {
	              $("#modalEdit").html(data);
	              $("#modalEdit").modal();
	            });
	      	return false;
     		});

	});

	$(function($) {
		$('#sel2').select2();
	});

	</script>